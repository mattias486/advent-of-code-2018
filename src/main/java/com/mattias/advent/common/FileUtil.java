package com.mattias.advent.common;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.List;
import java.util.stream.Collectors;

public class FileUtil {

    public List<Integer> getFileContentAsIntegerList(String filename) throws FileNotFoundException {
        BufferedReader bufferedReader = new BufferedReader(new FileReader(this.getClass().getClassLoader().getResource(filename).getFile()));
        return bufferedReader.lines()
                .map(Integer::parseInt)
                .collect(Collectors.toList());
    }

    public List<String> getFileContentAsStringList(String filename) throws FileNotFoundException {
        BufferedReader bufferedReader = new BufferedReader(new FileReader(this.getClass().getClassLoader().getResource(filename).getFile()));
        return bufferedReader.lines()
                .collect(Collectors.toList());
    }

    public String getFileContentRow(String filename, int rowIndex) {
        try {
            return getFileContentAsStringList(filename).get(rowIndex);
        } catch(FileNotFoundException e) {
            throw new RuntimeException(e);
        }
    }

    public char[][] getFileContentAsCharacterArray(String filename) throws FileNotFoundException {
        List<String> fileContentAsStringList = getFileContentAsStringList(filename);
        char[][] result = new char[fileContentAsStringList.size()][];
        for (int i = 0; i < fileContentAsStringList.size(); i++) {
            result[i] = fileContentAsStringList.get(i).toCharArray();
        }
        return result;
    }

    public char[][] getFileContentAsCharacterArray(String filename, int startRow, int endRow) throws FileNotFoundException {
        List<String> fileContentAsStringList = getFileContentAsStringList(filename);
        char[][] result = new char[endRow - startRow + 1][];
        for (int i = startRow; i <= endRow; i++) {
            result[i - startRow] = fileContentAsStringList.get(i).toCharArray();
        }
        return result;
    }

}
