package com.mattias.advent.day01;

import com.mattias.advent.common.FileUtil;

import java.io.FileNotFoundException;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class Day01 {

    private FileUtil fileUtil = new FileUtil();

    public Integer getSumOfContent() throws FileNotFoundException {
        List<Integer> day01input = fileUtil.getFileContentAsIntegerList("day01input");

        return day01input.stream()
                         .mapToInt(it -> it)
                         .sum();
    }

    public Integer getFirstDuplicateFrequency() throws FileNotFoundException {
        List<Integer> day01input = fileUtil.getFileContentAsIntegerList("day01input");

        int maxNumberFileReads = 10000;
        Integer currentFrequency = 0;

        Set<Integer> frequencyHistory = new HashSet<>();
        frequencyHistory.add(currentFrequency);

        for (int i = 0; i < maxNumberFileReads; i++) {
            for (Integer nextFrequencyAdjustment : day01input) {
                currentFrequency += nextFrequencyAdjustment;
                if (frequencyHistory.contains(currentFrequency)) {
                    return currentFrequency;
                }
                frequencyHistory.add(currentFrequency);
            }
        }
        return currentFrequency;
    }
}
