package com.mattias.advent.day02;

import com.mattias.advent.common.FileUtil;
import org.apache.commons.lang3.ArrayUtils;

import java.io.FileNotFoundException;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

public class Day02 {

    private FileUtil fileUtil = new FileUtil();

    public Long getChecksumForBoxes() throws FileNotFoundException {
        List<String> day02input = fileUtil.getFileContentAsStringList("day02input");

        long duplicates = day02input.stream()
                                    .filter(input -> containsNumberOfTheSameCharacters(input, 2))
                                    .count();

        System.out.println("Number of duplicates : " + duplicates);

        long triplets = day02input.stream()
                                  .filter(input -> containsNumberOfTheSameCharacters(input, 3))
                                  .count();

        System.out.println("Number of triplets : " + Long.toString(triplets));

        return duplicates * triplets;
    }

    protected boolean containsNumberOfTheSameCharacters(String input, int numberOfCharacters) {
        Set<Character> charactersInString = getCharactersInString(input);
        return charactersInString.stream()
                                 .anyMatch(character -> getNumberOfCharacterInstancesInString(input, character) == numberOfCharacters);
    }

    protected Set<Character> getCharactersInString(String input) {
        Character[] characters = ArrayUtils.toObject(input.toCharArray());
        HashSet<Character> uniqueCharacters = new HashSet<>();
        uniqueCharacters.addAll(Arrays.asList(characters));
        return uniqueCharacters;
    }

    protected int getNumberOfCharacterInstancesInString(String input, Character character) {
        char[] chars = input.toCharArray();
        int sumOfCharacters = 0;
        for (int i = 0; i < input.length(); i++) {
            if (chars[i] == character) {
                sumOfCharacters++;
            }
        }
        return sumOfCharacters;
    }

    protected String getSortedString(String input) {
        char[] chars = input.toCharArray();
        Arrays.sort(chars);
        return new String(chars);
    }

    protected Map<String, String> getStringsThatDifferWithOneCharacter() throws FileNotFoundException {
        List<String> day02input = fileUtil.getFileContentAsStringList("day02input");
        return day02input.stream()
                         .map(it1 -> day02input.stream()
                                               .filter(it2 -> getNumberOfCharDifferences(it1, it2) == 1)
                                               .collect(Collectors.toMap(it2 -> it2, __ -> it1))

                         )
                         .filter(it1 -> it1.size() > 0)
                         .findFirst()
                         .orElse(Collections.emptyMap());

    }

    protected String removeDifferingParts(String first, String second) {
        StringBuffer buffer = new StringBuffer();
        for(int i=0;i < first.length(); i++) {
            buffer.append(first.charAt(i) == second.charAt(i) ? first.charAt(i) :  "");
        }
        return buffer.toString();
    }

    protected int getNumberOfCharDifferences(String first, String second) {
        int numberOfDifferences = 0;
        if(first.length() != second.length()) {
            return 0;
        }
        for(int i=0;i < first.length(); i++) {
            numberOfDifferences += first.charAt(i) == second.charAt(i) ? 0 :  1;
        }
        return numberOfDifferences;
    }
}
