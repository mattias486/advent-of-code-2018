package com.mattias.advent.day03;

import com.mattias.advent.common.FileUtil;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Day03 {

    private FileUtil fileUtil = new FileUtil();

    public Long getNumberOfConflictingSquares() throws FileNotFoundException {
        List<Rectangle> rectangles = getRectangles();
        return getNumberOfConflictingSquares(rectangles);
    }

    private List<Rectangle> getRectangles() throws FileNotFoundException {
        List<String> day02input = fileUtil.getFileContentAsStringList("day03input");
        List<Rectangle> rectangles = new ArrayList<>();
        day02input.forEach(it -> rectangles.add(getRectangle(it)));
        return rectangles;
    }

    private Long getNumberOfConflictingSquares(List<Rectangle> rectangles) {
        return getConflictingSquares(rectangles).count();
    }

    private Stream<String> getConflictingSquares(List<Rectangle> rectangles) {
        Map<String, Integer> numberOfRectanglesInSquare = new HashMap<>();
        rectangles.forEach(it -> {
            getSquaresFromRectangle(it).forEach(square -> {
                int newNumber = numberOfRectanglesInSquare.get(square) == null ? 1 : numberOfRectanglesInSquare.get(square) + 1;
                numberOfRectanglesInSquare.put(square, newNumber);
            });
        });
        return numberOfRectanglesInSquare.keySet().stream()
                                         .filter(it -> numberOfRectanglesInSquare.get(it) > 1);
    }

    private Stream<String> getSquaresFromRectangle(Rectangle rectangle) {
        List<String> squares = new ArrayList<>();
        for (int x = rectangle.x; x < rectangle.x + rectangle.width; x++) {
            for (int y = rectangle.y; y < rectangle.y + rectangle.height; y++) {
                squares.add(x + "|" + y);
            }
        }
        return squares.stream();
    }

    private Rectangle getRectangle(String row) {
        String[] s = row.split(" ");
        String position = s[2].replace(":", "");
        String dimensions = s[3];
        String[] p = position.split(",");
        int x = Integer.parseInt(p[0]);
        int y = Integer.parseInt(p[1]);
        String[] d = dimensions.split("x");
        int width = Integer.parseInt(d[0]);
        int height = Integer.parseInt(d[1]);
        return new Rectangle(x, y, width, height);
    }

    public Rectangle getRectangleWithoutConflictingSquare() throws FileNotFoundException {
        List<Rectangle> rectangles = getRectangles();
        Stream<String> conflictingSquares = getConflictingSquares(rectangles);
        return getFirstRectangleWithoutConflict(rectangles, conflictingSquares.collect(Collectors.toList()));
    }

    private Rectangle getFirstRectangleWithoutConflict(List<Rectangle> rectangles, List<String> conflictingSquares) {
        for (Rectangle rectangle : rectangles) {
            Stream<String> squaresFromRectangle = getSquaresFromRectangle(rectangle);
            boolean containsConflict = squaresFromRectangle.anyMatch(conflictingSquares::contains);
            if (!containsConflict) {
                return rectangle;
            }
        }
        return null;
    }
}
