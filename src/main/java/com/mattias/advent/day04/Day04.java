package com.mattias.advent.day04;

import com.mattias.advent.common.FileUtil;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class Day04 {

    private FileUtil fileUtil = new FileUtil();

    private class GuardDay {

        protected String date;

        protected Integer guard;
        protected List<Integer> sleepMinutes = new ArrayList<>();
        protected List<Integer> awakeMinutes = new ArrayList<>();

        protected Integer getSleepMinutes() {
            int dateDuration = 0;
            for (int i = 0; i < sleepMinutes.size(); i++) {
                int sleepMinute = sleepMinutes.get(i);
                int awakeMinute = awakeMinutes.get(i);
                int napDuration = awakeMinute - sleepMinute;
                dateDuration += napDuration;
            }
            return dateDuration;
        }

    }


    private enum NoteType {
        GUARD_NAME, SLEEP, AWAKE;
    }

    public Integer getGuardSleepingTheMost() throws FileNotFoundException {
        Map<String, GuardDay> guardDays = getGuardDates();
        Map<Integer, Integer> guardSleepMinutes = getGuardWithMaxSleepingMinutes(guardDays);
        int maxSleepMinutes = guardSleepMinutes.values().stream()
                                               .mapToInt(Integer::intValue)
                                               .max()
                                               .orElse(0);
        Integer guardWithMostSleepMinutes = guardSleepMinutes.keySet().stream()
                                                             .filter(it -> guardSleepMinutes.get(it).equals(maxSleepMinutes))
                                                             .findFirst()
                                                             .orElse(null);

        System.out.println("Guard with the most sleeping minutes : " + guardWithMostSleepMinutes);
        List<GuardDay> guardDaysForGuardWithMostSleepingMinutes = guardDays.values().stream()
                                                                           .filter(it -> it.guard.equals(guardWithMostSleepMinutes))
                                                                           .collect(Collectors.toList());
        Map<Integer, Integer> minutesForGuard = getMinutesForGuardDays(guardDaysForGuardWithMostSleepingMinutes);

        Integer maxSleepMinute = minutesForGuard.values().stream()
                                                .mapToInt(Integer::valueOf).max()
                                                .orElse(0);
        Integer maxMinute = minutesForGuard.keySet().stream()
                                           .filter(minute -> minutesForGuard.get(minute).equals(maxSleepMinute))
                                           .findFirst().orElse(0);
        System.out.println("Max minute : " + maxMinute + ", number of times slept during that minute : " + maxSleepMinute);


        return maxMinute * guardWithMostSleepMinutes;
    }

    private Map<String, GuardDay> getGuardDates() throws FileNotFoundException {
        List<String> notes = fileUtil.getFileContentAsStringList("day04input");
        Collections.sort(notes);
        return updateGuardDaysFromNotes(notes);
    }

    private Map<Integer, Integer> getMinutesForGuardDays(List<GuardDay> guardDays) {
        Map<Integer, Integer> minutes = new HashMap<>();
        IntStream.range(0, 60).forEachOrdered(it -> minutes.put(it, 0));
        guardDays.forEach(day -> updateMinutes(minutes, day));
        return minutes;
    }

    private void updateMinutes(Map<Integer, Integer> minutes, GuardDay day) {
        getAllMinutesForGuardDay(day)
                .forEach(minute -> minutes.put(minute, minutes.get(minute) + 1));
    }

    private IntStream getAllMinutesForGuardDay(GuardDay day) {
        IntStream result = IntStream.empty();
        for (int i = 0; i < day.sleepMinutes.size(); i++) {
            IntStream minutesForGuardDayItem = getMinutesForGuardDayItem(day, i);
            result = IntStream.concat(result, minutesForGuardDayItem);
        }
        return result;
    }

    private IntStream getMinutesForGuardDayItem(GuardDay day, int index) {
        int startMinute = day.sleepMinutes.get(index);
        int endMinute = day.awakeMinutes.get(index);
        return IntStream.range(startMinute, endMinute);
    }

    private Map<Integer, Integer> getGuardWithMaxSleepingMinutes(Map<String, GuardDay> guardDays) {
        Map<Integer, Integer> guardSleepMinutes = new HashMap<>();
        for (GuardDay guardDay : guardDays.values()) {
            guardSleepMinutes.putIfAbsent(guardDay.guard, 0);
            Integer sleepMinutesToAdd = guardDay.getSleepMinutes();
            Integer currentGuardMinutes = guardSleepMinutes.get(guardDay.guard);
            guardSleepMinutes.put(guardDay.guard, sleepMinutesToAdd + currentGuardMinutes);
        }
        return guardSleepMinutes;
    }

    private NoteType getNoteType(String note) {
        if (isGuardNameNote(note)) {
            return NoteType.GUARD_NAME;
        }
        if (note.contains("falls asleep")) {
            return NoteType.SLEEP;
        }
        if (note.contains("wakes up")) {
            return NoteType.AWAKE;
        }
        return null;
    }

    private boolean isGuardNameNote(String note) {
        return note.contains("begins shift");
    }

    private Map<String, GuardDay> updateGuardDaysFromNotes(List<String> notes) {
        Map<String, GuardDay> guardDays = new HashMap<>();
        Integer mostRecentGuardName = null;
        for (String noteItem : notes) {
            NoteType noteType = getNoteType(noteItem);
            switch (noteType) {
                case GUARD_NAME:
                    mostRecentGuardName = getGuardName(noteItem);
                    break;
                case SLEEP:
                    addSleepMinute(guardDays, mostRecentGuardName, noteItem);
                    break;
                case AWAKE:
                    addAwakeTime(guardDays, noteItem);
                    break;
            }
        }
        return guardDays;
    }

    private void addSleepMinute(Map<String, GuardDay> guardDays, Integer mostRecentGuardName, String noteItem) {
        Integer sleepMinutes = getMinutes(noteItem);
        String date = getDateFromLine(noteItem);
        initiateGuardDate(guardDays, mostRecentGuardName, date);
        guardDays.get(date).sleepMinutes.add(sleepMinutes);
        guardDays.get(date).guard = mostRecentGuardName;
    }

    private void initiateGuardDate(Map<String, GuardDay> guardDays, int mostRecentGuardName, String date) {
        if (guardDays.get(date) == null) {
            guardDays.put(date, new GuardDay());
            guardDays.get(date).guard = mostRecentGuardName;
            guardDays.get(date).date = date;
        }
    }

    private void addAwakeTime(Map<String, GuardDay> dayNotes, String noteItem) {
        String date2 = getDateFromLine(noteItem);
        Integer awakeMinutes = getMinutes(noteItem);
        dayNotes.get(date2).awakeMinutes.add(awakeMinutes);
    }

    private int getGuardName(String note) {
        return Integer.parseInt(note.substring(26).replace(" begins shift", ""));
    }

    private int getMinutes(String note) {
        return Integer.parseInt(note.substring(15, 17));
    }

    private String getDateFromLine(String line) {
        return line.substring(1, 11);
    }


    public Integer getGuardSleepingTheMostOnTheSameMinute() throws FileNotFoundException {
        Map<Integer, Map<Integer, Integer>> guardSleepingMinutes = getGuardSleepingMinutes();
        Integer guardMaxSleepingMinute = guardSleepingMinutes.values().stream()
                                                             .flatMap(it -> it.values().stream())
                                                             .mapToInt(Integer::valueOf)
                                                             .max()
                                                             .orElse(0);

        Integer guardSleepingTheMost = guardSleepingMinutes.keySet().stream()
                                                           .filter(guardId -> guardSleepingMinutes.get(guardId).values().stream()
                                                                                                  .anyMatch(numberOfTimesAsleep -> numberOfTimesAsleep.equals(guardMaxSleepingMinute)))

                                                           .findFirst()
                                                           .orElse(0);

        Map<Integer, Integer> guardMinutes = guardSleepingMinutes.get(guardSleepingTheMost);
        Integer minutesMostSleptOn = guardMinutes.keySet().stream()
                                                 .filter(minute -> guardMinutes.get(minute).equals(guardMaxSleepingMinute))
                                                 .findFirst()
                                                 .orElse(0);

        System.out.println("Guard sleeping the most : " + guardSleepingTheMost + ",  numberOfTimesSleeping : " + guardMaxSleepingMinute + ", on minute : " + minutesMostSleptOn);

        return guardSleepingTheMost * minutesMostSleptOn;
    }

    private Map<Integer, Map<Integer, Integer>> getGuardSleepingMinutes() throws FileNotFoundException {
        Map<Integer, Map<Integer, Integer>> guardAndMinutes = new HashMap<>();
        for (GuardDay guardDay : getGuardDates().values()) {
            guardAndMinutes.putIfAbsent(guardDay.guard, new HashMap<>());
            IntStream allMinutesForGuardDay = getAllMinutesForGuardDay(guardDay);
            allMinutesForGuardDay.forEach(minute -> {
                Map<Integer, Integer> guardMinutes = guardAndMinutes.get(guardDay.guard);
                guardMinutes.putIfAbsent(minute, 0);
                guardMinutes.put(minute, guardMinutes.get(minute) + 1);
            });
        }
        return guardAndMinutes;
    }
}
