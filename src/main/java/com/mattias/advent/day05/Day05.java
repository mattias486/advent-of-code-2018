package com.mattias.advent.day05;

import com.mattias.advent.common.FileUtil;

import java.io.FileNotFoundException;

public class Day05 {

    private FileUtil fileUtil = new FileUtil();

    public String getRemainingString() throws FileNotFoundException {
        String input = fileUtil.getFileContentRow("day05input", 0);
        return replaceParts(input);
    }

    private String replaceParts(String input) {
        String lastInput = null;
        String currentInput = input;
        while (!currentInput.equals(lastInput)) {
            lastInput = currentInput;
            currentInput = replaceFirstPossible(currentInput);
        }
        return currentInput;
    }

    private String replaceFirstPossible(String input) {
        for(int i=0; i<input.length()-1; i++) {
            char token = input.charAt(i);
            char followingToken = input.charAt(i + 1);
            boolean isEqualCase = token == followingToken;
            boolean equalsWithDifferentCase = Character.toString(token).toUpperCase().equals(
                    Character.toString(followingToken).toUpperCase());
            if(!isEqualCase && equalsWithDifferentCase) {
                String replacedString = input.substring(0, i) + (i + 2 > input.length() ? "" : input.substring(i + 2));
                return replacedString;
            }
        }
        return input;
    }
}
