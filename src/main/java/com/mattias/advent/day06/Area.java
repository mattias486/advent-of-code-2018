package com.mattias.advent.day06;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class Area {
    private final Point topLeft;
    private final Point bottomLeft;
    private final Point topRight;
    private final Point bottomRight;
}
