package com.mattias.advent.day06;

import com.mattias.advent.common.FileUtil;

import java.io.FileNotFoundException;
import java.util.*;
import java.util.stream.Collectors;

public class Day06 {

    private FileUtil fileUtil = new FileUtil();

    public List<Point> getPoints() throws FileNotFoundException {
        List<String> input = fileUtil.getFileContentAsStringList("day06input");
        Queue<String> pointNames = getPointNames();
        return input.stream()
                .map(it -> it.replaceAll(" ", "").split(",", 2))
                .map(it -> new Point(Integer.parseInt(it[0]), Integer.parseInt(it[1]), pointNames.remove()))
                .collect(Collectors.toList());
    }

    private Queue<String> getPointNames() {
        Queue<String> names = new ArrayDeque<>() {
        };
        for(char uppercase='A'; uppercase <= 'Z'; uppercase++) {
            names.add(Character.toString(uppercase));
        }
        for(char lowercase='a'; lowercase <= 'z'; lowercase++) {
            names.add(Character.toString(lowercase));
        }
        return names;
    }


    public Area getArea(List<Point> points) {
        int bottom = points.stream()
                .max(Comparator.comparingInt(point -> point.y))
                .map(point -> point.y)
                .orElse(0);
        int top = points.stream()
                .min(Comparator.comparingInt(point -> point.y))
                .map(point -> point.y)
                .orElse(0);

        int left = points.stream()
                .min(Comparator.comparingInt(point -> point.x))
                .map(point -> point.x)
                .orElse(0);

        int right = points.stream()
                .max(Comparator.comparingInt(point -> point.x))
                .map(point -> point.x)
                .orElse(0);

        return Area.builder().topLeft(new Point(left, top, null))
                .topRight(new Point(right, top, null))
                .bottomLeft(new Point(left, bottom, null))
                .bottomRight(new Point(right, bottom, null))
                .build();
    }

    public Integer calculateLargestArea() throws FileNotFoundException {
        List<Point> points = getPoints();
        Area area = getArea(points);
        Grid grid = new Grid(area, points);
        return grid.calculateLargestArea();
    }

    public Long calculateRegionOfTotalDistancesToPoints() throws FileNotFoundException {
        List<Point> points = getPoints();
        Area area = getArea(points);
        Grid grid = new Grid(area, points);
        return grid.calculateRegionWithMaxDistancesToAllPoints();
    }
}
