package com.mattias.advent.day06;

import lombok.Data;

import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Data
public class Grid {
    private final Area area;
    private final List<Point> points;

    private Map<Integer, Map<Integer, GridPoint>> visual = new HashMap<>();

    public Integer calculateLargestArea() {
        initiateGrindForArea();
        calculateGridForPoints();
        Integer gridAreaSizeForLimitedPoints = getMaxGridAreaSizeForLimitedRegionPoints();

        printGrind();
        return gridAreaSizeForLimitedPoints;
    }

    public Long calculateRegionWithMaxDistancesToAllPoints() {
        initiateGrindForArea();
        Long regionSize = calculateGridWithMaxDistanceToAllPoints();

        printGrind();
        return regionSize;
    }

    private Long calculateGridWithMaxDistanceToAllPoints() {
        for (int y = area.getTopLeft().y; y <= area.getBottomLeft().y; y++) {
            for (int x = area.getTopLeft().x; x <= area.getTopRight().x; x++) {
                int distance = calculateDistanceToPoints(x, y);
                if(distance < 10000) {
                    visual.get(y).get(x).setPoint(points.get(0));
                }
            }
        }
        return visual.values().stream()
                .flatMap(it -> it.values().stream())
                .filter(it -> it.getPoint() != null)
                .count();
    }

    private int calculateDistanceToPoints(int x, int y) {
        return points.stream()
                .mapToInt(it -> it.getDistanceToPoint(x, y))
                .sum();
    }

    private Integer getMaxGridAreaSizeForLimitedRegionPoints() {
        Set<Point> unlimitedPoints = getPointsWithUnlimitedRegion();
        Map<Point, Integer> regionSizes = getRegionSizes();
        Point maxPoint = regionSizes.keySet().stream()
                .filter(it -> !unlimitedPoints.contains(it))
                .max(Comparator.comparingInt(regionSizes::get))
                .orElse(null);

        return regionSizes.get(maxPoint);
    }

    private Set<Point> getPointsWithUnlimitedRegion() {
        Set<Point> unlimited = new HashSet<>();
        Map<Integer, GridPoint> topRow = visual.get(area.getTopLeft().y);
        Map<Integer, GridPoint> bottomRow = visual.get(area.getBottomLeft().y);
        Stream.concat(topRow.values().stream(), bottomRow.values().stream())
                .filter(it -> !it.isInvalidDueToDuplicateDistance())
                .forEach(it -> unlimited.add(it.getPoint()));

        int leftColumnIndex = area.getTopLeft().x;
        int rightColumnIndex = area.getTopRight().x;
        visual.values().stream()
                .flatMap(it -> Stream.of(it.get(leftColumnIndex), it.get(rightColumnIndex)))
                .filter(it -> !it.isInvalidDueToDuplicateDistance())
                .forEach(it -> unlimited.add(it.getPoint()));

        return unlimited;
    }

    private Map<Point, Integer> getRegionSizes() {
        Map<Point, Integer> groupedByPoint = new HashMap<>();

        List<GridPoint> gridPoints = visual.values().stream()
                .flatMap(it -> it.values().stream())
                .filter(it -> !it.isInvalidDueToDuplicateDistance())
                .collect(Collectors.toList());

        for (GridPoint gridPoint : gridPoints) {
            Point point = gridPoint.getPoint();
            groupedByPoint.putIfAbsent(point, 0);
            groupedByPoint.put(point, groupedByPoint.get(point) + 1);
        }

        return groupedByPoint;
    }

    private void calculateGridForPoints() {
        updateAreaForPoint(points.get(0));
        for (Point point : points) {
            updateAreaForPoint(point);

        }
    }

    private void updateAreaForPoint(Point point) {
        for (int y = area.getTopLeft().y; y <= area.getBottomLeft().y; y++) {
            for (int x = area.getTopLeft().x; x <= area.getTopRight().x; x++) {
                visual.get(y).get(x).updateForPoint(point.getDistanceToPoint(x, y), point);
            }
        }
    }

    private void initiateGrindForArea() {
        for (int y = area.getTopLeft().y; y <= area.getBottomLeft().y; y++) {
            visual.put(y, new HashMap<>());
            for (int x = area.getTopLeft().x; x <= area.getTopRight().x; x++) {
                visual.get(y).put(x, new GridPoint(x, y));
            }
        }
    }

    private void printGrind() {
        AtomicInteger rowCounter = new AtomicInteger();
        for (Map<Integer, GridPoint> row : visual.values()) {
            StringBuilder output = new StringBuilder();
            for (GridPoint column : row.values()) {
                boolean isConflict = column.isInvalidDueToDuplicateDistance();
                output.append(isConflict ? "!" : Optional.ofNullable(column.getPoint()).map(Point::getName).orElse(" "));
            }
            System.out.println(String.format("%03d", rowCounter.incrementAndGet()) + "_" + output.toString());
        }
    }
}
