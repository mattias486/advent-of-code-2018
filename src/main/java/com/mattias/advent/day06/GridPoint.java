package com.mattias.advent.day06;

import lombok.Data;

@Data
public class GridPoint {
    private final int x;
    private final int y;

    private int minDistanceToPoint;
    private Point point;
    private boolean invalidDueToDuplicateDistance = false;

    /**
     * returns true if newPoint is the closest known point to the gridPoint so far
     * returns false if newPoint has the same distance or further then another already known point
     */
    public boolean updateForPoint(int distance, Point newPoint) {
        boolean replace = this.point == null || minDistanceToPoint > distance;
        boolean invalidate = this.point != null && minDistanceToPoint == distance;
        if(invalidate) {
            invalidDueToDuplicateDistance = true;
            return false;
        }
        if(replace) {
            this.point = newPoint;
            this.minDistanceToPoint = distance;
            this.invalidDueToDuplicateDistance = false;
            return true;
        }
        return false;
    }
}
