package com.mattias.advent.day06;

import lombok.Data;

@Data
public class Point {
    public final int x;
    public final int y;
    public final String name;

    public int getDistanceToPoint(int x, int y) {
        return Math.abs(this.x - x) + Math.abs(this.y - y);
    }

    @Override
    public String toString() {
        return String.format("%s, %s", x, y);
    }
}
