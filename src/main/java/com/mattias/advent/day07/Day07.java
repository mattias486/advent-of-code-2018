package com.mattias.advent.day07;

import com.mattias.advent.common.FileUtil;

import java.io.FileNotFoundException;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class Day07 {

    FileUtil fileUtil = new FileUtil();

    private Map<String, Set<String>> assemblySteps = new TreeMap<>();
    private Map<String, Integer> workers = new HashMap<>();

    public String getAssemblyOrder() throws FileNotFoundException {
        List<String> input = fileUtil.getFileContentAsStringList("day07input");
        setupAssemblySteps(input);
        return selectAssemblyOrder();
    }

    public Integer getAssemblyTimeWithMultipleWorkers() throws FileNotFoundException {
        List<String> input = fileUtil.getFileContentAsStringList("day07input");
        setupAssemblySteps(input);
        return selectAssemblyOrderMultipleWorkers();
    }

    private Integer selectAssemblyOrderMultipleWorkers() {
        int time = 0;
        while (!assemblySteps.isEmpty() && time < 100000) {
            log.info("Time : " + time);
            selectStepsForWorkers(5);
            removeStepFromWorkers();
            time = time + 1;
            tickWorkers();
        }
        return time;
    }

    private void tickWorkers() {
        workers = workers.keySet().stream()
                .collect(Collectors.toMap(Function.identity(), it -> workers.get(it) - 1));
    }

    private void removeStepFromWorkers() {
        workers.keySet().stream()
                .filter(it -> workers.get(it) == 1)
                .collect(Collectors.toSet())
                .forEach(it -> {
                    log.info("Done with step : " + it);
                    workers.remove(it);
                    removeAssemblyStep(it);
                });

    }

    private void selectStepsForWorkers(int maxWorkers) {
        int freeWorkers = maxWorkers - workers.size();
        for (int i = 1; i <= freeWorkers; i++) {
            String nextStep = getNextStepWithoutRequirements();
            if (nextStep == null) {
                return;
            }
            Integer timeForNextStep = 61 + nextStep.charAt(0) - 'A';
            workers.put(nextStep, timeForNextStep);
            log.info(String.format("Started worker for step : %s with time : %d. Workers running : %d", nextStep, timeForNextStep, workers.size()));
        }
    }

    private String selectAssemblyOrder() {
        StringBuilder order = new StringBuilder();
        while (!assemblySteps.isEmpty()) {
            String nextStep = selectNextStep();
            order.append(nextStep);
        }
        return order.toString();
    }

    private String selectNextStep() {
        String nextStep = getNextStepWithoutRequirements();
        removeAssemblyStep(nextStep);
        return nextStep;
    }

    private String getNextStepWithoutRequirements() {
        return assemblySteps.keySet().stream()
                .filter(it -> assemblySteps.get(it).isEmpty())
                .filter(it -> !workers.keySet().contains(it))
                .findFirst()
                .orElse(null);
    }

    private void removeAssemblyStep(String step) {
        workers.remove(step);
        assemblySteps.remove(step);
        assemblySteps.values()
                .forEach(it -> it.remove(step));
    }

    private void setupAssemblySteps(List<String> input) {
        for (String inputItem : input) {
            String assemblyStep = inputItem.substring(36, 37);
            String preRequirement = inputItem.substring(5, 6);
            assemblySteps.putIfAbsent(assemblyStep, new HashSet<>());
            assemblySteps.putIfAbsent(preRequirement, new HashSet<>());
            assemblySteps.get(assemblyStep).add(preRequirement);
        }
    }
}
