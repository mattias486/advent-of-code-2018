package com.mattias.advent.day08;

import com.mattias.advent.common.FileUtil;
import lombok.extern.slf4j.Slf4j;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Slf4j
public class Day08 {

    FileUtil fileUtil = new FileUtil();

    public Integer getLicenseMetadataSum() throws FileNotFoundException {
        String license = fileUtil.getFileContentRow("day08input", 0);
        List<Integer> licenseData = Arrays.stream(license.split(" "))
                .map(Integer::parseInt)
                .collect(Collectors.toList());

        LicenseNode licenseNode = buildLicenceTree(licenseData);

        return getLicenseMetadataSum(licenseNode);
    }

    public Integer calculatedAlternativeLicenseChecksum() throws FileNotFoundException {
        String license = fileUtil.getFileContentRow("day08input", 0);
        List<Integer> licenseData = Arrays.stream(license.split(" "))
                .map(Integer::parseInt)
                .collect(Collectors.toList());

        LicenseNode licenseNode = buildLicenceTree(licenseData);

        return getMetadataCalculatedOfLeafNodes(licenseNode);
    }

    private Integer getMetadataCalculatedOfLeafNodes(LicenseNode node) {
        if (node.getChildren().isEmpty()) {
            return node.getMetadata().stream()
                    .mapToInt(it -> it).sum();
        }
        return node.getMetadata().stream()
                .filter(childNumber -> childNumber > 0)
                .filter(childNumber -> childNumber <= node.getChildren().size())
                .mapToInt(childNumber -> childNumber - 1)
                .map(childIndex -> getMetadataCalculatedOfLeafNodes(node.getChildren().get(childIndex)))
                .sum();
    }

    private Integer getLicenseMetadataSum(LicenseNode node) {
        int metaDataSum = node.getMetadata().stream()
                .mapToInt(Integer::intValue)
                .sum();
        int childMetaDataSum = node.getChildren().stream()
                .mapToInt(this::getLicenseMetadataSum).sum();

        return metaDataSum + childMetaDataSum;
    }

    private LicenseNode buildLicenceTree(List<Integer> license) {
        log.info("License size : " + license.size());
        return parseLicenceNodeFromPosition(license, 0);
    }

    private LicenseNode parseLicenceNodeFromPosition(List<Integer> licence, Integer position) {
        Integer numberOfChildren = licence.get(position);
        Integer metaDataEntries = licence.get(position + 1);
        log.info(String.format("Parsing child at position : %s with children : %s and metadata entries : %s", position, numberOfChildren, metaDataEntries));

        List<LicenseNode> children = new ArrayList<>();

        Integer nextParseIndex = position + 2;
        for (int i = 1; i <= numberOfChildren; i++) {
            LicenseNode childNode = parseLicenceNodeFromPosition(licence, nextParseIndex);
            children.add(childNode);
            nextParseIndex = childNode.getLicenceLastIndex();
        }

        List<Integer> metadata = new ArrayList<>();

        if (metaDataEntries > 0) {
            metadata.addAll(licence.subList(nextParseIndex, nextParseIndex + metaDataEntries));
            nextParseIndex = nextParseIndex + metaDataEntries;
        }

        return LicenseNode.builder()
                .children(children)
                .metadata(metadata)
                .licenceLastIndex(nextParseIndex)
                .build();
    }
}
