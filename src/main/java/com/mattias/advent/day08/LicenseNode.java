package com.mattias.advent.day08;

import lombok.Builder;
import lombok.Data;

import java.util.List;

@Data
@Builder(toBuilder = true)
public class LicenseNode {
    private final List<LicenseNode> children;
    private final List<Integer> metadata;
    private final Integer licenceLastIndex;
}
