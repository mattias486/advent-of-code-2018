package com.mattias.advent.day09;

import lombok.Builder;
import lombok.extern.slf4j.Slf4j;

import java.util.*;

@Slf4j
@Builder
public class Day09 {

    Map<Integer, Long> totalScore;
    List<Integer> circleTiles;
    List<Integer> inactiveCircleElements;

    int currentTileIndex;

    int numPlayers;
    int lastMarbleWorth;

    public static final int ACTIVE_CIRCLE_SIZE = 24000;

    public long calculatedHighestScore(int numPlayers, int lastMarbleWorth) {
        this.numPlayers = numPlayers;
        this.lastMarbleWorth = lastMarbleWorth;
        return playGame();
    }

    private long playGame() {
        initiateGame();
        int turn = 1;
        while (turn <= lastMarbleWorth - 22) { //play turn only if there still are following tiles that will be a scoring rounds
            if (turn % 100_000 < 23) log.info("turn : " + turn);
            if(circleTiles.size() > ACTIVE_CIRCLE_SIZE * 2) {
                performanceTuneCircle();
            }
            playGameRound(turn);
            turn = turn + 23;
        }

        return totalScore.values().stream()
                .mapToLong(it -> it)
                .max()
                .orElse(0);
    }

    private void performanceTuneCircle() {
        mergeInactiveCircleParts();
        changeCurrentTileToIndexZero();
        moveInactivePartsOfCircle();
    }

    private void moveInactivePartsOfCircle() {
        List<Integer> newCircle = new ArrayList<>(circleTiles.subList(0, ACTIVE_CIRCLE_SIZE));
        List<Integer> newInactiveCircle = new ArrayList<>(circleTiles.subList(ACTIVE_CIRCLE_SIZE, circleTiles.size()));
        inactiveCircleElements = newInactiveCircle;
        circleTiles = newCircle;
    }

    private void mergeInactiveCircleParts() {
        List<Integer> wholeCircle = new ArrayList<>(circleTiles.size() + inactiveCircleElements.size());
        wholeCircle.addAll(circleTiles);
        wholeCircle.addAll(inactiveCircleElements);
        circleTiles = wholeCircle;
    }

    private void initiateGame() {
        circleTiles = new ArrayList<>();
        inactiveCircleElements = new ArrayList<>();
        circleTiles.add(0, 0);
        totalScore = new HashMap<>();
        for (int i = 1; i <= numPlayers; i++) {
            totalScore.putIfAbsent(i, 0L);
        }
        currentTileIndex = 0;
    }

    //22 regular turns and one score turn
    private void playGameRound(int turn) {
        perform22RegularTurns(turn);
        removeAndSkipMarble(turn + 22);
    }

    private void removeAndSkipMarble(int turn) {
        int marbleIndexToRemove = getIndexSevenTilesToTheLeft();
        int removedMarble = circleTiles.get(marbleIndexToRemove);
        circleTiles.set(marbleIndexToRemove, null);
        addScore(turn, removedMarble);
        currentTileIndex = getNoneNullTileIndexRight(marbleIndexToRemove);
    }

    private void addScore(int turn, Integer removedMarble) {
        int scoreToAdd = turn + removedMarble;
        Integer player = getPlayerFromTurn(turn);
        totalScore.put(player, totalScore.get(player) + scoreToAdd);
    }

    private Integer getPlayerFromTurn(int turn) {
        return 1 + turn % numPlayers;
    }

    private void perform22RegularTurns(int turn) {
        for (int i = turn; i <= turn + 21; i++) {
            performRegularTurn(i);
        }
    }

    /**
     * in order to reduce complexity of the case where we get to close to the end of the array representation of the
     * circle we reindex the circle so current tile becomes index 0 and we move circle tiles so the tiles to the right
     * and to the left of the current tile is still the same
     *
     * example:
     *
     * current index 4, current circle array elements : 0 1 2 3 4 5 6 7 8 9
     *
     * after changing so current index is 0, the circle array elements will be : 4 5 6 7 8 9 0 1 2 3
     *
     * so tile at current index - 1 is still "3" (wrapping around the array), and current element + 1 is still "5"
     */
    private void changeCurrentTileToIndexZero() {
        List<Integer> newCircle = new ArrayList<>(circleTiles.size());
        newCircle.addAll(circleTiles.subList(currentTileIndex, circleTiles.size()));
        newCircle.addAll(circleTiles.subList(0, currentTileIndex));
        circleTiles = newCircle;
        currentTileIndex = 0;
    }

    private void performRegularTurn(int turn) {
        int nextTileIndex = getIndexTwoTilesToTheRight(currentTileIndex);
        circleTiles.add(nextTileIndex, turn);
        currentTileIndex = nextTileIndex;
    }

    int getIndexTwoTilesToTheRight(int offset) {
        for (int i = 0; i < 2; i++) {
            offset = getNoneNullTileIndexRight(offset);
        }
        return offset;
    }

    int getIndexSevenTilesToTheLeft() {
        int offset = currentTileIndex;
        for (int i = 0; i < 7; i++) {
            offset = getNoneNullTileIndexLeft(offset);
        }
        return offset;
    }

    private int getIndexForNextTileRight(int offset) {
        return (offset + 1) % circleTiles.size();
    }

    private int getIndexForNextTileLeft(int offset) {
        return (circleTiles.size() + offset - 1) % circleTiles.size();
    }

    int getNoneNullTileIndexRight(int offset) {
        while (true) {
            int nextToTheRight = getIndexForNextTileRight(offset);
            if (circleTiles.get(nextToTheRight) != null) {
                return nextToTheRight;
            }
            offset += 1;
        }
    }

    int getNoneNullTileIndexLeft(int offset) {
        while (true) {
            int nextToTheLeft = getIndexForNextTileLeft(offset);
            if (circleTiles.get(nextToTheLeft) != null) {
                return nextToTheLeft;
            }
            offset -= 1;
        }
    }
}
