package com.mattias.advent.day10;

import java.io.FileNotFoundException;
import java.util.*;
import java.util.stream.Collectors;

public class Day10 {

    public static final int MAX_NUMBER_OF_STEPS = 100000;

    public List<PointOfLight> getPointsOfLight() throws FileNotFoundException {
        List<PointOfLight> pointsOfLight = new InputDataUtil().getPointsOfLight();
        return pointsOfLight;
    }

    public void findMessage() throws FileNotFoundException {
        List<PointOfLight> lights = getPointsOfLight();
        findLikelyMessage(lights);
    }

    private void findLikelyMessage(List<PointOfLight> lights) {
        int second = 0;
        while (!isThereNoGapInYCoordinates(lights) && second < MAX_NUMBER_OF_STEPS) {
            lights.forEach(this::moveLight);
            second += 1;
        }
        normalizeAndPrintMessage(lights, second); //found candidate for possible message
        while (isThereNoGapInYCoordinates(lights)) { //continue to print possible messages until there is a gap in coordinates
            lights.forEach(this::moveLight);
            second += 1;
            normalizeAndPrintMessage(lights, second);
        }
    }

    private void normalizeAndPrintMessage(List<PointOfLight> lights, int second) {
        List<PointOfLight> normalizedLights = getLightsWithNormalizedPositions(lights);
        printMessage(normalizedLights, second);
    }

    void moveLight(PointOfLight it) {
        it.position.x += it.velocity.right;
        it.position.y += it.velocity.down;
    }

    /**
     * trying to find a message by checking if there is no gap in the y-coordinates,
     * if a message is present then we would expect the lights to be on rows close to each other
     */
    boolean isThereNoGapInYCoordinates(List<PointOfLight> points) {
        List<Integer> yCoordinates = getYCoordinates(points);
        int minY = yCoordinates.stream().mapToInt(Integer::intValue).min().orElse(0);
        int maxY = yCoordinates.stream().mapToInt(Integer::intValue).max().orElse(0);
        for (int y = minY; y <= maxY; y++) {
            if(!yCoordinates.contains(y)) {
                return false;
            }
        }
        return true;
    }

    private List<Integer> getYCoordinates(List<PointOfLight> points) {
        return points.stream()
                .map(it -> it.position.y)
                .sorted()
                .collect(Collectors.toList());
    }

    private void printMessage(List<PointOfLight> lights, int second) {
        System.out.println("=== Possible message at second : " + second);
        Arrays.stream(getMessage(lights))
                .forEach(System.out::println);
        System.out.println("");
    }

    private List<PointOfLight> getLightsWithNormalizedPositions(List<PointOfLight> lights) {
        int widthToRemove = lights.stream().mapToInt(it -> it.position.x).min().orElse(0);
        int heightToRemove = lights.stream().mapToInt(it -> it.position.y).min().orElse(0);
        List<PointOfLight> normalizedLights = new ArrayList<>();
        lights.forEach(it -> normalizedLights.add(it.toBuilder()
                    .position(Position.builder()
                            .x(it.position.x - widthToRemove)
                            .y(it.position.y - heightToRemove)
                            .build())
                    .build()
        ));
        return normalizedLights;
    }

    /**
     * the printing of the message should be called after normalization of the lights have been made.
     * getMessage assumes that there are no negative values for x or y coordinates of the lights
     */
    String[] getMessage(List<PointOfLight> lights) {
        int maxXCord = lights.stream().mapToInt(it -> it.position.x).max().orElse(1);
        int maxYCord = lights.stream().mapToInt(it -> it.position.y).max().orElse(1);
        String[] message = new String[maxYCord + 1];
        for(int rowIndex=0;rowIndex <= maxYCord; rowIndex++) {
            message[rowIndex] = new String(getMessageRow(lights, maxXCord, rowIndex));
        }

        return message;
    }

    private char[] getMessageRow(List<PointOfLight> lights, int rowLength, int rowIndex) {
        char[] rowMessage = new char[rowLength + 1];
        Arrays.fill(rowMessage, ' ');
        lights.stream()
                .filter(it -> it.position.y == rowIndex)
                .forEach(it -> rowMessage[it.position.x] = 'X');
        return rowMessage;
    }
}
