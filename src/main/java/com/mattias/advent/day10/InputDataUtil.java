package com.mattias.advent.day10;

import com.mattias.advent.common.FileUtil;

import java.io.FileNotFoundException;
import java.util.List;
import java.util.stream.Collectors;

class InputDataUtil {

    FileUtil fileUtil = new FileUtil();

    public List<PointOfLight> getPointsOfLight() throws FileNotFoundException {
        List<String> day10input = fileUtil.getFileContentAsStringList("day10input");
        return day10input.stream()
                .map(this::getPointOfLightFromLine)
                .collect(Collectors.toList());
    }

    private PointOfLight getPointOfLightFromLine(String line) {
        int x = Integer.parseInt(line.substring(10, 16).trim());
        int y = Integer.parseInt(line.substring(18, 24).trim());
        int right = Integer.parseInt(line.substring(36, 38).trim());
        int down = Integer.parseInt(line.substring(40, 42).trim());
        Position position = getPosition(x, y);
        Velocity velocity = getVelocity(right, down);
        return getPointOfLight(position, velocity);
    }

    private PointOfLight getPointOfLight(Position position, Velocity velocity) {
        return PointOfLight.builder()
                .position(position)
                .velocity(velocity)
                .build();
    }

    private Velocity getVelocity(int right, int down) {
        return Velocity.builder()
                .right(right)
                .down(down)
                .build();
    }

    private Position getPosition(int x, int y) {
        return Position.builder()
                .x(x)
                .y(y)
                .build();
    }
}
