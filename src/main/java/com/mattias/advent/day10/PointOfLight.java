package com.mattias.advent.day10;

import lombok.Builder;

@Builder(toBuilder = true)
class PointOfLight {
    Position position;
    Velocity velocity;
}
