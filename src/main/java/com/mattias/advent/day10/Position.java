package com.mattias.advent.day10;

import lombok.Builder;

@Builder
class Position {
    int x;
    int y;
}
