package com.mattias.advent.day10;

import lombok.Builder;

@Builder
class Velocity {
    int right;
    int down;
}
