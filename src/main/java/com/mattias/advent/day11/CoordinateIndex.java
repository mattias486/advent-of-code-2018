package com.mattias.advent.day11;

import lombok.Data;

@Data
class CoordinateIndex {
    final int x;
    final int y;
}
