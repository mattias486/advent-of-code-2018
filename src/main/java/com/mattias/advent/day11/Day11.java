package com.mattias.advent.day11;

import lombok.extern.slf4j.Slf4j;

import java.util.Comparator;
import java.util.stream.IntStream;
import java.util.stream.Stream;

@Slf4j
public class Day11 {

    static final int POWER_CELL_SQUARE_SIZE = 300;

    public static final int FUEL_CELL_DIMENSION = 3;

    int getPowerLevel(int x, int y, int serialNumber) {
        int rackId = x + 10;
        int powerLevel = rackId * y;
        powerLevel += serialNumber;
        powerLevel *= rackId;
        powerLevel = get100Digit(powerLevel);
        return powerLevel - 5;
    }

    int get100Digit(int powerLevel) {
        if (powerLevel <= 99) {
            return 0;
        }
        return (powerLevel / 100) % 10;
    }

    int[][] powerLevels(int serialNumber) {
        int[][] powerLevels = new int[POWER_CELL_SQUARE_SIZE][POWER_CELL_SQUARE_SIZE];
        IntStream.range(1, POWER_CELL_SQUARE_SIZE)
                .forEach(y -> IntStream.range(1, POWER_CELL_SQUARE_SIZE)
                        .forEach(x -> powerLevels[y-1][x-1] = getPowerLevel(x, y, serialNumber))
                );
        return powerLevels;
    }

    FuelCellSquare getSquareWithMaxTotalPower(int[][] powerLevels, int fuelCellDimension) {
        FuelCellSquare fuelCellSquare = IntStream.range(0, POWER_CELL_SQUARE_SIZE - fuelCellDimension + 1)
                .boxed()
                .parallel()
                .flatMap(it -> getCoordinateForRow(it, fuelCellDimension))
                .map(index -> FuelCellSquare.builder()
                        .x(index.x + 1)  //we add 1 to the x and y here in order to change from using indexes to positions (starting at 1,1 instead of 0,0)
                        .y(index.y + 1)
                        .totalPower(getTotalPowerForSquare(powerLevels, index.x, index.y, fuelCellDimension))
                        .dimension(fuelCellDimension)
                        .build())
                .max(Comparator.comparingInt(square -> square.totalPower))
                .orElse(null);

        //log.info(String.format("Max power for square with dimension %s : %s", fuelCellSquare.dimension, fuelCellSquare.totalPower));
        return fuelCellSquare;
    }

    private Stream<CoordinateIndex> getCoordinateForRow(int y, int fuelCellDimension) {
        return IntStream.range(0, POWER_CELL_SQUARE_SIZE - fuelCellDimension + 1)
                .boxed()
                .map(x -> new CoordinateIndex(x, y));
    }

    int getTotalPowerForSquare(int[][] powerLevels, int leftOffset, int topOffset, int fuelCellDimension) {
        return IntStream.range(topOffset, topOffset + fuelCellDimension)
                .map(y -> getTotalPowerForSquareRow(powerLevels, leftOffset, y, fuelCellDimension))
                .sum();
    }

    private int getTotalPowerForSquareRow(int[][] powerLevels, int leftOffset, int top, int fuelCellDimension) {
        return IntStream.range(leftOffset, leftOffset + fuelCellDimension)
                .map(x -> powerLevels[top][x])
                .sum();
    }

    public FuelCellSquare getSquareWithLargestTotalPowerFixedCellDimensions(int serialNumber) {
        long startTime = System.currentTimeMillis();

        int[][] powerLevels = powerLevels(serialNumber);
        FuelCellSquare square = getSquareWithMaxTotalPower(powerLevels, FUEL_CELL_DIMENSION);

        long lastExecutionTime = System.currentTimeMillis() - startTime;
        log.info(String.format("Executed in %s ms", lastExecutionTime));
        return square;
    }

    public FuelCellSquare getSquareWithLargestTotalPowerVariableCellDimensions(int serialNumber) {
        long startTime = System.currentTimeMillis();

        int[][] powerLevels = powerLevels(serialNumber);

        FuelCellSquare square = IntStream.range(1, 301)
                .boxed()
                .map(dimension -> getSquareWithMaxTotalPower(powerLevels, dimension))
                .max(Comparator.comparingInt(it -> it.totalPower))
                .orElse(null);

        long lastExecutionTime = System.currentTimeMillis() - startTime;
        log.info(String.format("Executed in %s ms", lastExecutionTime));
        return square;
    }

}
