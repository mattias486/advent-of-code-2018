package com.mattias.advent.day11;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
class FuelCellSquare {
    final int x;
    final int y;
    final int totalPower;
    final int dimension;

    @Override
    public String toString() {
        return x + "," + y;
    }

    public String toStringWithDimension() {
        return x + "," + y + "," + dimension;
    }

}
