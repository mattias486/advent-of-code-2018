package com.mattias.advent.day12;

import com.mattias.advent.common.FileUtil;

import java.io.FileNotFoundException;
import java.util.List;

public class Day12 {

    FileUtil fileUtil = new FileUtil();

    FlowerPots getFlowerPots() throws FileNotFoundException {
        List<String> input = fileUtil.getFileContentAsStringList("day12input");
        String initialState = input.get(0).substring(15);
        List<String> rules = input.subList(2, input.size());
        boolean[] rulesAsBoolean = getRulesAsBooleans(rules);

        return FlowerPots.builder()
                .plants(getPlantsAsBoolean(initialState.toCharArray()))
                .rules(rulesAsBoolean)
                .build();
    }

    private boolean[] getPlantsAsBoolean(char[] plants) {
        boolean[] plantAsBoolean = new boolean[plants.length];
        for (int i = 0; i < plants.length; i++) {
            plantAsBoolean[i] = plants[i] == '#';
        }
        return plantAsBoolean;
    }

    private boolean[] getRulesAsBooleans(List<String> rules) {
        boolean[] result = new boolean[32];
        rules.forEach(ruleItem -> {
            boolean ruleResult = ruleItem.toCharArray()[9] == '#';
            result[getRuleFromString(ruleItem.substring(0, 5).toCharArray())] = ruleResult;
        });
        return result;
    }

    int getRuleFromString(char[] rule) {
        int ruleValue = 0;
        for (int i = 0; i < rule.length; i++) {
            if (rule[i] == '#') {
                ruleValue += (1 << Math.abs(i + 1 - rule.length));
            }
        }
        return ruleValue;
    }

    FlowerPots getFuturePlantGenerations(long generations) throws FileNotFoundException {
        FlowerPots flowerPots = getFlowerPots();
        for (long i = 0; i < generations; i++) {
            flowerPots.nextPlantGeneration(i);
        }
        return flowerPots;
    }
}
