package com.mattias.advent.day12;

import lombok.Builder;

import java.util.*;

@Builder(toBuilder = true)
public class FlowerPots {

    long zeroValuePlantIndex ;

    boolean[] rules;
    boolean[] plants;

    @Builder.Default
    Map<String, PlantGeneration> plantGenerationLog = new HashMap<>();

    long getSumOfPlants() {
        long sum = 0;
        for (int i = 0; i < plants.length; i++) {
            if (plants[i]) {
                sum += i + zeroValuePlantIndex;
            }
        }
        return sum;
    }

    /**
     * investigation has shown that there is only a limited number of plant states (101 different ones)
     * each plant states is always equal to the same plants for next generation and zero plant offset
     * we can start caching these plant states and fast forward if we've encountered the state before
     */
    void nextPlantGeneration(long i) {
        if (i > 101L) { //the plants get stuck in this generation, repeating itself to infinity
            zeroValuePlantIndex += 1;
            return;
        }

        String plantAsString = getPlantAsString(plants);
        /*
        if (plantGenerationLog.containsKey(plantAsString)) { //the plants get stuck in this generation, repeating itself to infinity
            PlantGeneration plantGeneration = plantGenerationLog.get(plantAsString);
            plants = plantGeneration.nextGeneration;
            zeroValuePlantIndex += plantGeneration.zeroPlantOffset;
            return;
        }*/

        byte zeroValuePlantIndexChange = 0;
        boolean newPlantToTheLeft = rules[getPlantRuleIndex(-1)];
        boolean newPlantToTheRight = rules[getPlantRuleIndex(plants.length)];
        int plantToTheLeftOffset = newPlantToTheLeft ? 1 : 0;
        boolean[] nextPlantGeneration = new boolean[plants.length + plantToTheLeftOffset + (newPlantToTheRight ? 1 : 0)];
        if (newPlantToTheLeft) {
            nextPlantGeneration[0] = true;
            zeroValuePlantIndexChange -= 1;
        }
        for (int currentPlant = 0; currentPlant < plants.length; currentPlant++) {
            int plantRuleIndex = getPlantRuleIndex(currentPlant);
            nextPlantGeneration[currentPlant + plantToTheLeftOffset] = rules[plantRuleIndex];
        }
        if (newPlantToTheRight) {
            nextPlantGeneration[plants.length + plantToTheLeftOffset] = true;
        }

        boolean isNoPlantToTheLeft = !nextPlantGeneration[0];
        if (isNoPlantToTheLeft) {
            nextPlantGeneration = Arrays.copyOfRange(nextPlantGeneration, 1, nextPlantGeneration.length);
            zeroValuePlantIndexChange += 1;
        }

        //the logging was only used for investigation, keeping it since it was a part of solving the task
        plantGenerationLog.put(plantAsString, PlantGeneration.builder()
                .nextGeneration(nextPlantGeneration)
                .zeroPlantOffset(zeroValuePlantIndexChange)
                .firstGenerationEncountered(i)
                .build());

        plants = nextPlantGeneration;
        zeroValuePlantIndex += zeroValuePlantIndexChange;
    }

    private String getPlantAsString(boolean[] plants) {
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < plants.length; i++) {
            builder.append(plants[i] ? "#" : ".");
        }
        return builder.toString();
    }

    int getPlantRuleIndex(int index) {
        boolean plantToTheLeft = index - 1 >= 0 && index - 1 < plants.length && plants[index - 1];
        boolean plantTwoToTheLeft = index - 2 >= 0 && index - 2 < plants.length && plants[index - 2];
        boolean plantAtCurrentPosition = index >= 0 && index < plants.length && plants[index];
        boolean plantToTheRight = index + 1 < plants.length && index + 1 >= 0 && plants[index + 1];
        boolean plantTwoToTheRight = index + 2 < plants.length && index + 2 >= 0 && plants[index + 2];

        int plantRuleInput = plantTwoToTheLeft ? 16 : 0;
        plantRuleInput += plantToTheLeft ? 8 : 0;
        plantRuleInput += plantAtCurrentPosition ? 4 : 0;
        plantRuleInput += plantToTheRight ? 2 : 0;
        plantRuleInput += plantTwoToTheRight ? 1 : 0;

        return plantRuleInput;
    }
}
