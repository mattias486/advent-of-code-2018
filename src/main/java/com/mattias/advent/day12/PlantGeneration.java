package com.mattias.advent.day12;

import lombok.Builder;

@Builder
public class PlantGeneration {
    boolean[] nextGeneration;
    long firstGenerationEncountered;
    byte zeroPlantOffset;
}
