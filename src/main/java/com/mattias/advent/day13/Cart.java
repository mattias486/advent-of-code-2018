package com.mattias.advent.day13;

import lombok.Builder;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.ArrayUtils;

import java.util.List;

@Builder(toBuilder = true)
@Slf4j
public class Cart {

    static char[] cartSymbols = new char[]{'<', '^', '>', 'v'};

    enum TurnPreference {
        LEFT, STRAIGHT, RIGHT;

        TurnPreference getNext() {
            if (this == LEFT) return STRAIGHT;
            if (this == STRAIGHT) return RIGHT;
            return LEFT;
        }

    }

    CartDirection getNewDirectionAfterIntersection() {
        switch (turnPreference) {
            case LEFT:
                return direction.getLeft();
            case STRAIGHT:
                return direction;
            case RIGHT:
                return direction.getRight();
        }
        return null;
    }

    @Builder.Default TurnPreference turnPreference = TurnPreference.LEFT;
    CartDirection direction;
    Position position;

    void moveCart() {
        switch (direction) {
            case LEFT:
                position.x -= 1;
                break;
            case UP:
                position.y -= 1;
                break;
            case RIGHT:
                position.x += 1;
                break;
            case DOWN:
                position.y += 1;
                break;
        }
    }

    static boolean isCart(char c) {
        return ArrayUtils.contains(cartSymbols, c);
    }

    static CartDirection getDirectionForCart(char c) {
        switch (c) {
            case '<':
                return CartDirection.LEFT;
            case '>':
                return CartDirection.RIGHT;
            case '^':
                return CartDirection.UP;
            case 'v':
                return CartDirection.DOWN;
            default:
                throw new IllegalArgumentException(c + " is not a cart character!");
        }
    }

    void switchDirectionDueToCurve(List<Track> tracks) {
        if (tracks.size() != 2) {
            log.warn("Unexpected size of tracks for curve, expected 2 but got : " + tracks.size());
            return;
        }
        switch (direction) {
            case LEFT:
            case RIGHT:
                turnCurveVertical(tracks);
                break;
            case UP:
            case DOWN:
                turnCurveHorizontal(tracks);
                break;
        }
    }

    private void turnCurveHorizontal(List<Track> tracks) {
        Track track = getTrack(tracks, Track.TrackDirection.HORIZONTAL);
        direction = position.equals(track.startPosition) ? CartDirection.RIGHT : CartDirection.LEFT;
    }

    private void turnCurveVertical(List<Track> tracks) {
        Track track = getTrack(tracks, Track.TrackDirection.VERTICAL);
        direction = position.equals(track.startPosition) ? CartDirection.DOWN : CartDirection.UP;
    }

    private Track getTrack(List<Track> tracks, Track.TrackDirection direction) {
        return tracks.stream()
                .filter(track -> track.direction == direction)
                .findFirst()
                .orElse(null);
    }

    void switchDirectionDueToIntersection() {
        direction = getNewDirectionAfterIntersection();
        turnPreference = turnPreference.getNext();
    }
}
