package com.mattias.advent.day13;

public enum CartDirection {
    LEFT, UP, RIGHT, DOWN;

    CartDirection getLeft() {
        switch (this) {
            case LEFT:
                return DOWN;
            case UP:
                return LEFT;
            case RIGHT:
                return UP;
            case DOWN:
                return RIGHT;
        }
        return null;
    }

    CartDirection getRight() {
        switch (this) {
            case LEFT:
                return UP;
            case UP:
                return RIGHT;
            case RIGHT:
                return DOWN;
            case DOWN:
                return LEFT;
        }
        return null;
    }
}
