package com.mattias.advent.day13;

import com.mattias.advent.common.FileUtil;
import lombok.Builder;
import lombok.extern.slf4j.Slf4j;

import java.io.FileNotFoundException;
import java.util.*;
import java.util.function.Predicate;
import java.util.stream.Collectors;

@Slf4j
@Builder
public class Day13 {

    public static final int MAX_NUMBER_OF_CART_MOVES = 100000;

    @Builder.Default
    FileUtil fileUtil = new FileUtil();
    @Builder.Default
    TrackCreator trackCreator = new TrackCreator();

    List<Cart> carts;
    List<Track> tracks;

    List<Position> intersections;
    List<Position> curves;

    Map<Position, List<Track>> tracksWithCurves;
    Map<Position, List<Track>> tracksWithIntersections;

    void setupCartsAndTracks() throws FileNotFoundException {
        List<String> day13input = getDay13Input();
        carts = getCarts(day13input);
        tracks = trackCreator.getTracks(day13input);
        curves = getCurves(day13input);
        intersections = getIntersections(day13input);

        tracksWithCurves = trackCreator.getTracksConnectedAtPositions(tracks, curves);
        tracksWithIntersections = trackCreator.getTracksConnectedAtPositions(tracks, intersections);
    }

    List<Position> getIntersections(List<String> rows) {
        return getPositionsForCharacters(rows, TrackCreator::isIntersection);
    }

    List<Position> getCurves(List<String> rows) {
        return getPositionsForCharacters(rows, TrackCreator::isCurve);
    }

    private List<Position> getPositionsForCharacters(List<String> rows, Predicate<Character> filter) {
        List<Position> result = new ArrayList<>();
        for (int row = 0; row < rows.size(); row++) {
            char[] rowContent = rows.get(row).toCharArray();
            for (int column = 0; column < rowContent.length; column++) {
                if (filter.test(rowContent[column])) {
                    result.add(getPosition(column, row));
                }
            }
        }
        return result;
    }

    private Position getPosition(int x, int y) {
        return Position.builder()
                .x(x)
                .y(y)
                .build();
    }


    void moveCartsUntilCollision() {
        int i = 0;
        for (; i < MAX_NUMBER_OF_CART_MOVES; i++) {
            if (moveCartsBreakOnCollision()) {
                log.info("Stopping after collision at turn : " + i);
                return;
            }
        }
    }

    void moveCartsUntilOnlyOneCartRemain() {
        int i = 0;
        for (; i < MAX_NUMBER_OF_CART_MOVES; i++) {
            if (moveCartsAndRemoveCollidedCarts()) {
                log.info("Stopping at turn : " + i);
                log.info("Position of last cart after turn : " + carts.get(0).position);
                return;
            }
        }
    }

    boolean moveCartsAndRemoveCollidedCarts() {
        carts.sort(sortCartsByPosition());
        List<Cart> removedCarts = new ArrayList<>();
        for(Cart cartItem : carts) {
            if(removedCarts.contains(cartItem)) {
                continue;
            }
            cartItem.moveCart();
            removedCarts.addAll(removeCollidedCarts());
            checkForSwitchingTrack(cartItem);
        }
        carts.removeAll(removedCarts);
        return carts.size() == 1;
    }

    private Comparator<Cart> sortCartsByPosition() {
        return (first, second) -> {
            if(first.position.y == second.position.y) {
                if(first.position.x == second.position.x) {
                    return 0;
                }
                return first.position.x < second.position.x ? -1 : 1;
            }
            return first.position.y < second.position.y ? -1 : 1;
        };
    }

    private List<Cart> removeCollidedCarts() {
        return getCollidedCarts(carts);
    }

    boolean moveCartsBreakOnCollision() {
        carts.sort(sortCartsByPosition());
        for(Cart cartItem : carts) {
            cartItem.moveCart();
            if (checkForCollision(this.carts)) {
                return true;
            }
            checkForSwitchingTrack(cartItem);
        }
        return false;
    }

    private void checkForSwitchingTrack(Cart cart) {
        if(tracksWithCurves.containsKey(cart.position)) {
            cart.switchDirectionDueToCurve(tracksWithCurves.get(cart.position));
        } else if(tracksWithIntersections.containsKey(cart.position)) {
            cart.switchDirectionDueToIntersection();
        }
    }

    boolean checkForCollision(List<Cart> carts) {
        return getCollidedCarts(carts).size() > 0;
    }

    List<Cart> getCollidedCarts(List<Cart> carts) {
        Map<Position, List<Cart>> cartsOnPosition = new HashMap<>();
        carts.forEach(cart -> {
            cartsOnPosition.putIfAbsent(cart.position, new ArrayList<>());
            cartsOnPosition.get(cart.position).add(cart);
        });
        return cartsOnPosition.values().stream().filter(it -> it.size() > 1)
                .flatMap(Collection::stream)
                .collect(Collectors.toList());
    }

    List<String> getDay13Input() throws FileNotFoundException {
        return fileUtil.getFileContentAsStringList("day13input");
    }

    List<Cart> getCarts(List<String> day13input) {
        List<Cart> carts = new ArrayList<>();
        for (int y = 0; y < day13input.size(); y++) {
            char[] row = day13input.get(y).toCharArray();
            for (int x = 0; x < row.length; x++) {
                if (Cart.isCart(row[x])) {
                    carts.add(createCart(x, y, Cart.getDirectionForCart(row[x])));
                }

            }
        }
        return carts;
    }

    private Cart createCart(int x, int y, CartDirection direction) {
        return Cart.builder()
                .position(getPosition(x, y))
                .direction(direction)
                .build();
    }
}
