package com.mattias.advent.day13;

import lombok.Builder;
import lombok.EqualsAndHashCode;

@Builder
@EqualsAndHashCode
class Position {
    int x;
    int y;

    public String toString() {
        return x + "," + y;
    }
}
