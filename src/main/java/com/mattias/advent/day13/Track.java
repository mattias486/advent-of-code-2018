package com.mattias.advent.day13;

import lombok.Builder;

@Builder
public class Track {

    enum TrackDirection {
        VERTICAL, HORIZONTAL;
    }

    Position startPosition;
    Position endPosition;

    TrackDirection direction;

    //size 1 for curves, size 3 for intersections
    Track[] tracksConnectedAtStart;
    Track[] tracksConnectedAtEnd;
}
