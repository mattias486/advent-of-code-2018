package com.mattias.advent.day13;

import lombok.extern.slf4j.Slf4j;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

@Slf4j
public class TrackCreator {

    List<Track> getTracks(List<String> day13input) {
        List<Track> tracks = new ArrayList<>(getHorizontalTracks(day13input));
        tracks.addAll(getVerticalTracks(flipColumnsToRows(day13input.toArray(new String[]{}))));
        connectTracks(tracks);
        return tracks;
    }

    private void connectTracks(List<Track> tracks) {
        List<Position> positionsWithTracks = getPositionsWhereTracksAreConnected(tracks);
        Map<Position, List<Track>> tracksOnPosition = getTracksConnectedAtPositions(tracks, positionsWithTracks);
        mapConnectedTracks(tracks, tracksOnPosition);
    }

    private void mapConnectedTracks(List<Track> tracks, Map<Position, List<Track>> tracksOnPosition) {
        tracks.forEach(track -> {
                    mapTracksConnectingAtStart(tracksOnPosition, track);
                    mapTracksConnectedAtEnd(tracksOnPosition, track);
                }
        );
    }

    Map<Position, List<Track>> getTracksConnectedAtPositions(List<Track> tracks, List<Position> positionsWithTracks) {
        Map<Position, List<Track>> startPositionMap = getTracksOnStartPosition(tracks, positionsWithTracks);
        Map<Position, List<Track>> endPositionMap = getTracksOnEndPosition(tracks, positionsWithTracks);
        return positionsWithTracks.stream()
                .collect(Collectors.toMap(Function.identity(),
                        position -> mergeValuesInMaps(position, startPositionMap, endPositionMap)));
    }

    private List<Track> mergeValuesInMaps(Position position, Map<Position, List<Track>> startPositionMap, Map<Position, List<Track>> endPositionMap) {
        List<Track> mergedTracks = new ArrayList<>(startPositionMap.get(position));
        mergedTracks.addAll(endPositionMap.get(position));
        return mergedTracks;
    }

    private Map<Position, List<Track>> getTracksOnEndPosition(List<Track> tracks, List<Position> positionsWithTracks) {
        return positionsWithTracks.stream()
                .collect(Collectors.toMap(position -> position, position -> tracks.stream()
                        .filter(track -> position.equals(track.endPosition))
                        .collect(Collectors.toList())));
    }

    private Map<Position, List<Track>> getTracksOnStartPosition(List<Track> tracks, List<Position> positionsWithTracks) {
        return positionsWithTracks.stream()
                .collect(Collectors.toMap(position -> position, position -> tracks.stream()
                        .filter(track -> position.equals(track.startPosition))
                        .collect(Collectors.toList())));
    }

    private List<Position> getPositionsWhereTracksAreConnected(List<Track> tracks) {
        return tracks.stream()
                .flatMap(track -> Stream.of(track.startPosition, track.endPosition))
                .distinct()
                .collect(Collectors.toList());
    }

    private void mapTracksConnectedAtEnd(Map<Position, List<Track>> tracksOnPosition, Track track) {
        track.tracksConnectedAtEnd = tracksOnPosition.get(track.endPosition)
                .stream()
                .filter(trackOnPosition -> trackOnPosition != track)
                .toArray(Track[]::new);
    }

    private void mapTracksConnectingAtStart(Map<Position, List<Track>> tracksOnPosition, Track track) {
        track.tracksConnectedAtStart = tracksOnPosition.get(track.startPosition)
                .stream()
                .filter(trackOnPosition -> trackOnPosition != track)
                .toArray(Track[]::new);
    }

    /**
     * we rotate the input to read columns as rows
     */
    List<String> flipColumnsToRows(String[] input) {
        char[][] output = new char[input[0].length()][input.length];
        for (int column = 0; column < input.length; column++) {
            char[] rowContent = input[column].toCharArray();
            for (int row = 0; row < rowContent.length; row++) {
                output[row][column] = rowContent[row];
            }
        }
        return IntStream.range(0, output.length)
                .boxed()
                .map(index -> new String(output[index]))
                .collect(Collectors.toList());
    }

    private List<Track> getVerticalTracks(List<String> columns) {
        return IntStream.range(0, columns.size())
                .boxed()
                .flatMap(columnIndex -> getVerticalTrackFromRow(columnIndex, columns.get(columnIndex).toCharArray()))
                .collect(Collectors.toList());
    }

    private List<Track> getHorizontalTracks(List<String> rows) {
        return IntStream.range(0, rows.size())
                .boxed()
                .flatMap(columnIndex -> getHorizontalTrackFromColumn(columnIndex, rows.get(columnIndex).toCharArray()))
                .collect(Collectors.toList());
    }

    private Stream<Track> getVerticalTrackFromRow(int columnIndex, char[] columns) {
        List<Integer> startOrEnd = IntStream.range(0, columns.length)
                .boxed()
                .filter(rowIndex -> isTrackStartOrEnd(columns[rowIndex]))
                .flatMap(rowIndex -> addIntersectionsTwice(rowIndex, columns[rowIndex]))
                .collect(Collectors.toList());

        return getVerticalTrackFromStartAndEndPosition(columnIndex, startOrEnd);
    }

    private Stream<Track> getHorizontalTrackFromColumn(int columnIndex, char[] column) {
        List<Integer> startOrEnd = IntStream.range(0, column.length)
                .boxed()
                .filter(it -> isTrackStartOrEnd(column[it]))
                .flatMap(it -> addIntersectionsTwice(it, column[it]))
                .collect(Collectors.toList());

        return getHorizontalTrackFromStartAndEndPosition(columnIndex, startOrEnd);
    }

    /**
     * we add start position for intersections twice since we want to match start/end positions on a row. intersections
     * are both start and end positions for tracks
     */
    private Stream<Integer> addIntersectionsTwice(Integer column, char c) {
        if (isIntersection(c)) {
            return Stream.of(column, column);
        }
        return Stream.of(column);
    }


    private Stream<Track> getHorizontalTrackFromStartAndEndPosition(int rowIndex, List<Integer> startAndEndIndexes) {
        if (startAndEndIndexes.size() % 2 == 1) {
            log.error("Unexpected size of start and end index for row : " + rowIndex);
            return Stream.empty();
        }
        return IntStream.range(0, startAndEndIndexes.size())
                .filter(it -> it % 2 == 0)
                .boxed()
                .map(it -> getHorizontalTrack(rowIndex, startAndEndIndexes.get(it), startAndEndIndexes.get(it + 1)));
    }

    private Stream<Track> getVerticalTrackFromStartAndEndPosition(int columnIndex, List<Integer> startAndEndIndexes) {
        if (startAndEndIndexes.size() % 2 == 1) {
            log.error("Unexpected size of start and end index for row : " + columnIndex);
            return Stream.empty();
        }
        return IntStream.range(0, startAndEndIndexes.size())
                .filter(it -> it % 2 == 0)
                .boxed()
                .map(it -> getVerticalTrack(columnIndex, startAndEndIndexes.get(it), startAndEndIndexes.get(it + 1)));
    }

    private Track getHorizontalTrack(int rowIndex, int startColumn, int endColumn) {
        return getTrack(startColumn, rowIndex, endColumn, rowIndex, Track.TrackDirection.HORIZONTAL);
    }

    private Track getVerticalTrack(int columnIndex, int startRow, int endRow) {
        return getTrack(columnIndex, startRow, columnIndex, endRow, Track.TrackDirection.VERTICAL);
    }

    private Track getTrack(int startX, int startY, int endX, int endY, Track.TrackDirection direction) {
        return Track.builder()
                .startPosition(Position.builder()
                        .y(startY)
                        .x(startX)
                        .build())
                .endPosition(Position.builder()
                        .y(endY)
                        .x(endX)
                        .build())
                .direction(direction)
                .build();

    }

    static boolean isTrackStartOrEnd(char c) {
        return isCurve(c)
                || isIntersection(c);
    }

    static boolean isCurve(char c) {
        return c == '/'
                || c == '\\';
    }

    static boolean isIntersection(char c) {
        return c == '+';
    }
}
