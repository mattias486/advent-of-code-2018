package com.mattias.advent.day14;

import lombok.Builder;
import lombok.extern.slf4j.Slf4j;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.IntStream;

@Builder
@Slf4j
public class Day14 {

    static final int MAX_NUMBER_OF_CREATIONS = 100_000_000;

    @Builder.Default
    int elf1RecipeIndex = 0;
    @Builder.Default
    int elf2RecipeIndex = 1;

    @Builder.Default
    List<Integer> recipeScores = new ArrayList<>(Arrays.asList(3, 7));


    String getScoreOfThe10LastRecipes(int recipeCreations) {
        return getScoreOfRecipes(recipeCreations, 10);
    }

    String getScoreOfRecipes(int recipeCreations, int scoreLength) {
        for (int i = 0; i < recipeCreations + scoreLength; i++) {
            createRecipes();
        }
        return getScoreOfRecipesAtIndex(recipeCreations, scoreLength);
    }

    String getScoreOfRecipesAtIndex(int index, int numberOfRecipes) {
        StringBuilder builder = new StringBuilder();
        IntStream.range(index, index + numberOfRecipes)
                .forEach(it -> builder.append(recipeScores.get(it)));
        return builder.toString();
    }

    void createRecipes() {
        int sumOfRecipes = getSumOfRecipes();
        addNewRecipes(sumOfRecipes);
        updateElfCurrentRecipes();
    }

    void addNewRecipes(int sumOfRecipes) {
        if (sumOfRecipes >= 10) {
            recipeScores.add(1);
            recipeScores.add(sumOfRecipes % 10);
        } else {
            recipeScores.add(sumOfRecipes);

        }
    }

    private void updateElfCurrentRecipes() {
        elf1RecipeIndex = getNewElfRecipeIndex(elf1RecipeIndex);
        elf2RecipeIndex = getNewElfRecipeIndex(elf2RecipeIndex);
    }

    int getNewElfRecipeIndex(int currentIndex) {
        return (currentIndex + recipeScores.get(currentIndex) + 1) % recipeScores.size();
    }

    int getSumOfRecipes() {
        return recipeScores.get(elf1RecipeIndex) + recipeScores.get(elf2RecipeIndex);
    }

    int getNumberOfRecipesLeftOfSequence(String sequence) {
        for (int i = 0; i < MAX_NUMBER_OF_CREATIONS; i++) {
            if (getLast7Recipes().contains(sequence)) {
                int indexInLast7 = getLast7Recipes().indexOf(sequence);
                return recipeScores.size() - 7 + indexInLast7;
            }
            createRecipes();
        }
        log.info("Did not find sequence");
        return 0;
    }

    //7 instead of 6 since each turn could add two recipes
    String getLast7Recipes() {
        if (recipeScores.size() < 7) {
            return "";
        }
        StringBuilder builder = new StringBuilder();
        IntStream.range(recipeScores.size() - 7, recipeScores.size())
                .forEach(it -> builder.append(recipeScores.get(it)));
        return builder.toString();
    }
}
