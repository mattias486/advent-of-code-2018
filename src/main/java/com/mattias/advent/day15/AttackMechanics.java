package com.mattias.advent.day15;

import lombok.extern.slf4j.Slf4j;

import java.util.List;
import java.util.stream.Collectors;

@Slf4j
public class AttackMechanics {

    boolean isEnemiesInRange(FightingUnit unit, List<FightingUnit> enemies) {
        List<FightingUnit> enemiesInRangeForAttack = getEnemiesInRangeForAttack(unit, enemies);
        return !enemiesInRangeForAttack.isEmpty();
    }

    void playFightingUnitAttackRound(FightingUnit unit, List<FightingUnit> enemies, List<Position> emptyPositions) {
        List<FightingUnit> enemiesInRangeForAttack = getEnemiesInRangeForAttack(unit, enemies);
        if (enemiesInRangeForAttack.isEmpty()) {
            //log.info(String.format("[%s] No enemies in range of attack.", unit));
            return;
        }
        FightingUnit target = enemiesInRangeForAttack.get(0);
        attackEnemy(unit, target);
        if (target.health < 1) {
            Position deadEnemyPosition = target.position;
            enemies.remove(target);
            emptyPositions.add(deadEnemyPosition);
            //log.info(String.format("Team %s member at %s killed by attacking team %s at %s", target.team, target.position, unit.team, unit.position));
        }
    }

    List<FightingUnit> getEnemiesInRangeForAttack(FightingUnit unit, List<FightingUnit> enemies) {
        return enemies.stream()
                .filter(FightingUnit::isStillAlive)
                .filter(enemy -> isAdjacentTo(enemy.position, unit.position))
                .sorted(FightingUnit.orderByHpAndThenPosition)
                .collect(Collectors.toList());
    }

    private void attackEnemy(FightingUnit attacker, FightingUnit target) {
        target.health -= attacker.attackPower;
    }

    boolean isAdjacentTo(Position first, Position second) {
        return (Math.abs(first.x - second.x) == 1 && first.y == second.y) ||
                (Math.abs(first.y - second.y) == 1 && first.x == second.x);

    }
}
