package com.mattias.advent.day15;

import com.mattias.advent.common.FileUtil;
import com.mattias.advent.day15.exception.DeadElfException;
import lombok.extern.slf4j.Slf4j;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Slf4j
public class Day15 {

    FileUtil fileUtil = new FileUtil();
    GameMechanics gameMechanics = new GameMechanics();

    char[][] getInput() throws FileNotFoundException {
        return fileUtil.getFileContentAsCharacterArray("day15input");
    }

    GameResult playGame(char[][] gameInput, int elfAttackPower, GameState.GameMode gameMode) {

        GameState gameState = getInitialStateWithAttackPower(gameInput, elfAttackPower, gameMode);

        int numberOfCompletedRounds = gameMechanics.playGame(gameState);

        GameResult.Team winningTeam = getWinningTeam(gameState);
        int totalHpForTeam = getTotalHpForTeam(gameState, winningTeam);
        return GameResult.builder()
                .numberOfCompletedRounds(numberOfCompletedRounds)
                .totalHpRemaining(totalHpForTeam)
                .totalScore(numberOfCompletedRounds * totalHpForTeam)
                .winningTeam(winningTeam)
                .elfAttackPower(elfAttackPower)
                .build();
    }

    GameResult playGameWithoutDeadElves(char[][] gameInput) {
        int maxAttackPowerWithElfDeaths = 3;
        int minimumAttackPowerWithoutElfDeaths = 300; //one shot

        Map<Integer, GameResult> gameResultWithoutElfDeaths = new HashMap<>();
        while(!foundMinimumAttackPowerToKeepElvesAlive(maxAttackPowerWithElfDeaths, minimumAttackPowerWithoutElfDeaths)) {
            int attackPower = getAttackPowerForNextGame(maxAttackPowerWithElfDeaths, minimumAttackPowerWithoutElfDeaths);
            //log.info("Playing game with attack power : " + attackPower);
            try {
                GameResult gameResult = playGame(gameInput, attackPower, GameState.GameMode.ELVES_SHOULD_NOT_DIE);
                gameResultWithoutElfDeaths.put(attackPower, gameResult);
                minimumAttackPowerWithoutElfDeaths = attackPower;
            } catch (DeadElfException e) {
                maxAttackPowerWithElfDeaths = attackPower;
            }
        }
        //log.info("Found minimum attack power required : " + minimumAttackPowerWithoutElfDeaths);
        return gameResultWithoutElfDeaths.get(minimumAttackPowerWithoutElfDeaths);
    }

    int getAttackPowerForNextGame(int maxWithDeaths, int minWithoutDeaths) {
        return maxWithDeaths + ((minWithoutDeaths - maxWithDeaths) / 2);
    }

    boolean foundMinimumAttackPowerToKeepElvesAlive(int max, int minimum) {
        return minimum - max == 1;
    }

    GameResult.Team getWinningTeam(GameState gameState) {
        if(gameState.goblins.isEmpty()) {
            return GameResult.Team.ELVES;
        }
        if(gameState.elves.isEmpty()) {
            return GameResult.Team.GOBLINS;
        }
        log.warn("Not able to determine winning team. Both team has members alive.");
        return null;
    }

    int getTotalHpForTeam(GameState gameState, GameResult.Team team) {
        return (GameResult.Team.ELVES == team ? gameState.elves : gameState.goblins).stream()
                .mapToInt(it -> it.health)
                .sum();

    }

    private GameState getInitialStateWithAttackPower(char[][] gameInput, int elfAttackPower, GameState.GameMode gameMode) {
        return GameState.builder()
                .elves(getElves(gameInput, elfAttackPower))
                .goblins(getGoblins(gameInput))
                .gameMode(gameMode)
                .emptyPositions(getEmptyFields(gameInput))
                .build();
    }

    List<FightingUnit> getElves(char[][] input, int attackPower) {
        return getPositionsOfCharacter(input, 'E').stream()
                .map(it -> FightingUnit.builder()
                        .position(it)
                        .team(GameResult.Team.ELVES)
                        .attackPower(attackPower)
                        .build())
                .collect(Collectors.toList());
    }

    List<FightingUnit> getGoblins(char[][] input) {
        return getPositionsOfCharacter(input, 'G').stream()
                .map(it -> FightingUnit.builder()
                        .position(it)
                        .team(GameResult.Team.GOBLINS)
                        .build())
                .collect(Collectors.toList());
    }

    private List<Position> getPositionsOfCharacter(char input[][], char character) {
        List<Position> units = new ArrayList<>();
        for (int row = 0; row < input.length; row++) {
            for (int column = 0; column < input[0].length; column++) {
                if (input[row][column] == character) {
                    units.add(new Position(column, row));
                }
            }
        }
        return units;
    }

    public List<Position> getWalls(char[][] input) {
        return getPositionsOfCharacter(input, '#');
    }

    public List<Position> getEmptyFields(char[][] input) {
        return getPositionsOfCharacter(input, '.');
    }

}
