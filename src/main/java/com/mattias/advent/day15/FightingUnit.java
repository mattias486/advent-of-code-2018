package com.mattias.advent.day15;

import lombok.Builder;
import lombok.Data;

import java.util.Comparator;

import static java.util.Comparator.comparingInt;

@Data
@Builder
class FightingUnit {

    public static final Comparator<FightingUnit> orderByHpAndThenPosition = comparingInt(FightingUnit::getHealth)
            .thenComparingInt(FightingUnit::getYCoordinate)
            .thenComparingInt(FightingUnit::getXCoordinate);

    public static final Comparator<FightingUnit> orderByPosition = comparingInt(FightingUnit::getYCoordinate)
            .thenComparingInt(FightingUnit::getXCoordinate);

    @Builder.Default int attackPower = 3;
    @Builder.Default int health = 200;
    Position position;
    GameResult.Team team;

    public boolean isStillAlive() {
        return health > 0;
    }

    public int getXCoordinate() {
        return position.x;
    }

    public int getYCoordinate() {
        return position.y;
    }
}
