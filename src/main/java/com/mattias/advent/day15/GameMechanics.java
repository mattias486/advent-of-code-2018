package com.mattias.advent.day15;

import com.mattias.advent.day15.exception.DeadElfException;
import lombok.extern.slf4j.Slf4j;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Slf4j
class GameMechanics {

    GameState gameState;
    MovementMechanics movementMechanics = new MovementMechanics();
    AttackMechanics attackMechanics = new AttackMechanics();

    private static int MAX_GAME_ROUNDS = 10000;

    int playGame(GameState gameState) {
        this.gameState = gameState;

        int completedRounds = 0;
        for (int i = 0; i < MAX_GAME_ROUNDS; i++) {
            if (!playRound(gameState)) {
                return completedRounds;
            }
            completedRounds++;
        }
        return completedRounds;
    }

    boolean playRound(GameState gameState) {
        int initialElves = gameState.elves.size();
        for(FightingUnit unit : getFightingUnitsOrdered()) {

            if(!isBothTeamsStillAlive(gameState)) {
                return false;
            }
            //the list of units in GameState, where units gets removed differs from this sorted collection we create at
            //the start of each game round, so we have to check if this unit has not been killed yet
            if(!unit.isStillAlive()) {
                continue;
            }
            playUnitTurn(unit);
            abortOnElfDeath(gameState, initialElves);
        }
        return true;
    }

    void abortOnElfDeath(GameState gameState, int initialElves) {
        if(gameState.gameMode == GameState.GameMode.ELVES_SHOULD_NOT_DIE &&
            gameState.elves.size() < initialElves) {
            throw new DeadElfException();
        }
    }

    private boolean isBothTeamsStillAlive(GameState gameState) {
        return gameState.goblins.stream().anyMatch(FightingUnit::isStillAlive)
                && gameState.elves.stream().anyMatch(FightingUnit::isStillAlive);
    }

    List<FightingUnit> getFightingUnitsOrdered() {
        return Stream.concat(gameState.elves.stream(), gameState.goblins.stream())
                .sorted(FightingUnit.orderByPosition)
                .collect(Collectors.toList());
    }

    void playUnitTurn(FightingUnit unit) {
        List<FightingUnit> enemies = getEnemies(unit);
        boolean isInRangeOfEnemies = attackMechanics.isEnemiesInRange(unit, enemies);
        if(!isInRangeOfEnemies) {
            movementMechanics.playMoveToClosestEnemy(unit, enemies, gameState.emptyPositions);
        }
        attackMechanics.playFightingUnitAttackRound(unit, enemies, gameState.emptyPositions);
    }

    private List<FightingUnit> getEnemies(FightingUnit unit) {
        switch (unit.team) {
            case ELVES:
                return gameState.goblins;
            case GOBLINS:
                return gameState.elves;
        }
        return null;
    }
}
