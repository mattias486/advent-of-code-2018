package com.mattias.advent.day15;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
class GameResult {
    enum Team {
        ELVES, GOBLINS
    }

   int totalScore;
   int totalHpRemaining;
   int numberOfCompletedRounds;
   int elfAttackPower;
   Team winningTeam;
}
