package com.mattias.advent.day15;

import lombok.Builder;

import java.util.List;

@Builder
class GameState {

    enum GameMode {
        EQUAL_ATTACK_POWER,
        ELVES_SHOULD_NOT_DIE
    }

    List<FightingUnit> elves;
    List<FightingUnit> goblins;
    List<Position> emptyPositions;

    @Builder.Default GameMode gameMode = GameMode.EQUAL_ATTACK_POWER;
}
