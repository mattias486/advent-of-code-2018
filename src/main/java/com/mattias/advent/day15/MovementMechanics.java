package com.mattias.advent.day15;

import lombok.extern.slf4j.Slf4j;

import java.util.*;
import java.util.stream.Collectors;

@Slf4j
public class MovementMechanics {

    ShortestPath shortestPath = new ShortestPath();

    Position getPositionToMoveTo(FightingUnit unit, List<FightingUnit> enemies, List<Position> emptyPositions) {
        Map<Position, Integer> unitDistancesToPosition = shortestPath.calculateDistances(unit.position, emptyPositions);
        Map<FightingUnit, Position> closestAttackPositions = getClosestPlaceOfAttack(unitDistancesToPosition, enemies, emptyPositions);
        int closestUnitAttackPosition = closestAttackPositions.values().stream()
                .map(unitDistancesToPosition::get)
                .filter(Objects::nonNull)
                .mapToInt(it -> it)
                .min()
                .orElse(0);
        if (closestUnitAttackPosition == 0) {
            //log.info(String.format("[%s] Not able find target to move to. Standing still", unit));
            return null;
        }

        List<Position> unitAttackPositionsWithShortestDistance = closestAttackPositions.values()
                .stream()
                .filter(position -> closestUnitAttackPosition == unitDistancesToPosition.get(position))
                .collect(Collectors.toList());
        if(unitAttackPositionsWithShortestDistance.size() > 1) {
            //log.info("More then one possible target at equal range");
        }
        return getFirstPositionToMoveTowardsTarget(unit.position, unitAttackPositionsWithShortestDistance, shortestPath.getParentPosition());
    }

    Position getFirstPositionToMoveTowardsTarget(Position currentPosition, List<Position> destinations, Map<Position, Position> parentPosition) {
        return destinations.stream()
                .filter(parentPosition::containsKey)
                .map(it -> getFirstPositionTowardsEndPosition(it, currentPosition, parentPosition))
                .min(Position.orderByPosition)
                .orElse(null);
    }

    Position getFirstPositionTowardsEndPosition(Position destination, Position currentPosition, Map<Position, Position> parentPosition) {
        Position position = destination;
        while(parentPosition.get(position) != null && !parentPosition.get(position).equals(currentPosition)) {
            position = parentPosition.get(position);
        }
        return position;
    }

    Map<FightingUnit, Position> getClosestPlaceOfAttack(Map<Position, Integer> unitDistancesToPosition, List<FightingUnit> enemies, List<Position> emptyPositions) {
        Map<FightingUnit, Position> result = new HashMap<>();
        enemies.forEach(enemy -> {
            List<Position> validPositionsOfAttack = getValidPositionsOfAttack(enemy.position, emptyPositions);
            if (!validPositionsOfAttack.isEmpty()) {
                Position closestPositionToEnemy = selectClosestPositionToEnemy(unitDistancesToPosition, validPositionsOfAttack);
                if (closestPositionToEnemy != null) {
                    result.put(enemy, closestPositionToEnemy);
                }
            }
        });
        return result;
    }

    Position selectClosestPositionToEnemy(Map<Position, Integer> unitDistancesToPosition, List<Position> validPositionsOfAttack) {
        Integer minDistanceToAttackPosition = getClosestDistanceToEnemy(unitDistancesToPosition, validPositionsOfAttack);
        if (minDistanceToAttackPosition == 0) {
            return null;
        }
        return validPositionsOfAttack.stream()
                .filter(position -> minDistanceToAttackPosition.equals(unitDistancesToPosition.get(position)))
                .min(Position.orderByPosition)
                .orElse(null);
    }

    private int getClosestDistanceToEnemy(Map<Position, Integer> unitDistancesToPosition, List<Position> validAttackPositionsOfEnemy) {
        return validAttackPositionsOfEnemy.stream()
                .map(unitDistancesToPosition::get)
                .filter(Objects::nonNull)
                .mapToInt(it -> it)
                .min()
                .orElse(0);
    }

    List<Position> getValidPositionsOfAttack(Position unitPosition, List<Position> emptyPositions) {
        List<Position> positions = new ArrayList<>();
        //we add attack positions in "reading order" so we know first position in list is to be prioritized if distance to the positions are equal
        addPositionIfEmpty(positions, new Position(unitPosition.x, unitPosition.y - 1), emptyPositions);
        addPositionIfEmpty(positions, new Position(unitPosition.x - 1, unitPosition.y), emptyPositions);
        addPositionIfEmpty(positions, new Position(unitPosition.x + 1, unitPosition.y), emptyPositions);
        addPositionIfEmpty(positions, new Position(unitPosition.x, unitPosition.y + 1), emptyPositions);
        return positions;
    }

    private void addPositionIfEmpty(List<Position> attackPositions, Position unitPosition, List<Position> emptyPositions) {
        if (emptyPositions.contains(unitPosition)) {
            attackPositions.add(unitPosition);
        }
    }


    void playMoveToClosestEnemy(FightingUnit unit, List<FightingUnit> enemies, List<Position> emptyPositions) {
        Position moveTo = getPositionToMoveTo(unit, enemies, emptyPositions);
        if(moveTo == null) {
            return;
        }
        Position moveFrom = unit.position;
        unit.position = moveTo;
        emptyPositions.remove(moveTo);
        emptyPositions.add(moveFrom);
    }
}
