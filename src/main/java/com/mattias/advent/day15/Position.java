package com.mattias.advent.day15;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;

import java.util.Comparator;

import static java.util.Comparator.comparingInt;

@EqualsAndHashCode
@AllArgsConstructor
@Getter
class Position {

    public static final Comparator<Position> orderByPosition = comparingInt(Position::getY)
            .thenComparingInt(Position::getX);

    int x;
    int y;

    public String toString() {
        return x + "," + y;
    }
}
