package com.mattias.advent.day15;

import lombok.Builder;

import java.util.*;

/**
 * shortest path solved by breadth first search
 */
public class ShortestPath {

    Queue<Position> queue;
    Set<Position> visited;
    Map<Position, Integer> distances;

    Map<Position, Position> parentPosition;

    List<Position> validEmptyPositions;

    public Map<Position, Integer> calculateDistances(Position startNode, List<Position> validEmptyPositions) {
        this.validEmptyPositions = validEmptyPositions;
        setupInternals();
        initiateStartNode(startNode);
        while (!queue.isEmpty()) {
            Position currentNode = queue.poll();
            for (Position childNode : getAdjacentPositions(currentNode)) {
                if (visited.contains(childNode)) {
                    continue;
                }
                distances.put(childNode, distances.get(currentNode) + 1);
                parentPosition.put(childNode, currentNode);
                queue.add(childNode);
                visited.add(childNode);
            }
        }
        return distances;
    }

    public Map<Position, Position> getParentPosition() {
        return parentPosition;
    }

    private void setupInternals() {
        queue = new ArrayDeque<>();
        visited = new HashSet<>();
        distances = new HashMap<>();
        parentPosition = new HashMap<>();
    }

    private void initiateStartNode(Position startNode) {
        queue.add(startNode);
        visited.add(startNode);
        distances.put(startNode, 0);
    }

    public List<Position> getAdjacentPositions(Position currentNode) {
        List<Position> neighbours = new ArrayList<>();
        //the order of adding neighbours is important - this effects which position is prioritized
        addPositionIfValid(neighbours, currentNode.x, currentNode.y - 1);
        addPositionIfValid(neighbours, currentNode.x - 1, currentNode.y);
        addPositionIfValid(neighbours, currentNode.x + 1, currentNode.y);
        addPositionIfValid(neighbours, currentNode.x, currentNode.y + 1);
        return neighbours;
    }

    private void addPositionIfValid(List<Position> neighbours, int x, int y) {
        Position position = new Position(x, y);
        if (!validEmptyPositions.contains(position)) {
            return;
        }
        if (visited.contains(position)) {
            return;
        }
        neighbours.add(position);
    }
}
