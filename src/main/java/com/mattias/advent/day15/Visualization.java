package com.mattias.advent.day15;

import java.util.Arrays;
import java.util.List;
import java.util.stream.IntStream;

class Visualization {

    void drawFreePositions(List<Position> freePositions) {
        int maxX = geMaxX(freePositions);
        int maxY = getMaxY(freePositions);

        char[][] positions = getEmptyArea(maxX, maxY);
        addFreePositions(freePositions, positions);
        printPositions(maxY, positions);
    }

    private int getMaxY(List<Position> freePositions) {
        return freePositions.stream()
                .mapToInt(it -> it.y)
                .max().orElse(1);
    }

    private int geMaxX(List<Position> freePositions) {
        return freePositions.stream()
                .mapToInt(it -> it.x)
                .max().orElse(1);
    }

    private void addFreePositions(List<Position> freePositions, char[][] positions) {
        for (Position position : freePositions) {
            positions[position.y - 1][position.x - 1] = '.';
        }
    }

    private void printPositions(int maxY, char[][] positions) {
        IntStream.range(0, maxY)
                .boxed()
                .map(it -> positions[it])
                .map(String::new)
                .forEach(System.out::println);
    }

    private char[][] getEmptyArea(int maxX, int maxY) {
        char[][] positions = new char[maxY][];
        for (int i = 0; i < maxY; i++) {
            positions[i] = new char[maxX];
            Arrays.fill(positions[i], 'X');
        }
        return positions;
    }

}
