package com.mattias.advent.day15.exception;

public class DeadElfException extends RuntimeException {
    public DeadElfException() {
        super("Dead elf is no good");
    }
}
