package com.mattias.advent.day16;

import com.mattias.advent.common.FileUtil;
import lombok.extern.slf4j.Slf4j;

import java.io.FileNotFoundException;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

@Slf4j
public class Day16 {

    FileUtil fileUtil = new FileUtil();

    private List<String> getInput() throws FileNotFoundException {
        return fileUtil.getFileContentAsStringList("day16input");
    }

    private List<String> getFirstPartInput() throws FileNotFoundException {
        return getInput().subList(0, 3043);
    }

    List<OperandBehavior> getOperandBehavior() throws FileNotFoundException {
        List<OperandBehavior> operandBehaviors = new ArrayList<>();
        List<String> firstPartInput = getFirstPartInput();
        for (int start = 0; start + 3 <= firstPartInput.size(); start += 4) {
            String before = firstPartInput.get(start);
            String after = firstPartInput.get(start + 2);
            String operand = firstPartInput.get(start + 1);

            operandBehaviors.add(OperandBehavior.builder()
                    .registersBeforeOperation(registerLineAsIntArray(before.substring(9, 19)))
                    .registersAfterOperations(registerLineAsIntArray(after.substring(9, 19)))
                    .operation(operationAsIntArray(operand))
                    .build());
        }
        return operandBehaviors;
    }

    private List<String> getSecondPartInput() throws FileNotFoundException {
        return getInput().subList(3046, 4014);
    }

    List<int[]> getProgramOperations() throws FileNotFoundException {
        return getSecondPartInput().stream()
                .map(it -> it.split(" "))
                .map(it -> Arrays.stream(it)
                        .mapToInt(Integer::parseInt)
                        .toArray())
                .collect(Collectors.toList());
    }

    private int[] registerLineAsIntArray(String source) {
        return Arrays.stream(source.replaceAll(" ", "").split(","))
                .mapToInt(Integer::valueOf)
                .toArray();
    }

    private int[] operationAsIntArray(String source) {
        return Arrays.stream(source.split(" "))
                .mapToInt(Integer::valueOf)
                .toArray();
    }

    List<OperandBehavior> getOperandBehaviorsMatchingMultipleOperands(List<OperandBehavior> behaviors, int numberOfBehaviorsToMatch) {
        return behaviors.stream()
                .filter(it -> matchesMultipleInstructions(it, numberOfBehaviorsToMatch))
                .collect(Collectors.toList());
    }

    Map<Integer, Operations.Instruction> getIdentifiedOperations(List<OperandBehavior> behaviors) {
        Map<Integer, List<Operations.Instruction>> possibleOperations = new HashMap<>();
        for (OperandBehavior behaviorItem : behaviors) {
            int operationIdentifier = behaviorItem.operation[0];
            List<Operations.Instruction> instructionsForBehavior = getInstructionsMatchingBehavior(behaviorItem);
            if (!possibleOperations.containsKey(operationIdentifier)) {
                possibleOperations.put(operationIdentifier, instructionsForBehavior);
            } else {
                possibleOperations.get(operationIdentifier).retainAll(instructionsForBehavior);
            }
        }
        makeOperationsUnique(possibleOperations);
        return possibleOperations.keySet().stream()
                .collect(Collectors.toMap(Function.identity(), it -> possibleOperations.get(it).get(0)));
    }

    private void makeOperationsUnique(Map<Integer, List<Operations.Instruction>> possibleOperations) {
        while (hasMoreThenOnePossibleOperation(possibleOperations)) {
            List<Operations.Instruction> identifiedOperations = getIdentifiedOperations(possibleOperations);
            //log.info("Identified operations : " + identifiedOperations.size());
            for (List<Operations.Instruction> instructionListItem : possibleOperations.values()) {
                if (instructionListItem.size() > 1) {
                    instructionListItem.removeAll(identifiedOperations);
                }
            }
        }
    }

    private List<Operations.Instruction> getIdentifiedOperations(Map<Integer, List<Operations.Instruction>> possibleOperations) {
        return possibleOperations.values().stream()
                .filter(it -> it.size() == 1)
                .map(it -> it.get(0))
                .collect(Collectors.toList());
    }

    private boolean hasMoreThenOnePossibleOperation(Map<Integer, List<Operations.Instruction>> possibleOperations) {
        return possibleOperations.values().stream()
                .anyMatch(it -> it.size() > 1);
    }

    int[] runProgram(List<int[]> operations, Map<Integer, Operations.Instruction> identifiedInstructions) {
        int[] register = new int[]{0, 0, 0, 0};
        for (int[] operationItem : operations) {
            register = identifiedInstructions.get(operationItem[0]).apply(register, operationItem);
        }
        return register;
    }

    private List<Operations.Instruction> getInstructionsMatchingBehavior(OperandBehavior behavior) {
        List<Operations.Instruction> result = new ArrayList<>();
        Operations.Instruction[] instructions = Operations.ALL_INSTRUCTIONS;
        for (int i = 0; i < instructions.length; i++) {
            int[] after = instructions[i].apply(behavior.registersBeforeOperation, behavior.operation);
            if (Arrays.equals(after, behavior.registersAfterOperations)) {
                result.add(instructions[i]);
            }
        }
        return result;
    }

    private boolean matchesMultipleInstructions(OperandBehavior behavior, int minimumMatches) {
        int matches = 0;
        Operations.Instruction[] instructions = Operations.ALL_INSTRUCTIONS;
        for (int i = 0; i < instructions.length; i++) {
            int[] after = instructions[i].apply(behavior.registersBeforeOperation, behavior.operation);
            if (Arrays.equals(after, behavior.registersAfterOperations)) {
                matches += 1;
            }
        }
        return matches >= minimumMatches;
    }
}
