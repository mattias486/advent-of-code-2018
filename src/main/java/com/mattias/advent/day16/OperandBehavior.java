package com.mattias.advent.day16;

import lombok.Builder;

@Builder
public class OperandBehavior {

    int[] registersBeforeOperation;
    int[] registersAfterOperations;
    int[] operation;
}
