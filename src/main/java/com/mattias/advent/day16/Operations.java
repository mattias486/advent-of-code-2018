package com.mattias.advent.day16;

import java.util.Arrays;
import java.util.function.BiFunction;

class Operations {

    @FunctionalInterface
    interface Instruction extends BiFunction<int[], int[], int[]> {
    }

    private static BiFunction<Integer, Integer, Integer> addOperation = (first, second) -> first + second;
    private static BiFunction<Integer, Integer, Integer> multiOperation = (first, second) -> first * second;
    private static BiFunction<Integer, Integer, Integer> bitwiseAnd = (first, second) -> first & second;
    private static BiFunction<Integer, Integer, Integer> bitwiseOr = (first, second) -> first | second;
    private static BiFunction<Integer, Integer, Integer> greaterThen = (first, second) -> first > second ? 1 : 0;
    private static BiFunction<Integer, Integer, Integer> equals = (first, second) -> first.equals(second) ? 1 : 0;
    private static BiFunction<Integer, Integer, Integer> set = (first, __) -> first;

    static Instruction addr = (register, operand) -> dualRegisterInstruction(register, operand, addOperation);
    static Instruction addi = (register, operand) -> registerValueInstruction(register, operand, addOperation);

    static Instruction mulr = (register, operand) -> dualRegisterInstruction(register, operand, multiOperation);
    static Instruction muli = (register, operand) -> registerValueInstruction(register, operand, multiOperation);

    static Instruction banr = (register, operand) -> dualRegisterInstruction(register, operand, bitwiseAnd);
    static Instruction bani = (register, operand) -> registerValueInstruction(register, operand, bitwiseAnd);

    static Instruction borr = (register, operand) -> dualRegisterInstruction(register, operand, bitwiseOr);
    static Instruction bori = (register, operand) -> registerValueInstruction(register, operand, bitwiseOr);

    static Instruction setr = (register, operand) -> dualRegisterInstruction(register, operand, set);
    static Instruction seti = (register, operand) -> valueRegisterInstruction(register, operand, set);

    static Instruction gtir = (register, operand) -> valueRegisterInstruction(register, operand, greaterThen);
    static Instruction gtri = (register, operand) -> registerValueInstruction(register, operand, greaterThen);
    static Instruction gtrr = (register, operand) -> dualRegisterInstruction(register, operand, greaterThen);

    static Instruction eqir = (register, operand) -> valueRegisterInstruction(register, operand, equals);
    static Instruction eqri = (register, operand) -> registerValueInstruction(register, operand, equals);
    static Instruction eqrr = (register, operand) -> dualRegisterInstruction(register, operand, equals);

    static Instruction[] ALL_INSTRUCTIONS = new Instruction[] {
            addi, addr,
            mulr, muli,
            bani, banr,
            bori, borr,
            setr, seti,
            gtir, gtri, gtrr,
            eqir, eqri, eqrr
    };


    private static int[] dualRegisterInstruction(int register[], int[] operand, BiFunction<Integer, Integer, Integer> operation) {
        int after[] = copy(register);
        after[operand[3]] =  operation.apply(register[operand[1]], register[operand[2]]);
        return after;
    }

    private static int[] valueRegisterInstruction(int register[], int[] operand, BiFunction<Integer, Integer, Integer> operation) {
        int after[] = copy(register);
        after[operand[3]] =  operation.apply(operand[1], register[operand[2]]);
        return after;
    }

    private static int[] registerValueInstruction(int register[], int[] operand, BiFunction<Integer, Integer, Integer> operation) {
        int after[] = copy(register);
        after[operand[3]] =  operation.apply(register[operand[1]], operand[2]);
        return after;
    }

    private static int[] copy(int[] before) {
        return Arrays.copyOf(before, 4);
    }
}
