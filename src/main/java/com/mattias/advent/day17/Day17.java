package com.mattias.advent.day17;

import com.mattias.advent.common.FileUtil;
import java.io.FileNotFoundException;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class Day17 {

    FileUtil fileUtil = new FileUtil();

    private List<String> getInput() throws FileNotFoundException {
        return fileUtil.getFileContentAsStringList("day17input");
    }

    List<Position> getClayPositions() throws FileNotFoundException {
        return getInput().stream()
                .flatMap(it -> getPositions(it).stream())
                .collect(Collectors.toList());
    }

    List<Position> getPositions(String input) {
        String[] s = input.split(" ");
        int firstCoordinate = Integer.parseInt(s[0].substring(2, s[0].length() - 1));
        String[] rangeValues = s[1].substring(2).split("\\.\\.");
        int rangeStart = Integer.parseInt(rangeValues[0]);
        int rangeEnd = Integer.parseInt(rangeValues[1]);
        if(s[0].charAt(0) == 'x') {
            return getPositionsForVerticalRange(firstCoordinate, rangeStart, rangeEnd);
        }
        return getPositionsForHorizontalRange(firstCoordinate, rangeStart, rangeEnd);
    }

    private List<Position> getPositionsForVerticalRange(int x, int startY, int endY) {
        return IntStream.range(startY, endY + 1)
                .boxed()
                .map(y -> new Position(x, y))
                .collect(Collectors.toList());
    }

    private List<Position> getPositionsForHorizontalRange(int y, int startX, int endX) {
        return IntStream.range(startX, endX + 1)
                .boxed()
                .map(x -> new Position(x, y))
                .collect(Collectors.toList());
    }

}
