package com.mattias.advent.day17;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;

import java.util.Comparator;

import static java.util.Comparator.comparingInt;

@EqualsAndHashCode
@AllArgsConstructor
@Getter
class Position {

    public static final Comparator<Position> orderByPosition = comparingInt(Position::getY)
            .thenComparingInt(Position::getX);

    int x;
    int y;

    public String toString() {
        return x + "," + y;
    }

    public Position up() {
        return new Position(this.x, this.y - 1);
    }

    public Position down() {
        return new Position(this.x, this.y + 1);
    }

    public Position left() {
        return new Position(this.x - 1, this.y);
    }

    public Position right() {
        return new Position(this.x + 1, this.y);
    }

}
