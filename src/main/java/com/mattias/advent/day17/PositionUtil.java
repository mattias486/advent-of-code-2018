package com.mattias.advent.day17;

import java.util.List;

public class PositionUtil {

    public static final int getMaxY(List<Position> positions) {
        return positions.stream().mapToInt(position -> position.y).max().orElse(0);
    }

    public static final int getMinY(List<Position> positions) {
        return positions.stream().mapToInt(position -> position.y).min().orElse(0);
    }

    public static final int getMaxX(List<Position> positions) {
        return positions.stream().mapToInt(position -> position.x).max().orElse(0);
    }

    public static final int getMinX(List<Position> positions) {
        return positions.stream().mapToInt(position -> position.x).min().orElse(0);
    }

}
