package com.mattias.advent.day18;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.tuple.Pair;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.IntStream;

@Slf4j
public class AcreLogic {

    static final char TREE = '|';
    static final char LUMBERYARD = '#';
    static final char OPEN_GROUND = '.';

    Map<CharacterArrayKey, char[][]> stateCache = new HashMap<>();
    boolean logFirst = true;

    char[][] getNextMinuteState(char[][] previous, int minute) {
        CharacterArrayKey initialStateKey = new CharacterArrayKey(previous);
        if (stateCache.containsKey(initialStateKey)) {
            if(logFirst) {
                log.info("First cache hit at minute : " + minute);
                logFirst = false;
            }
            return stateCache.get(initialStateKey);
        }

        char[][] newState = getEmptyResult(previous);
        IntStream.range(0, newState.length)
                .parallel()
                .boxed()
                .flatMap(y -> IntStream.range(0, newState[0].length).boxed().map(x -> Pair.of(x, y)))
                .forEach(pair -> newState[pair.getRight()][pair.getLeft()] = getNewState(previous, pair.getLeft(), pair.getRight()));

        stateCache.put(initialStateKey, newState);
        return newState;
    }

    char getNewState(char[][] previous, int x, int y) {
        char currentType = previous[y][x];
        int trees = getSurroundingTrees(previous, x, y);
        int lumberyards = getSurroundingLumberyards(previous, x, y);
        if (OPEN_GROUND == currentType) {
            return trees >= 3 ? TREE : OPEN_GROUND;
        }
        if (TREE == currentType) {
            return lumberyards >= 3 ? LUMBERYARD : TREE;
        }
        if (LUMBERYARD == currentType) {
            return lumberyards >= 1 && trees >= 1 ? LUMBERYARD : OPEN_GROUND;
        }
        return OPEN_GROUND;
    }

    int getSurroundingTrees(char[][] previous, int x, int y) {
        return getSurroundingOfType(previous, x, y, TREE);
    }

    int getSurroundingLumberyards(char[][] previous, int x, int y) {
        return getSurroundingOfType(previous, x, y, LUMBERYARD);
    }

    private int getSurroundingOfType(char[][] previous, int x, int y, char type) {
        return getSurroundingPositions(x, y).stream()
                .filter(position -> isValidInRange(previous, position.x, position.y))
                .filter(position -> isPositionOfType(previous, position.x, position.y, type))
                .mapToInt(__ -> 1)
                .sum();
    }

    private List<Position> getSurroundingPositions(int x, int y) {
        List<Position> result = new ArrayList<>();
        result.add(new Position(x - 1, y - 1));
        result.add(new Position(x, y - 1));
        result.add(new Position(x + 1, y - 1));
        result.add(new Position(x - 1, y));
        result.add(new Position(x + 1, y));
        result.add(new Position(x - 1, y + 1));
        result.add(new Position(x, y + 1));
        result.add(new Position(x + 1, y + 1));
        return result;
    }

    private boolean isPositionOfType(char[][] previous, int x, int y, char type) {
        return previous[y][x] == type;
    }

    private boolean isValidInRange(char[][] previous, int x, int y) {
        return x >= 0 && x < previous[0].length && y >= 0 && y < previous.length;
    }

    private char[][] getEmptyResult(char[][] previous) {
        char[][] result = new char[previous.length][];
        for (int i = 0; i < previous.length; i++) {
            result[i] = new char[previous[0].length];
        }
        return result;
    }
}
