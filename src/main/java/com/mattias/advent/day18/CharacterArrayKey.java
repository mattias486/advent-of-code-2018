package com.mattias.advent.day18;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode
@AllArgsConstructor
public class CharacterArrayKey {
    private final char[][] value;

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        for (char[] chars : value) {
            builder.append(new String(chars));
        }
        return builder.toString();
    }
}

