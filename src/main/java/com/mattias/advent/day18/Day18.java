package com.mattias.advent.day18;

import com.mattias.advent.common.FileUtil;
import lombok.extern.slf4j.Slf4j;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

@Slf4j
public class Day18 {

    FileUtil fileUtil = new FileUtil();
    AcreLogic acreLogic = new AcreLogic();

    List<char[][]> getExamples() throws FileNotFoundException {
        List<char[][]> result = new ArrayList<>();
        for (int i = 0; i < 11; i++) {
            result.add(fileUtil.getFileContentAsCharacterArray("day18/examples", 1 + (i * 12), 10 + (i * 12)));
        }
        return result;
    }

    char[][] getInput() throws FileNotFoundException {
        return fileUtil.getFileContentAsCharacterArray("day18input");
    }

    int getResourceValue(char[][] state) {
        int trees = 0;
        int lumberyards = 0;
        for (int y = 0; y < state.length; y++) {
            for (int x = 0; x < state[0].length; x++) {
                if (state[y][x] == AcreLogic.TREE) {
                    trees += 1;
                }
                if (state[y][x] == AcreLogic.LUMBERYARD) {
                    lumberyards += 1;
                }
            }
        }
        log.info(String.format("Lumber yards : %s, trees : %s", lumberyards, trees));
        return trees * lumberyards;
    }

    /**
     * spoiler : first cache hit is at minute 634 and then the state pattern repeats every 28 minutes.
     * this gives that for a value a where a > 634 the resource value is the same for "634 + (a % 28) + offset" as for a
     * offset was shown to be 10
     */
    int getResourceValueForHighValues(char[][] initialState, int minute) {
        if(minute < 634) {
            return 0;
        }
        int minuteWithResourceValueEqualToWanted = 10 + 634 + (minute % 28);
        char[][] newState = getNewState(initialState, minuteWithResourceValueEqualToWanted);
        return getResourceValue(newState);
    }

    char[][] getNewState(char[][] initialState, int minutes) {
        char[][] state = initialState;
        for (int i = 0; i < minutes; i++) {
            state = acreLogic.getNextMinuteState(state, i);
        }
        return state;
    }
}
