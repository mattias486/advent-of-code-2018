package com.mattias.advent.day18;

import lombok.AllArgsConstructor;

@AllArgsConstructor
public class Position {
    final int x;
    final int y;

    @Override
    public String toString() {
        return x + "," + y;
    }
}
