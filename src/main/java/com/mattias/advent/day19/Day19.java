package com.mattias.advent.day19;

import com.mattias.advent.common.FileUtil;
import lombok.extern.slf4j.Slf4j;

import java.io.FileNotFoundException;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static java.util.Map.entry;

@Slf4j
public class Day19 {

    FileUtil fileUtil = new FileUtil();

    static Map<String, Operations.Operation> operations = Map.ofEntries(
            entry("addr", Operations.addr),
            entry("addi", Operations.addi),
            entry("mulr", Operations.mulr),
            entry("muli", Operations.muli),
            entry("banr", Operations.banr),
            entry("bani", Operations.bani),
            entry("borr", Operations.borr),
            entry("bori", Operations.bori),
            entry("setr", Operations.setr),
            entry("seti", Operations.seti),
            entry("gtir", Operations.gtir),
            entry("gtri", Operations.gtri),
            entry("gtrr", Operations.gtrr),
            entry("eqir", Operations.eqir),
            entry("eqri", Operations.eqri),
            entry("eqrr", Operations.eqrr)
    );

    public List<Instruction> getInstruction() throws FileNotFoundException {
        List<String> day19input = fileUtil.getFileContentAsStringList("day19input");
        return day19input.stream()
                .skip(1)
                .map(this::getInstructionFromLine)
                .collect(Collectors.toList());
    }

    Instruction getInstructionFromLine(String line) {
        String[] s = line.split(" ");
        String name = s[0];
        int[] arguments = Arrays.stream(s)
                .skip(1)
                .mapToInt(Integer::valueOf)
                .toArray();
        return new Instruction(name, arguments);
    }

    public int[] runProgram(List<Instruction> instructions, int[] initialRegister) {
        int[] register = initialRegister;
        int indexForInstructionPointer = 1;
        int steps = 0;

        while (isValidInstructionPointerValue(register, indexForInstructionPointer, instructions)) {
            int instructionPointer = register[indexForInstructionPointer];
            String registerBefore = Arrays.toString(register);
            Instruction instruction = instructions.get(instructionPointer);
            register = operations.get(instruction.name).apply(register, instruction.arguments);
            increaseInstructionRegister(register, indexForInstructionPointer);
            steps += 1;
            logInstructionAndRegisters(registerBefore, register, instruction, instructionPointer);
            if (steps > 100) {
                return register;
            }
        }
        log.info(String.format("Finished after %s steps", steps));
        return register;
    }

    private boolean isValidInstructionPointerValue(int[] register, int indexForInstructionPointer, List<Instruction> instructions) {
        return register[indexForInstructionPointer] >= 0 &&
                register[indexForInstructionPointer] < instructions.size();
    }

    void logInstructionAndRegisters(String registersBefore, int[] register, Instruction instruction, int ipValueAtStart) {
        StringBuilder logRow = new StringBuilder();
        logRow.append("ip=")
                .append(ipValueAtStart)
                .append(" ")
                .append(registersBefore)
                .append(" ")
                .append(instruction.name)
                .append(" ")
                .append(getArgumentsAsString(instruction))
                .append(" ")
                .append(Arrays.toString(register));
        log.info(logRow.toString());
    }

    private String getArgumentsAsString(Instruction instruction) {
        return Arrays.stream(instruction.arguments)
                .mapToObj(String::valueOf)
                .collect(Collectors.joining(" "));
    }

    void increaseInstructionRegister(int[] register, int instructionRegisterIndex) {
        register[instructionRegisterIndex] = register[instructionRegisterIndex] + 1;
    }
}
