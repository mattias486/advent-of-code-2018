package com.mattias.advent.day19;

import lombok.AllArgsConstructor;

@AllArgsConstructor
public class Instruction {
    String name;
    int[] arguments;
}
