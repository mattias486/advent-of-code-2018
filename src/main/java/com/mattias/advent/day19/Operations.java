package com.mattias.advent.day19;

import java.util.Arrays;
import java.util.function.BiFunction;

class Operations {

    public static final int REGISTER_SIZE = 6;

    @FunctionalInterface
    interface Operation extends BiFunction<int[], int[], int[]> {
    }

    private static BiFunction<Integer, Integer, Integer> addOperation = (first, second) -> first + second;
    private static BiFunction<Integer, Integer, Integer> multiOperation = (first, second) -> first * second;
    private static BiFunction<Integer, Integer, Integer> bitwiseAnd = (first, second) -> first & second;
    private static BiFunction<Integer, Integer, Integer> bitwiseOr = (first, second) -> first | second;
    private static BiFunction<Integer, Integer, Integer> greaterThen = (first, second) -> first > second ? 1 : 0;
    private static BiFunction<Integer, Integer, Integer> equals = (first, second) -> first.equals(second) ? 1 : 0;
    private static BiFunction<Integer, Integer, Integer> set = (first, __) -> first;

    static Operation addr = (register, operand) -> dualRegisterInstruction(register, operand, addOperation);
    static Operation addi = (register, operand) -> registerValueInstruction(register, operand, addOperation);

    static Operation mulr = (register, operand) -> dualRegisterInstruction(register, operand, multiOperation);
    static Operation muli = (register, operand) -> registerValueInstruction(register, operand, multiOperation);

    static Operation banr = (register, operand) -> dualRegisterInstruction(register, operand, bitwiseAnd);
    static Operation bani = (register, operand) -> registerValueInstruction(register, operand, bitwiseAnd);

    static Operation borr = (register, operand) -> dualRegisterInstruction(register, operand, bitwiseOr);
    static Operation bori = (register, operand) -> registerValueInstruction(register, operand, bitwiseOr);

    static Operation setr = (register, operand) -> registerNoValueInstruction(register, operand, set);
    static Operation seti = (register, operand) -> valueNoRegisterInstruction(register, operand, set);

    static Operation gtir = (register, operand) -> valueRegisterInstruction(register, operand, greaterThen);
    static Operation gtri = (register, operand) -> registerValueInstruction(register, operand, greaterThen);
    static Operation gtrr = (register, operand) -> dualRegisterInstruction(register, operand, greaterThen);

    static Operation eqir = (register, operand) -> valueRegisterInstruction(register, operand, equals);
    static Operation eqri = (register, operand) -> registerValueInstruction(register, operand, equals);
    static Operation eqrr = (register, operand) -> dualRegisterInstruction(register, operand, equals);


    private static int[] dualRegisterInstruction(int[] register, int[] operand, BiFunction<Integer, Integer, Integer> operation) {
        int[] after = copy(register);
        after[operand[2]] =  operation.apply(register[operand[0]], register[operand[1]]);
        return after;
    }

    private static int[] valueRegisterInstruction(int[] register, int[] operand, BiFunction<Integer, Integer, Integer> operation) {
        int[] after = copy(register);
        after[operand[2]] =  operation.apply(operand[0], register[operand[1]]);
        return after;
    }

    private static int[] registerValueInstruction(int[] register, int[] operand, BiFunction<Integer, Integer, Integer> operation) {
        int[] after = copy(register);
        after[operand[2]] =  operation.apply(register[operand[0]], operand[1]);
        return after;
    }

    private static int[] valueNoRegisterInstruction(int[] register, int[] operand, BiFunction<Integer, Integer, Integer> operation) {
        int[] after = copy(register);
        after[operand[2]] =  operation.apply(operand[0], 0); //second argument is disregarded
        return after;
    }

    private static int[] registerNoValueInstruction(int[] register, int[] operand, BiFunction<Integer, Integer, Integer> operation) {
        int[] after = copy(register);
        after[operand[2]] =  operation.apply(register[operand[0]], 0); //second argument is disregarded
        return after;
    }

    private static int[] copy(int[] before) {
        return Arrays.copyOf(before, REGISTER_SIZE);
    }
}
