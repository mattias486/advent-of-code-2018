package com.mattias.advent.day20;

import com.mattias.advent.common.FileUtil;
import lombok.AllArgsConstructor;
import lombok.Value;
import lombok.extern.slf4j.Slf4j;

import java.util.*;

@Slf4j
public class Day20 {

    FileUtil fileUtil = new FileUtil();

    String input = fileUtil.getFileContentRow("day05input", 0);

    @AllArgsConstructor
    private enum Direction {
        NORTH(1, 0),
        EAST(0, 1),
        SOUTH(-1, 0),
        WEST(0, -1);

        private int y;
        private int x;

        private Direction enterDirection() {
            switch (this) {
                case NORTH:
                    return SOUTH;
                case EAST:
                    return WEST;
                case SOUTH:
                    return NORTH;
                case WEST:
                    return EAST;
            }
            return null;
        }
    }

    @Value
    private class Room {
        private final Position position;
        private final Set<Direction> doors;
    }

    private class RoomContainer {
        Position currentPosition = new Position(0, 0);
        Stack<Position> crossRoads = new Stack<>();

        Stack<Integer> crossRoadsDistance = new Stack<>();
        int currentDistance = 0;

        Map<Position, Integer> shortestDistanceToRoom = new HashMap<>();

        Map<Position, Room> rooms = new HashMap(Map.of(currentPosition, new Room(currentPosition, new HashSet<>())));

        int getMaxLeft() {
            return rooms.keySet().stream()
                    .mapToInt(i -> i.x)
                    .min()
                    .orElse(0);
        }

        int getMaxRight() {
            return rooms.keySet().stream()
                    .mapToInt(i -> i.x)
                    .max()
                    .orElse(0);
        }

        int getMaxNorth() {
            return rooms.keySet().stream()
                    .mapToInt(i -> i.y)
                    .max()
                    .orElse(0);
        }

        int getMaxSouth() {
            return rooms.keySet().stream()
                    .mapToInt(i -> i.y)
                    .min()
                    .orElse(0);
        }

        void move(Direction direction) {
            rooms.get(currentPosition).doors.add(direction);
            var nextPosition = new Position(currentPosition.x + direction.x, currentPosition.y + direction.y);
            Room nextRoom = getRoom(nextPosition);
            nextRoom.doors.add(direction.enterDirection());
            currentPosition = nextPosition;
            currentDistance++;
            checkIfNewMaxDistance();
        }

        private void checkIfNewMaxDistance() {
            int currentTotalNumberOfDoors = currentDistance +
                    crossRoadsDistance.stream()
                    .mapToInt(i -> i)
                    .sum();

            if(!shortestDistanceToRoom.containsKey(currentPosition)) { // first time in room
                shortestDistanceToRoom.put(currentPosition, currentTotalNumberOfDoors);
                return;
            }
            // update if we enter the room and we've entered fewer unique doors
            if(currentTotalNumberOfDoors < shortestDistanceToRoom.get(currentPosition)) {
                shortestDistanceToRoom.put(currentPosition, currentTotalNumberOfDoors);
            }
        }

        void enterCrossroad() {
            crossRoads.push(currentPosition);
            crossRoadsDistance.push(currentDistance);
            currentDistance = 0;
        }

        void returnToCrossroad() {
            currentPosition = crossRoads.peek();
            currentDistance = 0;
        }

        void exitCrossroad() {
            currentPosition = crossRoads.pop();
            currentDistance = crossRoadsDistance.pop();
        }

        private Room getRoom(Position position) {
            if (!rooms.containsKey(position)) {
                rooms.put(position, new Room(position,  new HashSet<>()));
            }
            return rooms.get(position);
        }

        public int getDistanceToFurthestRoom() {
            return shortestDistanceToRoom.values().stream()
                    .mapToInt(i -> i)
                    .max()
                    .orElse(0);
        }

        public long getRoomCountWithShortestDistance(int distance) {
            return shortestDistanceToRoom.values().stream()
                    .filter(i -> i >= distance)
                    .count();
        }
    }

    private class RenderedRooms {

        final char[][] rendered;

        public RenderedRooms(int height, int width) {
            int renderedWidth = width * 2 + 1;
            int renderedHeight = height * 2 + 1;
            rendered = new char[renderedHeight][renderedWidth];
            for (int y = 0; y < rendered.length; y++) {
                Arrays.fill(rendered[y], '#');
            }
        }

        void renderRoom(Room room) {

            int renderX = room.position.x * 2;
            int renderY = room.position.y * 2;

            //rendered[0][0] = '#';
            if (room.doors.contains(Direction.NORTH)) {
                rendered[renderY][renderX+1] = '-';
            }
            //rendered[0][2] = '#';

            if (room.doors.contains(Direction.WEST)) {
                rendered[renderY+1][0] = '|';
            }
            rendered[renderY+1][renderX+1] = '.';
            if (room.doors.contains(Direction.EAST)) {
                rendered[renderY+1][renderX+2] = '|';
            }

            //rendered[2][0] = '#';
            if (room.doors.contains(Direction.SOUTH)) {
                rendered[renderY+2][renderX+1] = '-';
            }
            //rendered[2][2] = '#';
        }

        public void drawRooms() {
            StringBuilder builder = new StringBuilder();
            for (int y = 0; y < rendered.length; y++) {
                builder.append(rendered[y]);
                builder.append('\n');
            }
            System.out.println(builder.toString());
        }
    }

    public void drawRooms(String regex) {

        RoomContainer roomContainer = createRooms(regex);

        int maxLeft = roomContainer.getMaxLeft();
        int maxNorth = roomContainer.getMaxNorth();
        int maxRight = roomContainer.getMaxRight();
        int maxSouth = roomContainer.getMaxSouth();

        var topLeft = new Position(maxLeft, maxNorth);

        int width = maxRight - maxLeft + 1; //max right is 0 or higher since we start here
        int height = maxNorth - maxSouth + 1; //max north is 0 or higher since we start here

        var renderedRooms = new RenderedRooms(width, height);

        roomContainer.rooms.values().stream()
                .map(i -> getRoomWithAdjustedPosition(i, topLeft))
                .forEach(renderedRooms::renderRoom);

        System.out.println(regex);
        System.out.println("Max number of doors : " + roomContainer.getDistanceToFurthestRoom());
        System.out.println("Rooms with shortest distance of at least 1000 doors : " + roomContainer.getRoomCountWithShortestDistance(1000));

        renderedRooms.drawRooms();


        /**
         * rooms (without splits) seem to be created correct
         *
         * suggested next step would be to render it
         *  - TOOD : translate positions to how we render it
         *
         * step after would be to add support for regex : (|)
         */
    }

    /**
     * We adjust the position to render using topLeft as the 0,0 position
     * rather then the bottom right corner
     */
    private Room getRoomWithAdjustedPosition(Room i, Position topLeft) {
        Position adjustedPosition = getAdjustedPosition(i.position, topLeft);
        return new Room(adjustedPosition, i.getDoors());
    }

    // x moves to the left (negative), y moves to the N (positive)
    Position getAdjustedPosition(Position position, Position topLeft) {
        return new Position(position.x - topLeft.x,  topLeft.y - position.y);
    }

    public RoomContainer createRooms(String regex) {
        regex = regex.replaceFirst("^\\^", "");
        regex = regex.replaceFirst("\\$$", "");

        RoomContainer roomContainer = new RoomContainer();

        regex.chars().forEach(i -> move(roomContainer, i));

        return roomContainer;
    }

    private void move(RoomContainer roomContainer, int direction) {
        switch (direction) {
            case 'W':
                roomContainer.move(Direction.WEST);
                break;
            case 'E':
                roomContainer.move(Direction.EAST);
                break;
            case 'N':
                roomContainer.move(Direction.NORTH);
                break;
            case 'S':
                roomContainer.move(Direction.SOUTH);
                break;
            case '(':
                roomContainer.enterCrossroad();
                break;
            case ')':
                roomContainer.exitCrossroad();
                break;
            case '|':
                roomContainer.returnToCrossroad();
                break;
            default:
        }
    }
}
