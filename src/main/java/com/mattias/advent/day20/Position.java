package com.mattias.advent.day20;

import lombok.Value;
import lombok.With;

@With
@Value
public class Position {
    public final int x;
    public final int y;
}
