package com.mattias.advent.day22;

import lombok.Value;

import java.util.Arrays;

public class Day22 {

    void getRiskOfArea(int depth, Coordinate target) {

        var regions = new RegionType[target.y + 1][target.x + 1];
        var regionGeologicIndex = new long[target.y + 1][target.x + 1];

        for (var y = 0; y <= target.y; y++) {
            for (var x = 0; x <= target.x; x++) {
                regionGeologicIndex[y][x] = getGeologicIndex(x, y, regionGeologicIndex, depth);
            }
        }

        for (var y = 0; y <= target.y; y++) {
            for (var x = 0; x <= target.x; x++) {
                regions[y][x] = getRegionType(getErosionLevel(regionGeologicIndex[y][x], depth));
            }
        }

        var sumOfArea = getSumOfAreaToReachTarget(regions, target);
        System.out.println("Sum of area : " + sumOfArea);

        drawArea(target, regions);
    }

    private void drawArea(Coordinate target, RegionType[][] regions) {
        for (var y = 0; y <= target.y; y++) {
            StringBuilder row = new StringBuilder();
            for (var x = 0; x <= target.x; x++) {
                if (x == 0 && y == 0) {
                    row.append('M');
                } else if (x == target.x && y == target.y) {
                    row.append('T');
                } else {
                    row.append(regions[y][x].visual);
                }
            }
            System.out.println(row.toString());
        }
    }

    long getSumOfAreaToReachTarget(RegionType[][] regionTypes, Coordinate target) {
        var sum = 0;
        for (var y = 0; y <= target.y; y++) {
            for (var x = 0; x <= target.x; x++) {
                if((x == 0 && y == 0) || (x == target.x && y == target.y)) {
                    // Skipping target and start position
                } else {
                    sum += regionTypes[y][x].regionTypeValue;
                }
            }
        }
        return sum;
    }

    long getGeologicIndex(int x, int y, long[][] regionGeologicIndex, int depth) {
        if (x == 0 && y == 0) {
            return 0;
        }
        if (y == 0) {
            return x * 16_807;
        }
        if (x == 0) {
            return y * 48_271;
        }
        return getErosionLevel(regionGeologicIndex[y][x - 1], depth) *
                getErosionLevel(regionGeologicIndex[y - 1][x], depth);
    }

    long getErosionLevel(long geologicIndex, int depth) {
        return (geologicIndex + depth) % 20_183;
    }

    RegionType getRegionType(long geologicIndex) {
        return RegionType.ofValue(geologicIndex % 3);
    }

    @Value
    public static class Coordinate {
        int x;
        int y;
    }

    enum RegionType {
        ROCKY(0, '.'),
        WET(1, '='),
        NARROW(2, '|'),
        UNKNOWN(-1, '@');

        int regionTypeValue;
        char visual;

        RegionType(int regionTypeValue, char visual) {
            this.regionTypeValue = regionTypeValue;
            this.visual = visual;
        }

        static RegionType ofValue(long value) {
            return Arrays.stream(RegionType.values())
                    .filter(i -> i.regionTypeValue == value)
                    .findAny()
                    .orElse(UNKNOWN);
        }
    }
}
