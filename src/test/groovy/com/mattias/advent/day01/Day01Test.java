package com.mattias.advent.day01;

import org.junit.Ignore;
import org.junit.Test;

import java.io.FileNotFoundException;

public class Day01Test {

    @Ignore("Part 1")
    @Test
    public void getSumOfContent() throws FileNotFoundException {

        Integer sumOfFile = new Day01().getSumOfContent();

        System.out.println("Sum of file content : " + sumOfFile);
    }

    @Ignore("Part 2")
    @Test
    public void getFirstDuplicate() throws FileNotFoundException {

        Integer firstDuplicate = new Day01().getFirstDuplicateFrequency();

        System.out.println("First duplicate frequency : " + firstDuplicate);
    }
}
