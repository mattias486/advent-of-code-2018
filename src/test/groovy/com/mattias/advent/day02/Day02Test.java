package com.mattias.advent.day02;

import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;

import java.io.FileNotFoundException;
import java.util.Map;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class Day02Test {

    private Day02 day02 = new Day02();

    @Test
    @Ignore("Part 1")
    public void getChecksumTask01() throws FileNotFoundException {
        Long checksumForBoxes = day02.getChecksumForBoxes();
        System.out.println("Checksum : " + checksumForBoxes);
    }

    @Test
    public void testNumberOfInstances() {
        assertFalse(day02.containsNumberOfTheSameCharacters("lias", 2));
        assertFalse(day02.containsNumberOfTheSameCharacters("liaas", 3));
        assertFalse(day02.containsNumberOfTheSameCharacters("liaaas", 4));

        assertFalse(day02.containsNumberOfTheSameCharacters("liaaas", 2));

        assertTrue(day02.containsNumberOfTheSameCharacters("liaas", 2));
        assertTrue(day02.containsNumberOfTheSameCharacters("liaaas", 3));
    }

    @Test
    public void testNumberOfCharacterInstancesInString() {
        Assert.assertEquals(day02.getNumberOfCharacterInstancesInString("abababbbac", 'a'), 4);
        Assert.assertEquals(day02.getNumberOfCharacterInstancesInString("abababbbac", 'b'), 5);
    }

    @Test
    public void getNumberOFDifferences() {
        Assert.assertEquals(1, day02.getNumberOfCharDifferences("pqrst", "pqrsz"));
        Assert.assertEquals(0, day02.getNumberOfCharDifferences("pqrst", "pqrst"));
        Assert.assertEquals(2, day02.getNumberOfCharDifferences("pqrst", "parsb"));
        Assert.assertEquals(1, day02.getNumberOfCharDifferences("uqyoeizfvmbistpkgnobcjtwld", "uqyoeizfvmbistpkgnoacjtwld"));
    }

    @Test
    @Ignore("Part 2")
    public void getStringsThatDifferWithOneCharacter() throws FileNotFoundException {
        Map<String, String> result = day02.getStringsThatDifferWithOneCharacter();
        System.out.println("Strings differing only by one : " + result.toString());

        String stringWithCommonParts = day02.removeDifferingParts(result.values().stream().findFirst().orElse(""),
                                                                  result.keySet().stream().findFirst().orElse(""));
        System.out.println("Common parts in string : " + stringWithCommonParts);
    }

}
