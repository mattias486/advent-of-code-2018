package com.mattias.advent.day03;

import org.junit.Ignore;
import org.junit.Test;

import java.io.FileNotFoundException;

public class Day03Test {

    private Day03 day03 = new Day03();

    @Ignore("Part 1")
    @Test
    public void getNumberOfConflictingSquares() throws FileNotFoundException {
        Long numberOfConflictingSquares = day03.getNumberOfConflictingSquares();
        System.out.println("Number of conflicting squares : " + numberOfConflictingSquares.toString());
    }


    @Ignore("Part 2 - The rectangle id can be searched in the input data")
    @Test
    public void getRectangleWithoutConflictingSquare() throws FileNotFoundException {
        Rectangle rectangle = day03.getRectangleWithoutConflictingSquare();
        System.out.println("Rectangle without conflicts: " + rectangle.x + "," + rectangle.y + ": " + rectangle.width + "x" + rectangle.height);
    }
}
