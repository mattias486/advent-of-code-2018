package com.mattias.advent.day04;

import org.junit.Ignore;
import org.junit.Test;

import java.io.FileNotFoundException;

public class Day04Test {

    Day04 day04 = new Day04();

    @Ignore("Part 1")
    @Test
    public void getGuardSleepingTheMost() throws FileNotFoundException {
        Integer calculated = day04.getGuardSleepingTheMost();
        System.out.println("Guard multiplied by max minute : " + calculated);
    }

    @Ignore("Part 2")
    @Test
    public void getGuardSleepingTheMostOnASingleMinute() throws FileNotFoundException {
        Integer calculated = day04.getGuardSleepingTheMostOnTheSameMinute();
        System.out.println("Guard multiplied by max minute (2nd strategy) : " + calculated);
    }
}
