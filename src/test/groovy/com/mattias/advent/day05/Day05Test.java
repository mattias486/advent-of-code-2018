package com.mattias.advent.day05;

import org.junit.Ignore;
import org.junit.Test;

import java.io.FileNotFoundException;

public class Day05Test {

    private Day05 day05 = new Day05();

    @Ignore("Part 1")
    @Test
    public void getRemainingString() throws FileNotFoundException {
        String remainingString = day05.getRemainingString();
        System.out.println("Units after replacements : " + remainingString.length());
    }

    //Sadly code for solving day05 part 2 was not pushed on a previous development machine
}
