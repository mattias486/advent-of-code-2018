package com.mattias.advent.day06;

import org.junit.Ignore;
import org.junit.Test;

import java.io.FileNotFoundException;

public class Day06Test {

    Day06 day06 = new Day06();

    @Ignore("Part 1")
    @Test
    public void getBestZonePart1() throws FileNotFoundException {
        Integer maxRegionSize = day06.calculateLargestArea();

        System.out.println("Max region size : " + maxRegionSize);
    }

    @Ignore("Part 2")
    @Test
    public void calculateRegionOfTotalDistanceToPoints() throws FileNotFoundException {
        Long regionSize = day06.calculateRegionOfTotalDistancesToPoints();

        System.out.println("Region size : " + regionSize);
    }
}
