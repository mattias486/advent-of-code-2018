package com.mattias.advent.day06;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class PointTest {

    @Test
    public void testDistanceToPoint() {
        Point point = new Point(10, 20, null);

        assertEquals(30, point.getDistanceToPoint(0, 0));
        assertEquals(30, point.getDistanceToPoint(20, 40));
    }
}
