package com.mattias.advent.day07;

import org.junit.Ignore;
import org.junit.Test;

import java.io.FileNotFoundException;

public class Day07Test {

    private Day07 day07 = new Day07();

    @Ignore("Part 1")
    @Test
    public void testGetAssemblyOrder() throws FileNotFoundException {
        String order = day07.getAssemblyOrder();

        System.out.println("Assembly order : " + order);
    }

    @Ignore("Part 2")
    @Test
    public void getAssemblyTimeWithMultipleWorkers() throws FileNotFoundException {
        Integer assemblyTime = day07.getAssemblyTimeWithMultipleWorkers();

        System.out.println("Assembly time : " + assemblyTime);
    }
}
