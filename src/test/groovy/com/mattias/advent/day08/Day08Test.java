package com.mattias.advent.day08;

import org.junit.Ignore;
import org.junit.Test;
import java.io.FileNotFoundException;

public class Day08Test {

    Day08 day08 = new Day08();

    @Test
    @Ignore("Part 1")
    public void getLicenseMetadataSum() throws FileNotFoundException {
        Integer licenseMetadataSum = day08.getLicenseMetadataSum();

        System.out.println("License metadata sum : " + licenseMetadataSum);
    }

    @Test
    @Ignore("Part 2")
    public void calculatedAlternativeLicenseChecksum() throws FileNotFoundException {
        Integer calculatedChecksum = day08.calculatedAlternativeLicenseChecksum();

        System.out.println("License checksum : " + calculatedChecksum);
    }
}
