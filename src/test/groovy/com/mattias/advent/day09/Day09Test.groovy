package com.mattias.advent.day09

class Day09Test extends Specification {

    def day09 = Day09.builder().build()

    @Ignore("Part 1")
    def "Part 1 score"() {
        when:
        def highestScore = day09.calculatedHighestScore(465, 71940)

        then:
        System.out.println "Highest score : " + highestScore
    }

    @Ignore("Part 2")
    def "Part 2 score"() {
        when:
        def highestScore = day09.calculatedHighestScore(465, 7194000)

        then:
        System.out.println "Highest score : " + highestScore
    }

    @Unroll
    def "Game sanity check"() {
        when:
        def highestScore = day09.calculatedHighestScore(players, last_title)

        then:
        highestScore == expected_score

        where:
        players | last_title | expected_score
        10      | 1618       | 8317
        17      | 1104       | 2764
        13      | 7999       | 146373
        21      | 6111       | 54718
        30      | 5807       | 37305
        465     | 71940      | 384475
    }

    @Unroll
    def "Test skipping removed tiles to the right"() {
        given:
        def day09 = Day09.builder()
                .circleTiles(circle_tiles)
                .build()

        when:
        def result = day09.getNoneNullTileIndexRight(offset)

        then:
        result == expected_result

        where:
        offset | expected_result | circle_tiles
        0      | 3               | [1, null, null, 4, null, 6]
        5      | 0               | [1, null, null, 4, null, 6]
        3      | 4               | [1, null, null, 4, 5, 6]
    }

    @Unroll
    def "Test remove tiles to the left"() {
        given:
        def day09 = Day09.builder()
                .circleTiles(circle_tiles)
                .build()

        when:
        def result = day09.getNoneNullTileIndexLeft(offset)

        then:
        result == expected_result

        where:
        offset | expected_result | circle_tiles
        0      | 5               | [1, null, null, 4, null, 6]
        5      | 3               | [1, null, null, 4, null, 6]
        3      | 0               | [1, null, null, 4, 5, 6]
        0      | 4               | [1, null, null, 4, 5, null]
    }

    @Unroll
    def "Test get seven tiles to the left"() {
        given:
        def day09 = Day09.builder()
                .circleTiles(circle_tiles)
                .currentTileIndex(offset)
                .build()

        when:
        def result = day09.getIndexSevenTilesToTheLeft()

        then:
        result == expected_result

        where:
        offset | expected_result | circle_tiles
        7      | 0               | [1, 2, 3, 4, 5, 6, 7, 8]
        11     | 1               | [1, 2, 3, 4, 5, 6, 7, 8, null, null, null, 9]
        1      | 3               | [1, 2, 3, 4, 5, 6, 7, 8, null, null, null, 9]
        3      | 0               | [1, null, null, 4, 5, null]

    }

    @Unroll
    def "Test get two tiles to the right"() {
        given:
        def day09 = Day09.builder()
                .circleTiles(circle_tiles)
                .build()

        when:
        def result = day09.getIndexTwoTilesToTheRight(offset)

        then:
        result == expected_result

        where:
        offset | expected_result | circle_tiles
        0      | 5               | [1, null, null, 4, null, 6]
        0      | 3               | [1, 2, null, 4, null, 6]
        5      | 3               | [1, null, null, 4, 5, 6]
        3      | 0               | [1, null, null, 4, 5, null]
    }
}
import spock.lang.Ignore
import spock.lang.Specification

import spock.lang.Unroll
