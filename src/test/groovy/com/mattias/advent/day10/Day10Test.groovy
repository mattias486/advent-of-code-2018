package com.mattias.advent.day10

import spock.lang.Ignore
import spock.lang.Specification

class Day10Test extends Specification {

    def day10 = new Day10()

    @Ignore("Part 1 and Part 2 - Part 1 message can be found in the output")
    def "Find message and time"() {
        when:
        true

        then:
        day10.findMessage()
    }

    def "Verify parsing of points of light"() {
        when:
        def lights = day10.getPointsOfLight()
        def firstLight = lights[0]

        then:
        lights.size() > 1
        firstLight.position.x == 31351
        firstLight.position.y == -51811
        firstLight.velocity.right == -3
        firstLight.velocity.down == 5
    }

    def "Test move light"() {
        given:
        def light = PointOfLight.builder()
                .position(Position.builder()
                    .x(-10)
                    .y(20)
                    .build())
                .velocity(Velocity.builder()
                    .right(-4)
                    .down(-1)
                    .build())
                .build()

        when:
        2.times { day10.moveLight(light) }

        then:
        light.position.x == -18
        light.position.y == 18
    }

    def "Test print message"() {
        given:
        def lights = [
                getLightWithoutVelocity(0, 0),
                getLightWithoutVelocity(0, 1),
                getLightWithoutVelocity(0, 2),
                getLightWithoutVelocity(1, 1),
                getLightWithoutVelocity(2, 0),
                getLightWithoutVelocity(2, 1),
                getLightWithoutVelocity(2, 2)
        ]

        when:
        def message = day10.getMessage(lights)

        then:
        message == ([
                "X X",
                "XXX",
                "X X"
        ] as String[])
    }

    def getLightWithoutVelocity(int x, int y) {
        PointOfLight.builder()
                .position(Position.builder()
                    .x(x)
                    .y(y)
                    .build())
                .build()
    }
}
