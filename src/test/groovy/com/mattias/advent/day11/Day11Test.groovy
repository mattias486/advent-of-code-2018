package com.mattias.advent.day11

import spock.lang.Ignore
import spock.lang.Specification
import spock.lang.Unroll

class Day11Test extends Specification {

    def day11 = new Day11()

    @Unroll
    def "Test 100 digit for input : #input"() {
        when:
        def result = day11.get100Digit(input)

        then:
        result == expected_result

        where:
        input | expected_result
        12345 | 3
        900   | 9
        99    | 0
        0     | 0
    }

    @Unroll
    def "Test calculate power level for serial : #serial_number"() {
        when:
        def result = day11.getPowerLevel(x, y, serial_number)

        then:
        result == expected_result

        where:
        x   | y   | serial_number | expected_result
        122 | 79  | 57            | -5
        217 | 196 | 39            | 0
        101 | 153 | 71            | 4
    }

    @Unroll
    def "Test calculate square power level for serial : #serial_number"() {
        given:
        def powerLevels = day11.powerLevels(serial_number)

        when:
        def power = day11.getTotalPowerForSquare(powerLevels, xIndex, yIndex, 3)

        then:
        power == expected_power

        where:
        serial_number | expected_power | xIndex | yIndex
        18            | 29             | 32     | 44
        42            | 30             | 20     | 60
    }

    @Unroll
    def "Find largest power square sanity check"() {
        when:
        def fuelCellSquare = day11.getSquareWithLargestTotalPowerFixedCellDimensions(serial_number)

        then:
        fuelCellSquare.toString() == coordinate
        fuelCellSquare.totalPower == total_power

        where:
        serial_number | coordinate | total_power
        18            | "33,45"    | 29
        42            | "21,61"    | 30
    }

    @Ignore("Part 1")
    def "Find largest power square with serial 5093"() {
        given:
        def serialNumber = 5093

        when:
        def result = day11.getSquareWithLargestTotalPowerFixedCellDimensions(serialNumber)

        then:
        System.out.println("Top left square coordinates for 3x3 square with largest total power : " + result)
    }

    @Ignore("This unit test took around 4-5minutes on my machine, so disabling it for now")
    def "Variable cell dimension sanity test"() {
        when:
        def fuelCellSquare = day11.getSquareWithLargestTotalPowerVariableCellDimensions(serial_number)

        then:
        fuelCellSquare.toStringWithDimension() == coordinateAndDimension
        fuelCellSquare.totalPower == total_power

        where:
        serial_number | coordinateAndDimension | total_power
        18            | "90,269,16"            | 113
        42            | "232,251,12"           | 119
    }

    @Ignore("Part 2 - This takes above one minute to run on my machine")
    def "Find the largest power square of any size with serial 5093"() {
        given:
        def serialNumber = 5093

        when:
        def result = day11.getSquareWithLargestTotalPowerVariableCellDimensions(serialNumber)

        then:
        System.out.println("Top left square coordinates and dimension square with largest total power : " + result.toStringWithDimension())
    }

}
