package com.mattias.advent.day12

import spock.lang.Ignore
import spock.lang.Specification
import spock.lang.Unroll

class Day12Test extends Specification {

    def day12 = new Day12()

    def "Test reading initial state and rules"() {
        when:
        def flowerPots = day12.getFlowerPots();

        then:
        flowerPots.plants.length == 99
        flowerPots.plants[0]
        !flowerPots.plants[3]
        flowerPots.plants[98]

        flowerPots.rules.size() == 32
        !flowerPots.rules[0]
        !flowerPots.rules[26]
        flowerPots.rules[5]
        flowerPots.rules[31]
        flowerPots.rules.size() == 32
    }

    def "Test next generation does not increase size size of plants if new plants were not added "() {
        given:
        def flowerPots = day12.getFlowerPots()
        flowerPots = flowerPots.toBuilder()
                .plants([true, true, false, true, true, false] as boolean[]) // "##.##."
                .build()

        when:
        flowerPots.nextPlantGeneration(i)

        then:
        //since we clean up the empty pots to the left, zero plant index is actually increased (and plant size is lowered)
        flowerPots.zeroValuePlantIndex == 1
        flowerPots.plants.size() == 5
        flowerPots.plants == [true, false, false, true, true] as boolean[] // "..#..##."
    }

    def "Test next generation does increase size size of plants if new plants are added "() {
        given:
        def flowerPots = day12.getFlowerPots()
        flowerPots = flowerPots.toBuilder()
                .plants([true, false, false, true, false, true] as boolean[])
                .build()

        when:
        flowerPots.nextPlantGeneration(i)

        then:
        flowerPots.zeroValuePlantIndex == -1
        flowerPots.plants.size() == 8
        flowerPots.plants == [true, true, false, true, true, true, true, true] as boolean[]
    }

    @Ignore("Part 1")
    def "Get sum of plants after 20 generations"() {
        given:
        def plants = day12.getFuturePlantGenerations(20)

        when:
        def sumAfter20Generations = plants.getSumOfPlants()

        then:
        System.out.println("Sum of plants after 20 generations : " + sumAfter20Generations)
    }

    @Unroll
    def "Test get rule value as integer"() {
        when:
        def result = day12.getRuleFromString(rule.toCharArray())

        then:
        result == expected_result

        where:
        rule | expected_result
        "....." | 0
        "....#" | 1
        "#.#.#" | 21
        "#...." | 16
    }

    def "Get sum of plants after 10M generations"() {
        given:
        def plants = day12.getFuturePlantGenerations(10_000_000)

        when:
        def sumAfterGenerations = plants.getSumOfPlants()

        then:
        System.out.println("Sum of plants after 10m generations : " + sumAfterGenerations)
    }

    @Ignore("Part 2")
    def "Get sum of plants after 50 billion generations"() {
        given:
        def plants = day12.getFuturePlantGenerations(50_000_000_000)

        when:
        def sumAfterGenerations = plants.getSumOfPlants()

        then:
        System.out.println("Sum of plants after 50 billion generations : " + sumAfterGenerations)
    }

}
