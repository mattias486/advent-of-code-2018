package com.mattias.advent.day12

import spock.lang.Specification
import spock.lang.Unroll

class FlowerPotsTest extends Specification {

    def "get sum of plants with negative values"() {
        given:
        def flowerPots = FlowerPots.builder()
                .zeroValuePlantIndex(-3)
                .plants([true, true, false, true, true, false] as boolean[])
                .build()
        when:
        def result = flowerPots.getSumOfPlants()

        then:
        result == -4
    }

    @Unroll
    def "get rule input as integer for plant rules for index : #index"() {
        given:
        def flowerPots = FlowerPots.builder()
                .plants([true, true, false, true, true, false] as boolean[]) // "##.##."
                .build()

        when:
        def result = flowerPots.getPlantRuleIndex(index)

        then:
        result == expected_result

        where:
        index | expected_result
        0     | 6 // "..##."
        2     | 27 // "##.##"
        5     | 24 // "##..."
        -1    | 3 // "...##"
        -2    | 1 // "....#"
        6     | 16 // "#...."
        7     | 0 // "....."
    }
}
