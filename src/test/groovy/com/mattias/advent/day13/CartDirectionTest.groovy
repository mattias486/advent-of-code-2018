package com.mattias.advent.day13

import spock.lang.Specification
import spock.lang.Unroll

class CartDirectionTest extends Specification {

    @Unroll
    def "Turn left, from #current_direction to #direction_after_turn"() {
        when:
        def result = ((CartDirection)current_direction).getLeft()

        then:
        result == direction_after_turn

        where:
        current_direction | direction_after_turn
        CartDirection.DOWN | CartDirection.RIGHT
        CartDirection.UP | CartDirection.LEFT
        CartDirection.LEFT | CartDirection.DOWN
        CartDirection.RIGHT | CartDirection.UP
    }

    @Unroll
    def "Turn right, from #current_direction to #direction_after_turn"() {
        when:
        def result = ((CartDirection)current_direction).getRight()

        then:
        result == direction_after_turn

        where:
        current_direction | direction_after_turn
        CartDirection.DOWN | CartDirection.LEFT
        CartDirection.UP | CartDirection.RIGHT
        CartDirection.LEFT | CartDirection.UP
        CartDirection.RIGHT | CartDirection.DOWN
    }
}

