package com.mattias.advent.day13

import com.mattias.advent.day13.Track.TrackDirection
import spock.lang.Specification
import spock.lang.Unroll

class CartTest extends Specification {

    @Unroll
    def "Cart next turn preference for : #current_preference"() {
        when:
        def cart = Cart.builder()
                .turnPreference(current_preference)
                .build()

        then:
        cart.turnPreference.getNext() == next_preference

        where:
        current_preference           | next_preference
        Cart.TurnPreference.LEFT     | Cart.TurnPreference.STRAIGHT
        Cart.TurnPreference.STRAIGHT | Cart.TurnPreference.RIGHT
        Cart.TurnPreference.RIGHT    | Cart.TurnPreference.LEFT
    }

    @Unroll
    def "Get direction for cart character : #character"() {
        when:
        def direction = Cart.getDirectionForCart(character as char)

        then:
        direction == expected_direction

        where:
        character | expected_direction
        '^'       | Cart.Direction.UP
        '<'       | Cart.Direction.LEFT
        '>'       | Cart.Direction.RIGHT
        'v'       | Cart.Direction.DOWN
    }

    @Unroll
    def "Test moving cart for direction : #direction"() {
        given:
        def cart = Cart.builder()
                .direction(direction)
                .position(Position.builder()
                .x(10)
                .y(12).build())
                .build()
        when:
        cart.moveCart()

        then:
        cart.position == expected_position

        where:
        direction            | expected_position
        Cart.Direction.LEFT  | getPosition(9, 12)
        Cart.Direction.RIGHT | getPosition(11, 12)
        Cart.Direction.UP    | getPosition(10, 11)
        Cart.Direction.DOWN  | getPosition(10, 13)

    }

    Position getPosition(int x, int y) {
        Position.builder().x(x).y(y).build()
    }

    @Unroll
    def "Switching direction due to curve from #start_direction to #end_direction"() {
        given:
        def cart = Cart.builder()
                .direction(start_direction)
                .position(getPosition(10, 20))
                .build()

        when:
        cart.switchDirectionDueToCurve([current_track, track])

        then:
        cart.direction == end_direction

        where:
        start_direction      | end_direction        | track                                                                     | current_track
        Cart.Direction.LEFT  | Cart.Direction.DOWN  | getTrackWithStartPosition(getPosition(10, 20), TrackDirection.VERTICAL)   | getHorizontalTrack()
        Cart.Direction.LEFT  | Cart.Direction.UP    | getTracksWithEndPosition(getPosition(10, 20), TrackDirection.VERTICAL)    | getHorizontalTrack()
        Cart.Direction.RIGHT | Cart.Direction.DOWN  | getTrackWithStartPosition(getPosition(10, 20), TrackDirection.VERTICAL)   | getHorizontalTrack()
        Cart.Direction.RIGHT | Cart.Direction.UP    | getTracksWithEndPosition(getPosition(10, 20), TrackDirection.VERTICAL)    | getHorizontalTrack()
        Cart.Direction.UP    | Cart.Direction.RIGHT | getTrackWithStartPosition(getPosition(10, 20), TrackDirection.HORIZONTAL) | getVerticalTrack()
        Cart.Direction.UP    | Cart.Direction.LEFT  | getTracksWithEndPosition(getPosition(10, 20), TrackDirection.HORIZONTAL)  | getVerticalTrack()
        Cart.Direction.DOWN  | Cart.Direction.RIGHT | getTrackWithStartPosition(getPosition(10, 20), TrackDirection.HORIZONTAL) | getVerticalTrack()
        Cart.Direction.DOWN  | Cart.Direction.LEFT  | getTracksWithEndPosition(getPosition(10, 20), TrackDirection.HORIZONTAL)  | getVerticalTrack()
    }

    private Track getVerticalTrack() {
        getTrack(TrackDirection.VERTICAL)
    }

    private Track getHorizontalTrack() {
        getTrack(TrackDirection.HORIZONTAL)
    }

    def getTrack(TrackDirection direction) {
        Track.builder()
                .direction(direction)
                .build()
    }

    def getTrackWithStartPosition(Position position, TrackDirection direction) {
        Track.builder()
                .startPosition(position)
                .endPosition(Position.builder().x(0).y(0).build())
                .direction(direction)
                .build()
    }

    def getTracksWithEndPosition(Position position, TrackDirection direction) {
        Track.builder()
                .endPosition(position)
                .startPosition(Position.builder().x(0).y(0).build())
                .direction(direction)
                .build()
    }

}
