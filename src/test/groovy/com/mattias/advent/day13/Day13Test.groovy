package com.mattias.advent.day13

import spock.lang.Ignore
import spock.lang.Specification

class Day13Test extends Specification {

    def day13 = Day13.builder().build()

    def "Test reading carts from input"() {
        given:
        def input = day13.getDay13Input()

        when:
        def carts = day13.getCarts(input)

        then:
        carts.size() == 17
        carts[0].direction == Cart.Direction.LEFT
        carts[0].position.y == 0
        carts[0].position.x == 126
    }

    def "Check for collision when there is a collision"() {
        given:
        def carts = [getCartWithPosition(10, 8), getCartWithPosition(10, 8)]

        when:
        def result = day13.checkForCollision(carts)

        then:
        result
    }

    def "Check for collision when there is not a collision"() {
        given:
        def carts = [getCartWithPosition(10, 8), getCartWithPosition(10, 9),]

        when:
        def result = day13.checkForCollision(carts)

        then:
        !result
    }

    def "Test reading intersections"() {
        given:
        def input = day13.getDay13Input()

        when:
        def intersections = day13.getIntersections(input)

        then:
        intersections.size() > 0
        intersections[0].x == 53
        intersections[0].y == 1
    }

    def "Test reading curves"() {
        given:
        def input = day13.getDay13Input()

        when:
        def curves = day13.getCurves(input)

        then:
        curves.size() > 0
        curves[0].x == 32
        curves[0].y == 0
    }

    Cart getCartWithPosition(int x, int y) {
        Cart.builder()
                .position(Position.builder()
                    .x(x)
                    .y(y)
                    .build()
                )
                .build()
    }

    def "Test moving carts over curves"() {

    }

    def "Test moving carts over intersections"() {

    }

    @Ignore("Part 1")
    def "Move carts until collision"() {
        when:
        def day13 = Day13.builder().build()
        day13.setupCartsAndTracks()

        then:
        day13.moveCartsUntilCollision()
    }

    @Ignore("Part 2")
    def "Move carts until only one cart remain"() {
        when:
        def day13 = Day13.builder().build()
        day13.setupCartsAndTracks()

        then:
        day13.moveCartsUntilOnlyOneCartRemain()
    }
}
