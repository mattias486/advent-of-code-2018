package com.mattias.advent.day13

import spock.lang.Specification

class TrackCreatorTest extends Specification {

    def day13 = Day13.builder().build()
    def trackCreator = new TrackCreator()

    def "Test reading tracks"() {
        given:
        def input = day13.getDay13Input()

        when:
        def tracks = trackCreator.getTracks(input)

        then:
        tracks.size() == 8240

        //first horizontal track
        tracks[0].startPosition.y == 0
        tracks[0].startPosition.x == 32
        tracks[0].endPosition.y == 0
        tracks[0].endPosition.x == 53
        tracks[0].direction == Track.TrackDirection.HORIZONTAL

        //first track with intersection (end position is start position of next track)
        tracks[2].endPosition.y == 1
        tracks[2].endPosition.x == 53
        tracks[2].direction == Track.TrackDirection.HORIZONTAL
        tracks[3].startPosition.y == 1
        tracks[3].startPosition.x == 53
        tracks[3].direction == Track.TrackDirection.HORIZONTAL

        //half of the tracks are vertical, half is horizontal
        tracks[4119].direction == Track.TrackDirection.HORIZONTAL
        tracks[4120].direction == Track.TrackDirection.VERTICAL

        //first vertical track
        tracks[4120].startPosition.x == 0
        tracks[4120].startPosition.y == 80
        tracks[4120].endPosition.x == 0
        tracks[4120].endPosition.y == 108



    }
}
