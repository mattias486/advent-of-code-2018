package com.mattias.advent.day14

import spock.lang.Ignore
import spock.lang.Specification
import spock.lang.Unroll

class Day14Test extends Specification {

    def day14 = Day14.builder().build()

    def "Test sum of recipes"() {
        when:
        def result = day14.getSumOfRecipes()

        then:
        result == 10
    }

    @Unroll
    def "Test get recipes from sum : #sum"() {
        given:
        def day14 = Day14.builder()
                .recipeScores([])
                .build()
        when:
        day14.addNewRecipes(sum)

        then:
        expected_result == day14.recipeScores

        where:
        sum | expected_result
        0   | [0]
        9   | [9]
        10  | [1, 0]
        18  | [1, 8]
    }

    def "Test get new elf index"() {
        given:
        def day14 = Day14.builder()
                .recipeScores([3, 7, 1, 0, 1, 0])
                .build()

        when:
        def elf1Index = day14.getNewElfRecipeIndex(0)
        def elf2Index = day14.getNewElfRecipeIndex(1)

        then:
        elf1Index == 4
        elf2Index == 3
    }

    @Unroll
    def "Score of last 10 recipes after #number_of_recipes recipes"() {
        when:
        def result = day14.getScoreOfThe10LastRecipes(number_of_recipes)

        then:
        result == expected_result

        where:
        number_of_recipes | expected_result
        9                 | "5158916779"
        5                 | "0124515891"
        18                | "9251071085"
        2018              | "5941429882"
        47801             | "1342316410"
    }

    @Ignore("Part 1")
    def "Score for the last 10 recipes after 47801 creations"() {
        when:
        def result = day14.getScoreOfThe10LastRecipes(47801)

        then:
        System.out.println("Score after 47801 creations : " + result)
    }

    @Unroll
    def "Get number of recipes to the left of recipe sequence : #recipe_sequence"() {
        when:
        def result = day14.getNumberOfRecipesLeftOfSequence(recipe_sequence)

        then:
        result == expected_number_of_recipes

        where:
        recipe_sequence | expected_number_of_recipes
        "51589"         | 9
        "01245"         | 5
        "92510"         | 18
        "59414"         | 2018
    }

    @Ignore("Part 2")
    def "Get number of recipes to the left of recipe sequence : 047801"() {
        when:
        def result = day14.getNumberOfRecipesLeftOfSequence("047801")

        then:
        System.out.println("Number of recipes left of sequence : " + result)
    }
}