package com.mattias.advent.day15

import spock.lang.Specification
import spock.lang.Unroll

class AttackMechanicsTest extends Specification {

    def attackMechanics = new AttackMechanics()

    @Unroll
    def "Test get units in range for attack : #comment"() {
        given:
        def currentUnit = FightingUnit.builder()
                .position(new Position(9, 10))
                .build()

        when:
        def result = attackMechanics.getEnemiesInRangeForAttack(currentUnit, enemies)

        then:
        result == expected_result

        where:
        comment                                                       | expected_result                                                     | enemies
        "empty enemies"                                               | []                                                                  | []
        "position not in range is filtered out"                       | []                                                                  | [unitWithPosition(11, 10)]
        "enemy in range is included"                                  | [unitWithPosition(10, 10)]                                          | [unitWithPosition(10, 10)]
        "only enemies in range are included"                          | [unitWithPosition(10, 10)]                                          | [unitWithPosition(11, 11), unitWithPosition(10, 10)]
        "hp is prioritized when multiple enemies in range"            | [unitWithPositionAndHp(9, 9, 4), unitWithPositionAndHp(10, 10, 5)]  | [unitWithPositionAndHp(10, 10, 5), unitWithPositionAndHp(9, 9, 4)]
        "y coordinate is prioritized if hp is equal"                  | [unitWithPositionAndHp(9, 9, 5), unitWithPositionAndHp(10, 10, 5)]  | [unitWithPositionAndHp(10, 10, 5), unitWithPositionAndHp(9, 9, 5)]
        "x coordinate is prioritized if hp and y coordinate is equal" | [unitWithPositionAndHp(8, 10, 5), unitWithPositionAndHp(10, 10, 5)] | [unitWithPositionAndHp(10, 10, 5), unitWithPositionAndHp(8, 10, 5)]
    }


    def unitWithPosition(int x, int y) {
        unitWithPositionAndHp(x, y, 0)
    }

    def unitWithPositionAndHp(int x, int y, int health) {
        FightingUnit.builder()
                .position(new Position(x, y))
                .health(health)
                .build()
    }


    @Unroll
    def "Test attack turn if no enemies in range, enemies : #enemies"() {
        given:
        def attacker = attackTurnAttacker()

        when:
        def result = attackMechanics.playFightingUnitAttackRound(attacker, enemies, gameState.emptyPositions)

        then:
        !result

        where:
        enemies << [[],
                    [unitWithPositionAndHp(12, 9, 8)]]
    }

    @Unroll
    def "Test attack turn if enemy attacked survives"() {
        given:
        def attacker = attackTurnAttacker()

        when:
        def result = attackMechanics.playFightingUnitAttackRound(attacker, enemies, gameState.emptyPositions)

        then:
        result
        enemies.size() == 1

        where:
        enemies << [[unitWithPositionAndHp(11, 9, 8)],
                    [unitWithPositionAndHp(11, 9, 4)]]

    }

    @Unroll
    def "Test attack turn if enemy attacked dies : #enemies"() {
        given:
        def attacker = attackTurnAttacker()
        def initialEnemySize = enemies.size()

        when:
        def result = attackMechanics.playFightingUnitAttackRound(attacker, enemies, gameState.emptyPositions)

        then:
        result
        enemies.size() == initialEnemySize - 1

        where:
        enemies << [[unitWithPositionAndHp(11, 9, 1)],
                    [unitWithPositionAndHp(11, 9, 2)],
                    [unitWithPositionAndHp(11, 9, 3)],
                    [unitWithPositionAndHp(11, 9, 3), unitWithPositionAndHp(11, 9, 4)]]
    }

    private FightingUnit attackTurnAttacker() {
        FightingUnit.builder()
                .position(new Position(10, 9))
                .attackPower(3)
                .build()
    }


    @Unroll
    def "Is #first adjacent to #second"() {
        when:
        boolean result = attackMechanics.isAdjacentTo(first, second)

        then:
        result == expected_result

        where:
        expected_result | first               | second
        false           | new Position(10, 10) | new Position(10, 10)
        true            | new Position(10, 10) | new Position(11, 10)
        true            | new Position(10, 10) | new Position(9, 10)
        true            | new Position(10, 10) | new Position(10, 11)
        true            | new Position(10, 10) | new Position(10, 9)
        false           | new Position(10, 10) | new Position(11, 11)
        false           | new Position(10, 10) | new Position(9, 9)
        false           | new Position(10, 10) | new Position(11, 9)
        false           | new Position(10, 10) | new Position(9, 11)
    }
}
