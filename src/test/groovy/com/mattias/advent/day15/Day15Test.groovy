package com.mattias.advent.day15

import com.mattias.advent.common.FileUtil
import spock.lang.Ignore
import spock.lang.Specification
import spock.lang.Unroll

class Day15Test extends Specification {

    def day15 = new Day15()
    def fileUtils = new FileUtil()

    def "Test reading input"() {
        when:
        def input = day15.getInput()

        then:
        input.length > 0
        input[0].length > 0
        input[0][0] == '#' as char
        input[31][31] == '#' as char
    }

    def "Test reading elves"() {
        given:
        def input = day15.getInput()

        when:
        def elves = day15.getElves(input, 3)

        then:
        elves.size() == 10
        elves.get(0).team == GameResult.Team.ELVES
        elves.get(0).position.x == 23
        elves.get(0).position.y == 12
    }

    def "Test reading walls"() {
        given:
        def input = day15.getInput()

        when:
        def walls = day15.getWalls(input)

        then:
        walls.size() == 599
        walls[0].x == 0
        walls[0].y == 0
        walls[598].x == 31
        walls[598].y == 31
    }

    def "Test reading empty positions"() {
        given:
        def input = day15.getInput()

        when:
        def emptySpots = day15.getEmptyFields(input)

        then:
        emptySpots.size() == 395
        emptySpots[0].x == 10
        emptySpots[0].y == 1
        emptySpots[394].x == 13
        emptySpots[394].y == 30
    }

    def "Test reading goblins"() {
        given:
        def input = day15.getInput()

        when:
        def goblins = day15.getGoblins(input)

        then:
        goblins.size() == 20
        goblins.get(0).team == GameResult.Team.GOBLINS
        goblins.get(0).position.x == 16
        goblins.get(0).position.y == 4
    }

    @Unroll
    def "Play test game round : #input_file"() {
        given:
        def input = fileUtils.getFileContentAsCharacterArray("day15/" + input_file)

        when:
        def result = day15.playGame(input, 3, GameState.GameMode.EQUAL_ATTACK_POWER)

        then:
        result.numberOfCompletedRounds == number_of_rounds
        result.totalHpRemaining == sum_of_hp
        result.totalScore == score
        result.winningTeam == winning_team

        where:
        input_file    | number_of_rounds | sum_of_hp | score | winning_team
        "testCombat1" | 47               | 590       | 27730 | GameResult.Team.GOBLINS
        "testCombat2" | 37               | 982       | 36334 | GameResult.Team.ELVES
        "testCombat3" | 46               | 859       | 39514 | GameResult.Team.ELVES
        "testCombat4" | 35               | 793       | 27755 | GameResult.Team.GOBLINS
        "testCombat5" | 54               | 536       | 28944 | GameResult.Team.GOBLINS
        "testCombat6" | 20               | 937       | 18740 | GameResult.Team.GOBLINS
    }

    @Unroll
    def "Play test game with attack power : #input_file"() {
        given:
        def input = fileUtils.getFileContentAsCharacterArray("day15/" + input_file)

        when:
        def result = day15.playGameWithoutDeadElves(input)

        then:
        result.numberOfCompletedRounds == number_of_rounds
        result.totalHpRemaining == sum_of_hp
        result.totalScore == score
        result.winningTeam == winning_team

        where:
        input_file    | number_of_rounds | sum_of_hp | score | winning_team          | expected_end_attack_power
        "testCombat1" | 29               | 172       | 4988  | GameResult.Team.ELVES | 15
        "testCombat3" | 33               | 948       | 31284 | GameResult.Team.ELVES | 4
        "testCombat4" | 37               | 94        | 3478  | GameResult.Team.ELVES | 15
        "testCombat5" | 39               | 166       | 6474  | GameResult.Team.ELVES | 12
        "testCombat6" | 30               | 38        | 1140  | GameResult.Team.ELVES | 34
    }


    @Ignore("Part 1")
    def "Play part 1 game"() {
        given:
        def input = fileUtils.getFileContentAsCharacterArray("day15input")

        when:
        def result = day15.playGame(input, 3, GameState.GameMode.EQUAL_ATTACK_POWER)

        then:
        System.out.println("Score after game : " + result.totalScore)
    }

    @Unroll
    def "Test get next attack power"() {
        when:
        def result = day15.getAttackPowerForNextGame(max_with_deaths, min_without_deaths)

        then:
        result == expected_result

        where:
        expected_result | max_with_deaths | min_without_deaths
        153             | 6               | 300
        63              | 62              | 64
    }

    @Unroll
    def "Test found minimum attack power"() {
        when:
        def result = day15.foundMinimumAttackPowerToKeepElvesAlive(maximum, minimum)

        then:
        result == expected_result

        where:
        expected_result | maximum | minimum
        false           | 3       | 300
        true            | 34      | 35
    }

    @Ignore("Part 2")
    def "Play game part 2"() {
        given:
        def input = fileUtils.getFileContentAsCharacterArray("day15input")

        when:
        def result = day15.playGameWithoutDeadElves(input)

        then:
        System.out.println("Score after game (part 2) : " + result.totalScore)
    }
}
