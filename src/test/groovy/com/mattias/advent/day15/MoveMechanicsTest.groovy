package com.mattias.advent.day15

import spock.lang.Specification

class MoveMechanicsTest extends Specification {

    def moveMechanics = new MovementMechanics()

    def "Get sorted positions of attack"() {
        given:
        def emptyPositions = [new Position(9, 21),
                              new Position(11, 20), // right
                              new Position(10, 21), // down
                              new Position(10, 19), // up
                              new Position(9, 20), // left
                              new Position(30, 21),
                              new Position(10, 30)]
        def fightingUnitPosition = new Position(10, 20)

        when:
        def positions = moveMechanics.getValidPositionsOfAttack(fightingUnitPosition, emptyPositions)

        then:
        positions.size() == 4
        positions == [new Position(10, 19),
                      new Position(9, 20),
                      new Position(11, 20),
                      new Position(10, 21)
        ]
    }

    def "Test closest position of enemy"() {
        given:
        def unitDistancesToPosition = [:]
        unitDistancesToPosition.put(new Position(10, 10), 5)
        unitDistancesToPosition.put(new Position(9, 9), 4)
        unitDistancesToPosition.put(new Position(8, 8), 3)

        def validPointsOfAttack = [new Position(9, 8), new Position(9, 9), new Position(8, 8)]

        when:
        def result = moveMechanics.selectClosestPositionToEnemy(unitDistancesToPosition, validPointsOfAttack)

        then:
        result == new Position(8, 8)
    }

    def "Test find close position to move to"() {
        given:
        def attacker = getGoblin(10, 8)
        def enemies = [getElf(8, 8), getElf(13, 13), getElf(10, 11)]
        def emptyPositions = []
        (6..16).each { y ->
            (6..16).each { x ->
                def position = new Position(x, y);
                if (!enemies.contains(position) &&  position != attacker.position) {
                    emptyPositions.add(position)
                }
            }
        }

        when:
        def result = moveMechanics.getPositionToMoveTo(attacker, enemies, emptyPositions)

        then:
        result == new Position(9, 8)
    }

    def "Test find further position to move against"() {
        given:
        def attacker = getGoblin(10, 8)
        def enemies = [getElf(13, 13)]
        def emptyPositions = []
        (6..16).each { y ->
            (6..16).each { x ->
                def position = new Position(x, y);
                if (!enemies.contains(position) &&  position != attacker.position) {
                    emptyPositions.add(position)
                }
            }
        }

        when:
        def result = moveMechanics.getPositionToMoveTo(attacker, enemies, emptyPositions)

        then:
        result == new Position(11, 8)
    }

    def "Test get first position to move against target"() {
        given:
        def unitPosition = new Position(11, 12)
        def destination = new Position(13, 14)

        def parentNodes = new HashMap<Position, Position>()
        parentNodes.put(new Position(13, 14), new Position(12, 14))
        parentNodes.put(new Position(12, 14), new Position(12, 13))
        parentNodes.put(new Position(12, 13), new Position(11, 13))
        parentNodes.put(new Position(11, 13), new Position(11, 12))

        when:
        def result = moveMechanics.getFirstPositionToMoveTowardsTarget(unitPosition, destination, parentNodes)

        then:
        result == new Position(11, 13)
    }

    def "Test get closest position of attack"() {
        given:
        def enemies = [getElf(8, 8), getElf(13, 13), getElf(10, 11)]

        def unitDistancesToPosition = [:]
        enemies.each { enemy ->
            unitDistancesToPosition.put(new Position(enemy.position.x, enemy.position.y - 1), 3)
            unitDistancesToPosition.put(new Position(enemy.position.x - 1, enemy.position.y), 4)
            unitDistancesToPosition.put(new Position(enemy.position.x + 1, enemy.position.y), 2)
            unitDistancesToPosition.put(new Position(enemy.position.x, enemy.position.y + 1), 6)
        }

        def emptyPositions = []
        (6..16).each { y ->
            (6..16).each { x ->
                def position = new Position(x, y);
                if (!enemies.contains(position)) {
                    emptyPositions.add(position);
                }
            }
        }

        when:
        def result = moveMechanics.getClosestPlaceOfAttack(unitDistancesToPosition, enemies, emptyPositions)

        then:
        result.size() == 3
        result[enemies[0]] == new Position(9, 8)
        result[enemies[1]] == new Position(14, 13)
        result[enemies[2]] == new Position(11, 11)
    }

    def "Test get closes position of attack when enemy is not attackable"() {
        given:
        def enemies = [getElf(10, 10)]
        def unitDistancesToPosition = [:]
        def emptyPositions = []

        when:
        def result = moveMechanics.getClosestPlaceOfAttack(unitDistancesToPosition, enemies, emptyPositions)

        then:
        result == [:]
    }

    FightingUnit getElf(int x, int y) {
        FightingUnit.builder()
                .team(GameResult.Team.ELVES)
                .position(new Position(x, y))
                .build()
    }

    FightingUnit getGoblin(int x, int y) {
        FightingUnit.builder()
                .team(GameResult.Team.GOBLINS)
                .position(new Position(x, y))
                .build()
    }

}
