package com.mattias.advent.day15

import spock.lang.Specification

class ShortestPathTest extends Specification {

    def "Test shortest path"() {
        given:
        def targetPosition = new Position(10, 10)

        def positionsCloseToTarget = [
                new Position(10, 11),
                new Position(10, 9),
                new Position(9, 10),
                new Position(11, 10)
        ]

        def positionWithDistance2 = new Position(10, 8)
        def positionWithDistance3 = new Position(10, 7)

        def validPositions = [positionWithDistance2,
                              positionWithDistance3]
        validPositions.addAll(positionsCloseToTarget)

        def shortestPath = new ShortestPath()

        when:
        def result = shortestPath.calculateDistances(targetPosition, validPositions)

        then:
        result.size() == 7
        result[targetPosition] == 0
        positionsCloseToTarget.each { it -> result[it] == 1 }
        result[positionWithDistance2] == 2
        result[positionWithDistance3] == 3
    }
}
