package com.mattias.advent.day15

import spock.lang.Specification

class VisualizationTest extends Specification {

    def visualization = new Visualization()

    def "Print empty positions"() {
        given:
        def day15 = new Day15()
        def input = day15.getInput()
        def emptyFields = day15.getEmptyFields(input)

        when:
        visualization.drawFreePositions(emptyFields)

        then:
        true
    }
}
