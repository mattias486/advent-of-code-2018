package com.mattias.advent.day16

import spock.lang.Ignore
import spock.lang.Specification

class Day16Test extends Specification{

    def day16 = new Day16()

    def "Read input"() {
        when:
        def input = day16.getOperandBehavior()

        then:
        input.size() > 0
        input[0].registersBeforeOperation == [0, 2, 2, 2] as int[]
        input[0].operation == [4, 2, 3, 2] as int[]
        input[0].registersAfterOperations == [0, 2, 5, 2] as int[]

        input[1].registersBeforeOperation == [2, 2, 1, 3] as int[]
        input[1].operation == [3, 1, 0, 2] as int[]
        input[1].registersAfterOperations == [2, 2, 1, 3] as int[]

        input[input.size() - 1].registersBeforeOperation == [1, 2, 2, 0] as int[]
        input[input.size() - 1].operation == [14, 1, 2, 2] as int[]
        input[input.size() - 1].registersAfterOperations == [1, 2, 4, 0] as int[]
    }

    def "Read test program"() {
        when:
        def input = day16.getProgramOperations()

        then:
        input[0] == [1, 0, 2, 3] as int[]
        input[input.size() - 1] == [5, 3, 0, 0] as int[]
    }

    @Ignore("Part 1")
    def "Test which operand behaviors that match 3 or more instructions"() {
        given:
        def input = day16.getOperandBehavior()

        when:
        def operands = day16.getOperandBehaviorsMatchingMultipleOperands(input, 3)

        then:
        System.out.println("Operand behaviors matching 3 or more instructions : " + operands.size())
    }

    @Ignore("Part 2")
    def "Figure out op codes and run program"() {
        given:
        def operandBehaviors = day16.getOperandBehavior()
        def program = day16.getProgramOperations()

        when:
        def operandsAndIdentifiers = day16.getIdentifiedOperations(operandBehaviors)
        def registerAfterRunning = day16.runProgram(program, operandsAndIdentifiers)

        then:
        System.out.println("Register 0 after running the program : " + registerAfterRunning[0]);
    }
}
