package com.mattias.advent.day17

import spock.lang.Ignore
import spock.lang.Specification

class Day17Test extends Specification {

    def day17 = new Day17()
    def waterLogic = new WaterLogic()

    def "Test get vertical positions"() {
        given:
        def inputRow = "x=569, y=570..582"

        when:
        def positions = day17.getPositions(inputRow)

        then:
        positions.size() == 13
        positions[0] == new Position(569, 570)
        positions[positions.size() - 1] == new Position(569, 582)
    }

    def "Test get horizontal positions"() {
        given:
        def inputRow = "y=372, x=495..519"

        when:
        def positions = day17.getPositions(inputRow)

        then:
        positions.size() == 25
        positions[0] == new Position(495, 372)
        positions[positions.size() - 1] == new Position(519, 372)
    }

    def "Test get all clay positions"() {
        when:
        def positions = day17.getClayPositions()

        then:
        positions.size() == 19267
    }

    @Ignore("Part 1")
    def "Get number of reachable water positions"() {
       given:
       def clay = day17.getClayPositions()

        when:
        def reachableWaterTiles = waterLogic.getNumberOfReachableWaterTiles(clay)

        then:
        System.out.println("Reachable water tiles : " + reachableWaterTiles)
    }

    @Ignore("Part 2")
    def "Get number of settled water positions"() {
        given:
        def clay = day17.getClayPositions()

        when:
        def reachableWaterTiles = waterLogic.getNumberOfSettledWaterTiles(clay)

        then:
        System.out.println("Settled water tiles : " + reachableWaterTiles)
    }
}
