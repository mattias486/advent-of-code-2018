package com.mattias.advent.day17;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

class Visualization {

    void drawPositions(int turn, List<Position> clayPositions, List<Position> reachableWaterPositions, List<Position> settledWater) {
        char[][] visualization = getVisualization(clayPositions, reachableWaterPositions, settledWater);
        System.out.println("=== turn " + turn + " ====");
        for (int y = 0; y < visualization.length; y++) {
            System.out.println(String.format("%03d %s", y, new String(visualization[y])));
        }
    }

    private char[][] getVisualization(List<Position> clayPositions, List<Position> reachableWaterPositions, List<Position> settledWater) {
        int ySize = PositionUtil.getMaxY(clayPositions) + 1;
        int minX = getMinX(clayPositions, reachableWaterPositions);
        int maxX = getMaxX(clayPositions, reachableWaterPositions);
        int xSize = maxX - minX + 1;
        char[][] visual = new char[ySize][];
        for (int y = 0; y < ySize; y++) {
            visual[y] = new char[xSize];
            Arrays.fill(visual[y], ' ');
            addCharacterForPositions(visual[y], y, minX, '#', clayPositions);
            addCharacterForPositions(visual[y], y, minX, '|', reachableWaterPositions);
            addCharacterForPositions(visual[y], y, minX, '~', settledWater);
        }
        return visual;
    }

    private int getMinX(List<Position> clayPositions, List<Position> reachableWaterPositions) {
        int min = PositionUtil.getMinX(clayPositions);
        int minReachable = PositionUtil.getMinX(reachableWaterPositions);
        return Math.min(min, minReachable);
    }

    private int getMaxX(List<Position> clayPositions, List<Position> reachableWaterPositions) {
        int max = PositionUtil.getMaxX(clayPositions);
        int maxReachable = PositionUtil.getMaxX(reachableWaterPositions);
        return Math.max(max, maxReachable);
    }

    private void addCharacterForPositions(char[] visual, int row, int columnOffset, char character, List<Position> positions) {
        getPositionsAtRow(positions, row).forEach(
                position -> visual[position.x - columnOffset] = character
        );
    }

    private List<Position> getPositionsAtRow(List<Position> positions, int row) {
        return positions.stream()
                .filter(position -> position.y == row)
                .collect(Collectors.toList());
    }

}
