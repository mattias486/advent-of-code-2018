package com.mattias.advent.day17;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.tuple.Pair;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@Slf4j
public class WaterLogic {

    private Visualization visualization = new Visualization();

    enum WaterDirection {
        UP, DOWN, LEFT, RIGHT
    }

    Position waterSource = new Position(500, 0);

    List<Position> settledWater;
    List<Position> reachableWaterPositions;
    List<Position> clayPositions;

    Queue<Pair<Position, WaterDirection>> waterPositionsToVisit = new ArrayDeque<>();

    int minY;
    int maxY;

    int turn = 0;

    int getNumberOfReachableWaterTiles(List<Position> clayPositions) {
        setupInternals(clayPositions);
        addWater();
        visualization.drawPositions(turn, clayPositions, reachableWaterPositions, settledWater);
        return getPositionsWithinRange(settledWater).size() + getPositionsWithinRange(reachableWaterPositions).size();
    }

    int getNumberOfSettledWaterTiles(List<Position> clayPositions) {
        setupInternals(clayPositions);
        addWater();
        visualization.drawPositions(turn, clayPositions, reachableWaterPositions, settledWater);
        return getPositionsWithinRange(settledWater).size();
    }

    void addWater() {
        waterPositionsToVisit.add(Pair.of(waterSource, WaterDirection.DOWN));
        while (!waterPositionsToVisit.isEmpty()) {
            Pair<Position, WaterDirection> pair = waterPositionsToVisit.poll();
            Position position = pair.getLeft();
            WaterDirection direction = pair.getRight();
            if (clayPositions.contains(position)) {
                continue;
            }
            if (position.y > maxY) {
                continue;
            }
            if (settledWater.contains(position)) {
                //this is needed for the case where two water streams have entered a reservoir and water have
                //already settled when "visiting" the position of the other stream. Water should continue to raise
                //also for the second stream
                queueIfNotAlreadyQueued(Pair.of(position.up(), WaterDirection.UP));
                continue;
            }
            addWaterToPositionIfViable(position);
            getPositionsToVisit(position, direction);
            turn++;
        }
    }

    List<Position> getPositionsWithinRange(List<Position> positions) {
        return positions.stream()
                .filter(position -> position.y >= minY)
                .filter(position -> position.y <= maxY)
                .collect(Collectors.toList());
    }

    void addWaterToPositionIfViable(Position position) {
        if (!reachableWaterPositions.contains(position)) {
            reachableWaterPositions.add(position);
        }
        if (isInReservoir(position)) {
            addSettledWater(position);
            //possibly find all reachable positions above here and queue water going up on all those positions
        }
    }


    void queueIfNotAlreadyQueued(Pair<Position, WaterDirection> pair) {
        if(!waterPositionsToVisit.contains(pair)) {
            waterPositionsToVisit.add(pair);
        }
    }

    private void getPositionsToVisit(Position position, WaterDirection direction) {
        boolean isSettledWater = settledWater.contains(position);
        if (isSettledWater) {
            queueIfNotAlreadyQueued(Pair.of(position.up(), WaterDirection.UP));
            return;
        }
        switch (direction) {
            case UP:
                queueIfNotAlreadyQueued(Pair.of(position.right(), WaterDirection.RIGHT));
                queueIfNotAlreadyQueued(Pair.of(position.left(), WaterDirection.LEFT));
                break;
            case DOWN:
                if (isClayOrSettledWaterBelow(position)) {
                    queueIfNotAlreadyQueued(Pair.of(position.left(), WaterDirection.LEFT));
                    queueIfNotAlreadyQueued(Pair.of(position.right(), WaterDirection.RIGHT));
                } else {
                    queueIfNotAlreadyQueued(Pair.of(position.down(), WaterDirection.DOWN));
                }
                break;
            case LEFT:
                if (isClayOrSettledWaterBelow(position)) {
                    queueIfNotAlreadyQueued(Pair.of(position.left(), WaterDirection.LEFT));
                } else {
                    queueIfNotAlreadyQueued(Pair.of(position.down(), WaterDirection.DOWN));
                }
                break;
            case RIGHT:
                if (isClayOrSettledWaterBelow(position)) {
                    queueIfNotAlreadyQueued(Pair.of(position.right(), WaterDirection.RIGHT));
                } else {
                    queueIfNotAlreadyQueued(Pair.of(position.down(), WaterDirection.DOWN));
                }
                break;
        }
    }

    private void addSettledWater(Position position) {
        List<Position> newSettledWater = IntStream.range(getNextClayToTheLeft(position).x + 1, getNextClayToTheRight(position).x)
                .boxed()
                .map(x -> new Position(x, position.y))
                .collect(Collectors.toList());
        settledWater.addAll(newSettledWater);
        reachableWaterPositions.removeAll(newSettledWater);
    }

    private boolean isInReservoir(Position position) {
        Position nextClayToTheLeft = getNextClayToTheLeft(position);
        if (nextClayToTheLeft == null) {
            return false;
        }
        Position nextClayToTheRight = getNextClayToTheRight(position);
        if (nextClayToTheRight == null) {
            return false;
        }
        for (int x = nextClayToTheLeft.x; x <= nextClayToTheRight.x; x++) {
            Position positionBelow = new Position(x, position.y);
            if (!isClayOrSettledWaterBelow(positionBelow)) {
                return false;
            }
        }
        return true;
    }

    Position getNextClayToTheLeft(Position position) {
        return clayPositions.stream()
                .filter(it -> it.y == position.y)
                .filter(it -> it.x < position.x)
                .max(Comparator.comparingInt(it -> it.x))
                .orElse(null);
    }

    Position getNextClayToTheRight(Position position) {
        return clayPositions.stream()
                .filter(it -> it.y == position.y)
                .filter(it -> it.x > position.x)
                .min(Comparator.comparingInt(it -> it.x))
                .orElse(null);
    }

    boolean isClayOrSettledWaterBelow(Position position) {
        return isClayBelow(position) || isSettledWaterBelow(position);
    }

    boolean isSettledWaterBelow(Position position) {
        return settledWater.contains(position.down());
    }

    boolean isClayBelow(Position position) {
        return clayPositions.contains(position.down());
    }

    private void setupInternals(List<Position> clayPositions) {
        settledWater = new ArrayList<>();
        reachableWaterPositions = new ArrayList<>();
        this.clayPositions = clayPositions;
        this.minY = getMinY();
        this.maxY = getMaxY();
    }

    int getMinY() {
        return PositionUtil.getMinY(clayPositions);
    }

    int getMaxY() {
        return PositionUtil.getMaxY(clayPositions);
    }
}
