package com.mattias.advent.day18

import spock.lang.Specification

class AcreeLogicTest extends Specification {

    def acreeLogic = new AcreLogic()

    def "Test happycase lumberyard"() {
        given:
        def state = [['#', '#', '#'] as char[],
                     ['#', 'X', '#'] as char[],
                     ['#', '#', '#'] as char[]] as char[][]

        when:
        def lumberyards = acreeLogic.getSurroundingLumberyards(state, 1, 1)

        then:
        lumberyards == 8
    }

    def "Test happycase trees"() {
        given:
        def state = [['|', '|', '|'] as char[],
                     ['|', 'X', '|'] as char[],
                     ['|', '|', '|'] as char[]] as char[][]

        when:
        def trees = acreeLogic.getSurroundingTrees(state, 1, 1)

        then:
        trees == 8
    }

}
