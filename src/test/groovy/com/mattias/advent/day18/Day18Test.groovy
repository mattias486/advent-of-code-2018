package com.mattias.advent.day18

import com.mattias.advent.common.VisualizationUtil
import spock.lang.Ignore
import spock.lang.Specification
import spock.lang.Unroll

class Day18Test extends Specification {

    def day18 = new Day18()
    def visualizationUtil = new VisualizationUtil()

    def "test reading examples"() {
        when:
        def examples = day18.getExamples();

        then:
        examples.size() == 11
        examples.get(0).size() == 10
        examples.get(0)[0] as String == ".#.#...|#."
        examples.get(0)[9] as String == "...#.|..|."
        examples.get(10)[0] as String == ".||##....."
        examples.get(10)[9] as String == "||||||||||"
    }

    def "test reading input"() {
        when:
        def input = day18.getInput()

        then:
        input.length == 50
        input[0].length == 50
        input[49].length == 50
    }

    @Unroll
    def "test examples, minute : #minute"() {
        given:
        def examples = day18.getExamples()
        def initialState = examples.get(0)

        when:
        def expectedState = examples.get(minute)
        def afterMinute = day18.getNewState(initialState, minute)

        then:
        /*System.out.println("Actual :")
        visualizationUtil.draw(afterMinute)
        System.out.println("Expected :")
        visualizationUtil.draw(expectedState)*/

        afterMinute == expectedState

        where:
        minute << [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
    }

    @Ignore("Part 1")
    def "get resource value"() {
       given:
       def initialState = day18.getInput()

       when:
       def endState = day18.getNewState(initialState, 10)
       def resourceValue = day18.getResourceValue(endState)

       then:
       System.out.println("Resource value : " + resourceValue)
    }

    @Unroll
    def "Make sure simplified resource value equals regular calculator"() {
        given:
        def initialState = day18.getInput()

        when:
        def endState = day18.getNewState(initialState, minute_to_test)
        def resourceValue = day18.getResourceValue(endState)
        def simplifiedResourceValue = day18.getResourceValueForHighValues(initialState, minute_to_test);

        then:
        resourceValue == simplifiedResourceValue

        where:
        minute_to_test << [634, 10_020, 10_028]
    }

    @Ignore("Part 2")
    def "get resource value really far in the future"() {
        given:
        def initialState = day18.getInput()

        when:
        def resourceValue = day18.getResourceValueForHighValues(initialState, 1_000_000_000);

        then:
        System.out.println("Resource value : " + resourceValue)
    }
}
