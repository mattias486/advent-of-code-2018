package com.mattias.advent.day19

import spock.lang.Ignore
import spock.lang.Specification

class Day19Test extends Specification {

    def day19 = new Day19()

    def "Get input"() {
        when:
        def instructions = day19.getInstruction()

        then:
        instructions.size() > 0
        instructions[0].name == "addi"
        instructions[0].arguments == [1, 16, 1] as int[]
        instructions[instructions.size() - 1].name == "seti"
        instructions[instructions.size() - 1].arguments == [0, 6, 1] as int[]
    }


    @Ignore("Part 1")
    def "Run program with empty register"() {
        given:
        def instructions = day19.getInstruction()

        when:
        def registers = day19.runProgram(instructions, [0, 0, 0, 0, 0, 0] as int[])

        then:
        System.out.println("Registers after running program : " + registers)
    }

    @Ignore("Part 2")
    def "Run alternative implementation of program"() {
        when:
        def calculatedValue = new ImprovedCalculationAlgorithm().calculateEndValue()

        then:
        System.out.println("Calculated end value for program : " + calculatedValue)
    }
}
