package com.mattias.advent.day19;

import java.util.stream.IntStream;

/**
 * Spoiler : I solved this by starting to implement the instructions by hand
 * in java by using the registers and then simplified it one step at a time,
 * finally ending up with implementation below that I used.
 *
 * Running the program using the operations is to slow due to the inefficient algorithm
 * for finding wanted values
 *
 * I removed the comments
 */
public class ImprovedCalculationAlgorithm {

    public static final int REGISTER_INDEX_3 = 10_551_408;

    //register 2 loops from 1 up to including REGISTER_INDEX_3
    //register 4 loops from 1 up to including REGISTER_INDEX_3
    //if register 2 and register 4 multiplied at some time matches REGISTER_INDEX_3
    //  then increase the register 0 value by the value in register 4
    public long calculateEndValue() {
        return IntStream.range(1, REGISTER_INDEX_3 + 1)
                .parallel()
                .filter(register4 ->  REGISTER_INDEX_3 % register4 == 0)
                .sum();

    }
}
