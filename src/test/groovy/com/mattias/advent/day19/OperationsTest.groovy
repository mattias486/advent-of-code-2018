package com.mattias.advent.day19

import spock.lang.Specification

class OperationsTest extends Specification {

    def "Test addr"() {
        given:
        def operand = [1, 2, 3] as int[]
        def register = [0, 2, 4, 0] as int[]

        when:
        def after = Operations.addr.apply(register, operand)

        then:
        after == [0, 2, 4, 6, 0, 0] as int[]
    }

    def "Test addi"() {
        given:
        def operand = [1, 2, 3] as int[]
        def register = [0, 2, 0, 0] as int[]

        when:
        def after = Operations.addi.apply(register, operand)

        then:
        after == [0, 2, 0, 4, 0, 0] as int[]
    }

    def "Test mulr"() {
        given:
        def operand = [1, 2, 3] as int[]
        def register = [0, 2, 3, 0] as int[]

        when:
        def after = Operations.mulr.apply(register, operand)

        then:
        after == [0, 2, 3, 6, 0, 0] as int[]
    }

    def "Test muli"() {
        given:
        def operand = [1, 2, 3] as int[]
        def register = [0, 2, 3, 0] as int[]

        when:
        def after = Operations.muli.apply(register, operand)

        then:
        after == [0, 2, 3, 4, 0, 0] as int[]
    }

    def "Test setr"() {
        given:
        def operand = [1, 0, 3] as int[]
        def register = [0, 2, 0, 0] as int[]

        when:
        def after = Operations.setr.apply(register, operand)

        then:
        after == [0, 2, 0, 2, 0, 0] as int[]
    }

    def "Test seti"() {
        given:
        def operand = [1, 0, 3] as int[]
        def register = [0, 2, 0, 0] as int[]

        when:
        def after = Operations.seti.apply(register, operand)

        then:
        after == [0, 2, 0, 1, 0, 0] as int[]
    }

    def "Test gtir"() {
        given:
        def operand = [5, 1, 2] as int[]
        def register = [0, 4, 0, 0, 0, 0] as int[]

        when:
        def after = Operations.gtir.apply(register, operand)

        then:
        after == [0, 4, 1, 0, 0, 0] as int[]
    }

    def "Test gtri"() {
        given:
        def operand = [1, 3, 2] as int[]
        def register = [0, 4, 0, 0] as int[]

        when:
        def after = Operations.gtri.apply(register, operand)

        then:
        after == [0, 4, 1, 0, 0, 0] as int[]
    }

    def "Test gtrr"() {
        given:
        def operand = [1, 2, 3] as int[]
        def register = [0, 3, 2, 0] as int[]

        when:
        def after = Operations.gtrr.apply(register, operand)

        then:
        after == [0, 3, 2, 1, 0, 0] as int[]
    }

    def "Test eqir"() {
        given:
        def operand = [3, 2, 3] as int[]
        def register = [0, 0, 3, 0] as int[]

        when:
        def after = Operations.eqir.apply(register, operand)

        then:
        after == [0, 0, 3, 1, 0, 0] as int[]
    }

    def "Test eqri"() {
        given:
        def operand = [2, 1, 3] as int[]
        def register = [0, 0, 1, 0] as int[]

        when:
        def after = Operations.eqri.apply(register, operand)

        then:
        after == [0, 0, 1, 1, 0, 0] as int[]
    }

    def "Test eqrr"() {
        given:
        def operand = [1, 3, 2] as int[]
        def register = [0, 4, 0, 4] as int[]

        when:
        def after = Operations.eqrr.apply(register, operand)

        then:
        after == [0, 4, 1, 4, 0, 0] as int[]
    }

    def "Test banr"() {
        given:
        def operand = [1, 2, 3] as int[]
        def register = [0, 6, 5, 0] as int[]

        when:
        def after = Operations.banr.apply(register, operand)

        then:
        after == [0, 6, 5, 4, 0, 0] as int[]
    }

    def "Test bani"() {
        given:
        def operand = [1, 5, 3] as int[]
        def register = [0, 6, 0, 0] as int[]

        when:
        def after = Operations.bani.apply(register, operand)

        then:
        after == [0, 6, 0, 4, 0, 0] as int[]
    }

    def "Test borr"() {
        given:
        def operand = [1, 2, 3] as int[]
        def register = [0, 6, 5, 0] as int[]

        when:
        def after = Operations.borr.apply(register, operand)

        then:
        after == [0, 6, 5, 7, 0, 0] as int[]
    }

    def "Test bori"() {
        given:
        def operand = [1, 5, 3] as int[]
        def register = [0, 6, 0, 0] as int[]

        when:
        def after = Operations.bori.apply(register, operand)

        then:
        after == [0, 6, 0, 7, 0, 0] as int[]
    }

}
