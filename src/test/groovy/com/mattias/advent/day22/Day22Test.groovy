package com.mattias.advent.day22

import spock.lang.Ignore
import spock.lang.Specification

class Day22Test extends Specification{

    def day22 = new Day22()

    def "Draw example"() {
        when:
        def depth = 510
        def target = new Day22.Coordinate(10, 10)

        then:
        day22.getRiskOfArea(depth, target)
    }

    @Ignore("Part01")
    def "Part 01"() {
        when:
        def depth = 6969
        def target = new Day22.Coordinate(9,796)

        then:
        day22.getRiskOfArea(depth, target)
    }
}
